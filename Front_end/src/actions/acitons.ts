import {
    ADD_BRANCH,
    ADD_DEPARTMENT, ADD_PROVIDER,
    ADD_RADIOSTATION,
    ADD_TECHNIQUE,
    DELETE_BRANCH,
    DELETE_DEPARTMENT, DELETE_PROVIDER,
    DELETE_RADIOSTATION,
    DELETE_TECHNIQUE,
    LOAD_BRANCH,
    LOAD_DEPARTMENT, LOAD_DIAPASONS, LOAD_PROVIDER,
    LOAD_RADIOSTATION, LOAD_SPECIFICITIES,
    LOAD_TECHNIQUE, SELECT_ALL_TECHNIQUES,
    SELECT_BRANCH,
    SELECT_BRANCH_FOR_EDIT, SELECT_COLUMN,
    SELECT_DEPARTMENT,
    SELECT_DEPARTMENT_FOR_EDIT, SELECT_PROVIDER, SELECT_PROVIDER_FOR_EDIT,
    SELECT_RADIOSTATION, SELECT_TAB,
    SELECT_TECHNIQUE, UNLOCK_APP,
    UPDATE_BRANCH,
    UPDATE_DEPARTMENT, UPDATE_PROVIDER,
    UPDATE_RADIOSTATION,
    UPDATE_TECHNIQUE
} from "../consts/actionTypes";
import {Action} from "redux";
import axios from 'axios';
import {IBranch, IColumn, ICommEquipment, IDepartment, IFilter, IProvider} from "../consts/consts";

export const loadDepartment = (): Action | any => {
    return (dispatch: any) => {
        return axios.get('/departments')
            .then(response => {
                dispatch({
                    type: LOAD_DEPARTMENT,
                    payload: {
                        response: response.data,
                    }
                })
            })
            .catch(error => {
                throw(error);
            });
    };
};

export const addDepartment = (name: string): Action | any => {
    return (dispatch: any) => {
        return axios.post(`/department/add`, {name})
            .then(response => {
                dispatch({
                    type: ADD_DEPARTMENT,
                    payload: {
                        response: response.data,
                    }
                })
            })
            .catch(error => {
                throw(error);
            });
    };
};

export const selectDepartment = (department: IDepartment) => ({
    type: SELECT_DEPARTMENT,
    payload: {
        selected: department,
    }
});

export const selectDepartmentForEdit = (department: IDepartment) => ({
    type: SELECT_DEPARTMENT_FOR_EDIT,
    payload: {
        selected: department,
    }
});

export const updateDepartment = (id: number, name: string): Action | any => {
    return (dispatch: any) => {
        return axios.post(`/department/update/${id}`, {name})
            .then(response => {
                dispatch({
                    type: UPDATE_DEPARTMENT,
                    payload: {
                        response: response.data,
                    }
                })
            })
            .catch(error => {
                throw(error);
            });
    };
};

export const deleteDepartment = (id: number): Action | any => {
    return (dispatch: any) => {
        return axios.get(`/department/delete/${id}`)
            .then(response => {
                dispatch({
                    type: DELETE_DEPARTMENT,
                    payload: {
                        response: response.data,
                    }
                })
            })
            .catch(error => {
                alert("Удаление не произошло\nУбедитесь, что удалили все службы в этом подразделении");
                throw(error);
            });
    };
};

export const loadBranch = (id: number): Action | any => {
    return (dispatch: any) => {
        return axios.get(`/branches/${id}`)
            .then(response => {
                dispatch({
                    type: LOAD_BRANCH,
                    payload: {
                        response: response.data,
                    }
                })
            })
            .catch(error => {
                throw(error);
            });
    };
};

export const addBranch = (name: string, idDepartment: number): Action | any => {
    return (dispatch: any) => {
        return axios.post(`/branch/add/${idDepartment}`, {name, idDepartment})
            .then(response => {
                dispatch({
                    type: ADD_BRANCH,
                    payload: {
                        response: response.data,
                    }
                })
            })
            .catch(error => {
                throw(error);
            });
    };
};

export const selectBranch = (branch: IBranch) => ({
    type: SELECT_BRANCH,
    payload: {
        selected: branch,
    }
});


export const selectBranchForEdit = (branch: IBranch) => ({
    type: SELECT_BRANCH_FOR_EDIT,
    payload: {
        selected: branch,
    }
});

export const updateBranch = (idDepartment: number, id: number, name: string): Action | any => {
    return (dispatch: any) => {
        return axios.post(`department/${idDepartment}/branch/${id}/update`, {name})
            .then(response => {
                dispatch({
                    type: UPDATE_BRANCH,
                    payload: {
                        response: response.data,
                    }
                })
            })
            .catch(error => {
                throw(error);
            });
    };
};

export const deleteBranch = (department: IDepartment, id: number): Action | any => {
    return (dispatch: any) => {
        return axios.get(`department/${department.id}/branch/${id}/delete`)
            .then(response => {
                dispatch({
                    type: DELETE_BRANCH,
                    payload: {
                        response: response.data,
                    }
                })
            })
            .catch(error => {
                alert("Удаление не произошло\nУбедитесь, что удалили все средства связи в этой службе");
                throw(error);
            });
    };
};

export const loadRadiostations = (id: number): Action | any => {
    return (dispatch: any) => {
        return axios.get(`/radiostations/${id}`)
            .then(response => {
                dispatch({
                    type: LOAD_RADIOSTATION,
                    payload: {
                        response: response.data,
                    }
                })
            })
            .catch(error => {
                throw(error);
            });
    };
};

export const selectRadiostation = (radiostation: ICommEquipment | null) => ({
    type: SELECT_RADIOSTATION,
    payload: {
        selected: radiostation,
    }
});

export const addRadiostation = (data: ICommEquipment, idBranch: number): Action | any => {
    return (dispatch: any) => {
        return axios.post(`/radiostation/add/${idBranch}`, data)
            .then(response => {
                dispatch({
                    type: ADD_RADIOSTATION,
                    payload: {
                        response: response.data,
                    }
                })
            })
            .catch(error => {
                throw(error);
            });
    };
};

export const editRadiostation = (data: ICommEquipment, idBranch: number): Action | any => {
    return (dispatch: any) => {
        return axios.post(`/radiostation/update/${idBranch}`, data)
            .then(response => {
                dispatch({
                    type: UPDATE_RADIOSTATION,
                    payload: {
                        response: response.data,
                    }
                })
            })
            .catch(error => {
                throw(error);
            });
    };
};

export const deleteRadiostation = (radiostation: ICommEquipment | null, idBranch: number): Action | any => {
    return (dispatch: any) => {
        return axios.post(`/radiostation/delete/${idBranch}`, radiostation)
            .then(response => {
                dispatch({
                    type: DELETE_RADIOSTATION,
                    payload: {
                        response: response.data,
                    }
                })
            })
            .catch(error => {
                throw(error);
            });
    };
};

export const loadTechniques = (id: number): Action | any => {
    return (dispatch: any) => {
        return axios.get(`/techniques/${id}`)
            .then(response => {
                dispatch({
                    type: LOAD_TECHNIQUE,
                    payload: {
                        response: response.data,
                    }
                })
            })
            .catch(error => {
                throw(error);
            });
    };
};

export const selectTechnique = (technique: ICommEquipment | null) => ({
    type: SELECT_TECHNIQUE,
    payload: {
        selected: technique,
    }
});

export const addTechnique = (data: ICommEquipment, idBranch: number): Action | any => {
    return (dispatch: any) => {
        return axios.post(`/technique/add/${idBranch}`, data)
            .then(response => {
                dispatch({
                    type: ADD_TECHNIQUE,
                    payload: {
                        response: response.data,
                    }
                })
            })
            .catch(error => {
                throw(error);
            });
    };
};

export const editTechnique = (data: ICommEquipment, idBranch: number): Action | any => {
    return (dispatch: any) => {
        return axios.post(`/technique/update/${idBranch}`, data)
            .then(response => {
                dispatch({
                    type: UPDATE_TECHNIQUE,
                    payload: {
                        response: response.data,
                    }
                })
            })
            .catch(error => {
                throw(error);
            });
    };
};

export const deleteTechnique = (technique: ICommEquipment | null, idBranch: number): Action | any => {
    return (dispatch: any) => {
        return axios.post(`/technique/delete/${idBranch}`, technique)
            .then(response => {
                dispatch({
                    type: DELETE_TECHNIQUE,
                    payload: {
                        response: response.data,
                    }
                })
            })
            .catch(error => {
                throw(error);
            });
    };
};

export const loadSpecificities = (): Action | any => {
    return (dispatch: any) => {
        return axios.get('/specificities')
            .then(response => {
                dispatch({
                    type: LOAD_SPECIFICITIES,
                    payload: {
                        response: response.data,
                    }
                })
            })
            .catch(error => {
                throw(error);
            });
    };
};

export const loadDiapasons = (): Action | any => {
    return (dispatch: any) => {
        return axios.get('/diapasons')
            .then(response => {
                dispatch({
                    type: LOAD_DIAPASONS,
                    payload: {
                        response: response.data,
                    }
                })
            })
            .catch(error => {
                throw(error);
            });
    };
};

export const loadProviders = (): Action | any => {
    return (dispatch: any) => {
        return axios.get('/providers')
            .then(response => {
                dispatch({
                    type: LOAD_PROVIDER,
                    payload: {
                        response: response.data,
                    }
                })
            })
            .catch(error => {
                throw(error);
            });
    };
};

export const addProvider = (name: string): Action | any => {
    return (dispatch: any) => {
        return axios.post(`/provider/add`, {name})
            .then(response => {
                dispatch({
                    type: ADD_PROVIDER,
                    payload: {
                        response: response.data,
                    }
                })
            })
            .catch(error => {
                throw(error);
            });
    };
};

export const selectProvider = (provider: IProvider | null) => ({
    type: SELECT_PROVIDER,
    payload: {
        selected: provider,
    }
});

export const selectProviderForEdit = (provider: IProvider | null) => ({
    type: SELECT_PROVIDER_FOR_EDIT,
    payload: {
        selected: provider,
    }
});

export const updateProvider = (id: number, name: string): Action | any => {
    return (dispatch: any) => {
        return axios.post(`/provider/update/${id}`, {name})
            .then(response => {
                dispatch({
                    type: UPDATE_PROVIDER,
                    payload: {
                        response: response.data,
                    }
                })
            })
            .catch(error => {
                throw(error);
            });
    };
};

export const deleteProvider = (id: number): Action | any => {
    return (dispatch: any) => {
        return axios.get(`/provider/delete/${id}`)
            .then(response => {
                dispatch({
                    type: DELETE_PROVIDER,
                    payload: {
                        response: response.data,
                    }
                })
            })
            .catch(error => {
                alert("Удаление не произошло\nУбедитесь, что этот поставщик нигде не указан");
                throw(error);
            });
    };
};

export const getAllTechniques = (filter: IFilter): Action | any => {
    return (dispatch: any) => {
        return axios.post(`/all-techniques`, filter)
            .then(response => {
                dispatch({
                    type: SELECT_ALL_TECHNIQUES,
                    payload: {
                        response: response.data,
                    }
                })
            })
            .catch(error => {
                throw(error);
            });
    };
};

export const createReport = (filter: IFilter): Action | any => {
    console.log(filter + "123");
        return axios.post(`/create-report`, filter)
            .then(response => {
               alert("Отчет сформирован")
            })
            .catch(error => {
                alert("Произошла ошибка при формировании отчета")
            });
};

export const selectTab = (tabIndex: number) => {
    return (dispatch: any) => {
        dispatch({
            type: SELECT_TAB,
            payload: {
                tabIndex,
            },
        })
    }
};

export const selectColumns = (selectedColumns: IColumn[]) => {
    return (dispatch: any) => {
        dispatch({
            type: SELECT_COLUMN,
            payload: {
                selectedColumns,
            },
        })
    }
};

export const unlockApp = () => {
    return (dispatch: any) => {
        dispatch({
            type: UNLOCK_APP,
        })
    }
};
