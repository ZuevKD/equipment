import React from 'react';
import {IBranch, IColumn, ICommEquipment, ITableHeader, ReadableField} from "../../consts/consts";
import {bindActionCreators, Dispatch} from "redux";
import {deleteTechnique, loadTechniques, selectTechnique} from "../../actions/acitons";
import {connect} from "react-redux";
import {Edit} from "@material-ui/icons";
import {TableRow} from "@material-ui/core";
import TableCell from "@material-ui/core/es/TableCell";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';
import {AbsentEntities} from "./AbsentEntities";
import {ExistEntities} from "./ExtistEntities";
import EditTechniqueDialog from "./EditTechniqueDialog";
import {renderCell} from "../common/Utils";
import DeleteDialog from "./DeleteDialog";

let headers: ITableHeader[] = [];

let i = 100;

const createHeader = (name: string) => {
    headers.push({id: i, title: name, align: 'left'} as ITableHeader);
    i++;
};

createHeader(ReadableField.NAME);
createHeader(ReadableField.MODEL);
createHeader(ReadableField.SERIAL_NUM);
createHeader(ReadableField.INVENTORY_NUM);
createHeader(ReadableField.LIFE_TIME);
createHeader(ReadableField.DISTRIBUTION_PLAN);
createHeader(ReadableField.RELEASE_DATE);
createHeader(ReadableField.COMMISSIONING_DATE);
createHeader(ReadableField.PRICE);
createHeader(ReadableField.MODEL_OPERABILITTY);
createHeader(ReadableField.PROVIDER);
createHeader(ReadableField.INVENTORY_NUM);
createHeader(ReadableField.INVOICE_DATA);
createHeader(ReadableField.EQUIPMENT);
createHeader(ReadableField.DESCRIPTION);
createHeader('');

interface IDispatchProps {
    loadTechniques: (id: number) => any;
    selectTechnique: (technique: ICommEquipment | null) => any;
    deleteTechnique: (technique: ICommEquipment | null, idBranch: number) => any;
}

interface IStateProps {
    items: ICommEquipment[];
    selectedBranch: IBranch;
    selectedColumns: IColumn[];
}

interface IState {
    show: boolean;
    showDeleteDialog: boolean;
    selectedForDelete: ICommEquipment | null;
}

type IProps = IDispatchProps & IStateProps;

class TableTechnique extends React.Component<IProps, IState> {
    state: IState = {
        show: false,
        showDeleteDialog: false,
        selectedForDelete: null,
    };

    private isActiveColumn = (name: string): boolean => {
        const {selectedColumns} = this.props;
        const selectedColumn = selectedColumns.find(x => x.name === name);

        return selectedColumn ? selectedColumn.selected : true;
    };

    private createHeaders = () => {
        let i = 100;
        headers = [];

        const createHeader = (name: string) => {
            this.isActiveColumn(name) && headers.push({
                id: i,
                title: name,
                align: 'left'
            } as ITableHeader);
            i++;
        };

        createHeader(ReadableField.NAME);
        createHeader(ReadableField.MODEL);
        createHeader(ReadableField.SERIAL_NUM);
        createHeader(ReadableField.INVENTORY_NUM);
        createHeader(ReadableField.LIFE_TIME);
        createHeader(ReadableField.DISTRIBUTION_PLAN);
        createHeader(ReadableField.RELEASE_DATE);
        createHeader(ReadableField.COMMISSIONING_DATE);
        createHeader(ReadableField.PRICE);
        createHeader(ReadableField.MODEL_OPERABILITTY);
        createHeader(ReadableField.PROVIDER);
        createHeader(ReadableField.INVOICE_NUM);
        createHeader(ReadableField.INVOICE_DATA);
        createHeader(ReadableField.EQUIPMENT);
        createHeader(ReadableField.DESCRIPTION);
        createHeader('');
    };

    private openDialog = (technique: ICommEquipment | null = null) => () => {
        if (technique !== null) {
            this.props.selectTechnique(technique);
        } else {
            this.props.selectTechnique(null);
        }

        this.setState({show: true});
    };

    private openDeleteDialog = (radiostation: ICommEquipment | null = null) => () => {
        if (radiostation !== null) {
            this.setState({showDeleteDialog: true, selectedForDelete: radiostation});
        }
    };

    private closeDialog = () => this.setState({show: false});
    private closeDeleteDialog = () => this.setState({showDeleteDialog: false});

    render() {
        this.createHeaders();
        return (
            <>
                {this.props.items.length ?
                    <ExistEntities
                        openAddDialog={this.openDialog}
                        nameEntities={ReadableField.TECHNIQUE}
                        headers={headers}
                        renderRows={this.renderRows}
                    />
                    :
                    <AbsentEntities openAddDialog={this.openDialog} nameEntities={ReadableField.TECHNIQUE}/>
                }
                <EditTechniqueDialog
                    handleClose={this.closeDialog}
                    open={this.state.show}
                    idBranch={this.props.selectedBranch.id}
                />

                <DeleteDialog
                    closeDialog={this.closeDeleteDialog}
                    isOpen={this.state.showDeleteDialog}
                    idBranch={this.props.selectedBranch.id}
                    deleteCommEquipment={this.onDelete}
                    commEquipment={this.state.selectedForDelete}
                />
            </>
        )
    }

    private renderRows = (classNames: string) => this.props.items.map(row => {
        let id = Math.random();
            return (
                <TableRow key={id} hover>
                    {renderCell(row.model.technique.name, classNames)}
                    {renderCell(row.model.name, classNames)}
                    {this.isActiveColumn(ReadableField.SERIAL_NUM) && renderCell(row.serialNum, classNames)}
                    {this.isActiveColumn(ReadableField.INVENTORY_NUM) && renderCell(row.inventoryNum, classNames)}
                    {this.isActiveColumn(ReadableField.LIFE_TIME) && renderCell(row.model.lifeTime, classNames)}
                    {this.isActiveColumn(ReadableField.DISTRIBUTION_PLAN) && renderCell(row.distributionPlan, classNames)}
                    {this.isActiveColumn(ReadableField.RELEASE_DATE) && renderCell(row.releaseDate, classNames)}
                    {this.isActiveColumn(ReadableField.COMMISSIONING_DATE) && renderCell(row.commissioningDate, classNames)}
                    {this.isActiveColumn(ReadableField.PRICE) && renderCell(row.price, classNames)}
                    {this.isActiveColumn(ReadableField.MODEL_OPERABILITTY) && renderCell(row.modelOperability, classNames)}
                    {this.isActiveColumn(ReadableField.PROVIDER) && renderCell(row.supply.provider.name, classNames)}
                    {this.isActiveColumn(ReadableField.INVOICE_NUM) && renderCell(row.supply.invoiceNum, classNames)}
                    {this.isActiveColumn(ReadableField.INVOICE_DATA) && renderCell(row.supply.date, classNames)}
                    {this.isActiveColumn(ReadableField.EQUIPMENT) && renderCell(row.equipment.composition, classNames)}
                    {this.isActiveColumn(ReadableField.DESCRIPTION) && renderCell(row.equipment.description, classNames)}
                    <TableCell
                        className={classNames}
                        scope="row"
                        align={'right'}
                        style={{width: 100, fontSize: 11}}
                        key={id}
                    >
                        <IconButton onClick={this.openDialog(row)}>
                            <Edit color="primary"/>
                        </IconButton>
                        <IconButton onClick={this.openDeleteDialog(row)}>
                            <DeleteIcon color={'error'}/>
                        </IconButton>
                    </TableCell>
                </TableRow>
            )
        }
    );

    private onDelete = (technique: ICommEquipment | null, idBranch: number) => {
        this.setState({
            showDeleteDialog: false,
            selectedForDelete: null,
        });

        this.props.deleteTechnique(technique, idBranch);
    }


}

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators({
        loadTechniques,
        selectTechnique,
        deleteTechnique,
    }, dispatch);

const mapStateToProps = (state: any): IStateProps => ({
    items: state.techniques.items,
    selectedBranch: state.branches.selected,
    selectedColumns: state.selectedColumns,
});

export default connect<IStateProps, IDispatchProps, null>(
    mapStateToProps,
    mapDispatchToProps,
)(TableTechnique);

