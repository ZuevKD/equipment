import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import React from "react";
import {withStyles} from "@material-ui/core";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import {ICommEquipment} from "../../consts/consts";
import {stylesForEditedDialog, Transition} from "../common/Utils";

interface IProps {
    isOpen: boolean;
    closeDialog: () => any;
    deleteCommEquipment: (commEquipment: ICommEquipment | null, idBranch: number) => any;
    idBranch: number;
    commEquipment: ICommEquipment | null;
}
const DeleteDialog = ({ closeDialog, isOpen, idBranch, commEquipment, deleteCommEquipment}: IProps) => {

    const onDelete = (commEquipment: ICommEquipment | null, idBranch: number) => () =>
        deleteCommEquipment(commEquipment, idBranch);

    const getName = () => {
       if (commEquipment === null) {
           return '';
       }
       const radiostation = commEquipment.model.radiostation;
       const technique = commEquipment.model.technique;

       return radiostation ? radiostation.name : technique.name;
    };

    return (
        <Dialog
            open={isOpen}
            TransitionComponent={Transition}
            maxWidth={'xl'}

        >
            <DialogTitle>Внимание!</DialogTitle>
            <DialogContent>
                <p>{`Вы действительно хотите удалить ${getName()}?`}</p>
                <DialogActions>
                    <Button onClick={onDelete(commEquipment, idBranch)} color="primary">
                        Удалить
                    </Button>
                    <Button onClick={closeDialog} color="primary">
                        Отменить
                    </Button>
                </DialogActions>
            </DialogContent>
        </Dialog>
    )
};

export default withStyles(stylesForEditedDialog)(DeleteDialog);
