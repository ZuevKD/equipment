import React from 'react';

import {ReadableField} from "../../consts/consts";
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';

interface IProps {
    openAddDialog: (value: null) => any;
    nameEntities: ReadableField.RADIOSTATIONS | ReadableField.TECHNIQUE;
}

export const AbsentEntities = ({openAddDialog, nameEntities}: IProps) => (
    <div style={{textAlign: 'center'}}>
        <h3 style={{marginTop: 10, marginBottom: 0}}>
            {nameEntities === ReadableField.RADIOSTATIONS ? `${nameEntities} отсутствуют` : `${nameEntities} отсутствует`}
        </h3>
        <Fab
            color="primary"
            aria-label="Add"
            size='small'
            onClick={openAddDialog(null)}
        >
            <AddIcon/>
        </Fab>
    </div>
);