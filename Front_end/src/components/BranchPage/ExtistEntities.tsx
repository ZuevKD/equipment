import React from 'react';

import {ITableHeader, ReadableField} from "../../consts/consts";
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import SelectDisplayColumn from "./SelectDisplayColumn";
import CommonTable from "../common/CommonTable";

interface IProps {
    openAddDialog: (value: null) => any;
    nameEntities: ReadableField.RADIOSTATIONS | ReadableField.TECHNIQUE;
    headers: ITableHeader[],
    renderRows: (classNames: string) => JSX.Element[];
}

export const ExistEntities = ({openAddDialog, nameEntities, headers, renderRows}: IProps) => (
    <>
        <div style={{display: 'flex', alignItems: 'center'}}>
            <div>
                <h3>{nameEntities}</h3>
            </div>

            <Fab
                color="primary"
                aria-label="Add"
                style={{marginLeft: 16}}
                size='small'
                onClick={openAddDialog(null)}
            >
                <AddIcon/>
            </Fab>

            <div style={{marginLeft: 'auto'}}>
                <SelectDisplayColumn/>
            </div>
        </div>
        <CommonTable
            headers={headers}
            renderRows={renderRows}
            height={'100%'}
        />
    </>
);