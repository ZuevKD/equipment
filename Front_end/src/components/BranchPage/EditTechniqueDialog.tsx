import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import {TextField, withStyles} from "@material-ui/core";
import {ICommEquipment, IProvider, ReadableField} from "../../consts/consts";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormLabel from "@material-ui/core/FormLabel";
import {connect} from "react-redux";
import {bindActionCreators, Dispatch} from "redux";
import {addTechnique, editTechnique, loadProviders} from "../../actions/acitons";
import {
    getBoolValue,
    getValueForRadioGroup,
    isEmpty,
    MoneyFormatCustom, NumberFormatCustom,
    stylesForEditedDialog,
    Transition
} from "../common/Utils";
import MenuItem from '@material-ui/core/MenuItem';

interface IDispatchProps {
    addTechnique: (data: ICommEquipment, branchId: number) => any;
    editTechnique: (data: ICommEquipment, branchId: number) => any;
}

interface IStateProps {
    selectedTechnique: ICommEquipment;
    providers: IProvider[];
}

interface IProps {
    handleClose: () => any;
    open: boolean;
    idBranch: number
    classes: {
        textField: string
    },
}

type TProps = IProps & IDispatchProps & IStateProps;

const EditTechniqueDialog = ({handleClose, open, classes, idBranch, selectedTechnique, addTechnique, editTechnique, providers}: TProps) => {
    const [name, setName] = React.useState('');
    const [model, setModel] = React.useState('');
    const [inventoryNum, setInventoryNum] = React.useState();
    const [serialNum, setSerialNum] = React.useState();
    const [lifeTime, setLifeTime] = React.useState();
    const [price, setPrice] = React.useState();
    const [releaseDate, setReleaseDate] = React.useState();
    const [commissioningDate, setCommissioningDate] = React.useState();
    const [distributionPlan, setDistributionPlan] = React.useState();
    const [modelOperability, setOperability] = React.useState();
    const [composition, setComposition] = React.useState('');
    const [description, setDescription] = React.useState('');
    const [provider, setProvider] = React.useState();
    const [invoiceNum, setInvoiceNum] = React.useState();
    const [invoiceDate, setInvoiceDate] = React.useState();
    const [isChecked, setChecked] = React.useState(false);

    React.useEffect(() => {
        if (selectedTechnique) {
            const {model, inventoryNum, serialNum, supply, releaseDate, commissioningDate, distributionPlan, equipment, modelOperability, price} = selectedTechnique;
            const {
                name: nameModel,
                lifeTime,
                technique,
            } = model;
            const {provider, date, invoiceNum} = supply;
            const {description, composition} = equipment;
            const {name} = technique;

            setName(name);
            setModel(nameModel);
            setSerialNum(serialNum);
            setInventoryNum(inventoryNum);
            setLifeTime(lifeTime);
            setReleaseDate(releaseDate);
            setCommissioningDate(commissioningDate);
            setDistributionPlan(distributionPlan);
            setOperability(modelOperability);
            setComposition(composition);
            setDescription(description);
            setProvider(provider.id);
            setInvoiceNum(invoiceNum);
            setInvoiceDate(date);
            setPrice(price);
        } else {
            resetFields();
        }
    }, [selectedTechnique]);

    function handleChangeName(event: any) {
        setName(event.target.value);
    }

    function handleChangeModel(event: any) {
        setModel(event.target.value);
    }

    function handleChangeInventoryNum(event: any) {
        setInventoryNum(event.target.value);
    }

    function handleSerialNum(event: any) {
        setSerialNum(event.target.value);
    }

    function handleLifeTime(event: any) {
        setLifeTime(event.target.value);
    }

    function handlePrice(event: any) {
        setPrice(event.target.value);
    }

    function handleReleaseDate(event: any) {
        setReleaseDate(event.target.value);
    }

    function handleCommissioningDate(event: any) {
        setCommissioningDate(event.target.value);
    }

    function handleDistributionPlan(event: any) {
        setDistributionPlan(event.target.value);
    }

    function handleOperability(event: any) {
        setOperability(event.target.value);
    }

    function handleComposition(event: any) {
        setComposition(event.target.value);
    }

    function handleChangeDescription(event: any) {
        setDescription(event.target.value);
    }

    function handleChangeProvider(event: any) {
        setProvider(event.target.value);

        if (event.target.value === 0 || event.target.value === '') {
            setInvoiceNum("");
            setInvoiceDate("")
        }
    }

    function handleChangeInvoiceNum(event: any) {
        setInvoiceNum(event.target.value);
    }

    function handleChangeInvoiceDate(event: any) {
        setInvoiceDate(event.target.value);
    }

    const addOrUpdateTechnique = async () => {
        setChecked(true);

        if (isNotFillingFields()) {
            return;
        }

        const selectedProvider: IProvider | undefined = providers.find(x => x.id === provider);

        const data = {
            id: selectedTechnique ? selectedTechnique.id : 0,
            commissioningDate,
            distributionPlan,
            releaseDate,
            price,
            idBranch: idBranch,
            modelOperability: getBoolValue(modelOperability),
            serialNum,
            supply: {
                date: invoiceDate,
                invoiceNum,
                provider: {
                    id: selectedProvider ? selectedProvider.id : 0,
                    name: selectedProvider ? selectedProvider.name : '',
                },
            },
            equipment: {
                composition,
                description,
            },
            inventoryNum,
            model: {
                name: model,
                lifeTime,
                technique: {
                    name,
                },
            },
        } as ICommEquipment;

        if (selectedTechnique) {
            await editTechnique(data, idBranch);
        } else {
            await addTechnique(data, idBranch);
        }

        if (selectedTechnique) {
            handleClose();
        }

        resetFields();
    };

    const isNotFillingFields = () => {
        return isEmpty(name)
            || isEmpty(lifeTime)
            || isEmpty(modelOperability)
    };

    const resetFields = () => {
        setName('');
        setModel('');
        setSerialNum('');
        setInventoryNum('');
        setLifeTime('');
        setReleaseDate('');
        setCommissioningDate('');
        setDistributionPlan('');
        setOperability('');
        setComposition('');
        setDescription('');
        setProvider('');
        setInvoiceNum('');
        setInvoiceDate('');
        setPrice('');

        setChecked(false);
    };

    return (
        <Dialog
            open={open}
            TransitionComponent={Transition}
            maxWidth={'xl'}
            onClose={handleClose}
        >
            <DialogTitle>{`${selectedTechnique ? "Изменить" : "Добавить"} технику`}</DialogTitle>
            <DialogContent>
                <TextField
                    label={ReadableField.TECHNIQUE}
                    margin="normal"
                    fullWidth
                    required
                    value={name}
                    onChange={handleChangeName}
                    error={isChecked && isEmpty(name)}
                />

                <TextField
                    label={ReadableField.MODEL}
                    margin="normal"
                    fullWidth
                    required
                    value={model}
                    onChange={handleChangeModel}
                    error={isChecked && isEmpty(model)}
                />
                <TextField
                    label={ReadableField.SERIAL_NUM}
                    margin="normal"
                    className={classes.textField}
                    value={serialNum}
                    onChange={handleSerialNum}
                />

                <TextField
                    label={ReadableField.INVENTORY_NUM}
                    margin="normal"
                    className={classes.textField}
                    value={inventoryNum}
                    onChange={handleChangeInventoryNum}
                />
                <TextField
                    label={ReadableField.LIFE_TIME}
                    margin="normal"
                    required
                    InputProps={{
                        inputComponent: NumberFormatCustom,
                    }}
                    className={classes.textField}
                    value={lifeTime}
                    onChange={handleLifeTime}
                    error={isChecked && isEmpty(lifeTime)}
                />

                <TextField
                    label={ReadableField.PRICE}
                    margin="normal"
                    className={classes.textField}
                    InputProps={{
                        inputComponent: MoneyFormatCustom,
                    }}
                    value={price}
                    onChange={handlePrice}
                />

                <TextField
                    label={ReadableField.RELEASE_DATE}
                    type="date"
                    margin="normal"
                    className={classes.textField}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    value={releaseDate}
                    onChange={handleReleaseDate}
                />

                <TextField
                    label={ReadableField.COMMISSIONING_DATE}
                    type="date"
                    margin="normal"
                    className={classes.textField}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    value={commissioningDate}
                    onChange={handleCommissioningDate}
                />

                <TextField
                    label={ReadableField.DISTRIBUTION_PLAN}
                    type="date"
                    margin="normal"
                    className={classes.textField}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    value={distributionPlan}
                    onChange={handleDistributionPlan}
                />

                <div style={{display: 'flex', alignItems: 'center'}}>
                    <div style={{display: 'block'}}>
                        <FormLabel error={isChecked && isEmpty(modelOperability)} style={{fontSize: 12}}
                                   required>{ReadableField.MODEL_OPERABILITTY}</FormLabel>
                        <RadioGroup
                            aria-label="Operability"
                            name="operability"
                            row
                            value={getValueForRadioGroup(modelOperability)}
                            onChange={handleOperability}
                        >
                            <FormControlLabel value={ReadableField.YES} control={<Radio/>} label={ReadableField.YES}/>
                            <FormControlLabel value={ReadableField.NO} control={<Radio/>} label={ReadableField.NO}/>
                        </RadioGroup>
                    </div>
                </div>

                <TextField
                    label={ReadableField.EQUIPMENT}
                    margin="normal"
                    className={classes.textField}
                    value={composition}
                    onChange={handleComposition}
                />

                <TextField
                    label={ReadableField.DESCRIPTION}
                    margin="normal"
                    className={classes.textField}
                    value={description}
                    onChange={handleChangeDescription}
                />

                <div style={{display: 'block'}}>
                    <TextField
                        select
                        label={ReadableField.PROVIDER}
                        className={classes.textField}
                        margin="normal"
                        value={provider}
                        onChange={handleChangeProvider}
                    >
                        <MenuItem value={0}/>
                        {providers.map(provider =>
                            <MenuItem value={provider.id}>
                                {provider.name}
                            </MenuItem>
                        )}
                    </TextField>

                    <TextField
                        label={ReadableField.INVOICE_NUM}
                        margin="normal"
                        className={classes.textField}
                        value={provider === "" || provider === 0 ? "" : invoiceNum}
                        onChange={handleChangeInvoiceNum}
                        disabled={provider === "" || provider === 0}
                    />

                    <TextField
                        label={ReadableField.INVOICE_DATA}
                        type="date"
                        margin="normal"
                        className={classes.textField}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        value={provider === "" || provider === 0 ? "" : invoiceDate}
                        onChange={handleChangeInvoiceDate}
                        disabled={provider === "" || provider === 0}
                    />
                </div>
            </DialogContent>

            <DialogActions>
                <Button onClick={addOrUpdateTechnique} color="primary">
                    {selectedTechnique ? 'Изменить' : 'Добавить'}
                </Button>

                <Button onClick={handleClose} color="primary">
                    Отменить
                </Button>
            </DialogActions>
        </Dialog>
    );
};

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators({
        addTechnique,
        editTechnique,
        loadProviders,
    }, dispatch);

const mapStateToProps = (state: any): IStateProps => ({
    selectedTechnique: state.techniques.selected,
    providers: state.providers.items,
});

export default connect<IStateProps, IDispatchProps>(
    mapStateToProps,
    mapDispatchToProps,
)(withStyles(stylesForEditedDialog)(EditTechniqueDialog));

