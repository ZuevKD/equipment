import React from 'react';
import {IBranch, IColumn, ICommEquipment, ITableHeader, ReadableField} from "../../consts/consts";
import {bindActionCreators, Dispatch} from "redux";
import {
    deleteRadiostation,
    loadRadiostations,
    selectRadiostation,
} from "../../actions/acitons";
import {connect} from "react-redux";
import {Edit} from "@material-ui/icons";
import {TableRow} from "@material-ui/core";
import TableCell from "@material-ui/core/es/TableCell";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';
import EditRadiostationDialog from "./EditRadiostationDialog";
import {AbsentEntities} from "./AbsentEntities";
import {ExistEntities} from "./ExtistEntities";
import {renderCell} from "../common/Utils";
import DeleteDialog from "./DeleteDialog";

let headers: ITableHeader[] = [];

interface IState {
    show: boolean;
    showDeleteDialog: boolean;
    selectedForDelete: ICommEquipment | null;
}

interface IDispatchProps {
    loadRadiostations: (id: number) => any;
    selectRadiostation: (radiostation: ICommEquipment | null) => any;
    deleteRadiostation: (radiostation: ICommEquipment | null, idBranch: number) => any;
}

interface IStateProps {
    items: ICommEquipment[];
    selectedBranch: IBranch;
    selectedColumns: IColumn[];
}

class TableRadiostation extends React.Component<IDispatchProps & IStateProps, IState> {
    state: IState = {
        show: false,
        showDeleteDialog: false,
        selectedForDelete: null,
    };

    private isActiveColumn = (name: string): boolean => {
        const {selectedColumns} = this.props;
        const selectedColumn = selectedColumns.find(x => x.name === name);

        return selectedColumn ? selectedColumn.selected : true;
    };

    private createHeaders = () => {
        let i = 100;
        headers = [];

        const createHeader = (name: string) => {
            this.isActiveColumn(name) && headers.push({
                id: i,
                title: name,
                align: 'left'
            } as ITableHeader);
            i++;
        };

        createHeader(ReadableField.NAME);
        createHeader(ReadableField.MODEL);
        createHeader(ReadableField.DIAPASON);
        createHeader(ReadableField.PRESENCE_SC_DEVICE);
        createHeader(ReadableField.SPECIFICITY);
        createHeader(ReadableField.SERIAL_NUM);
        createHeader(ReadableField.INVENTORY_NUM);
        createHeader(ReadableField.LIFE_TIME);
        createHeader(ReadableField.DISTRIBUTION_PLAN);
        createHeader(ReadableField.RELEASE_DATE);
        createHeader(ReadableField.COMMISSIONING_DATE);
        createHeader(ReadableField.PRICE);
        createHeader(ReadableField.MODEL_OPERABILITTY);
        createHeader(ReadableField.PROVIDER);
        createHeader(ReadableField.INVOICE_NUM);
        createHeader(ReadableField.INVOICE_DATA);
        createHeader(ReadableField.EQUIPMENT);
        createHeader(ReadableField.DESCRIPTION);
        createHeader('');
    };

    private openDialog = (radiostation: ICommEquipment | null = null) => () => {
        if (radiostation !== null) {
            this.props.selectRadiostation(radiostation);
        } else {
            this.props.selectRadiostation(null);
        }

        this.setState({show: true});
    };

    private openDeleteDialog = (radiostation: ICommEquipment | null = null) => () => {
        if (radiostation !== null) {
            this.setState({showDeleteDialog: true, selectedForDelete: radiostation});
        }
    };

    private closeDialog = () => this.setState({show: false});
    private closeDeleteDialog = () => this.setState({showDeleteDialog: false});

    render() {
        this.createHeaders();
        return (
            <>
                {this.props.items.length ?
                    <ExistEntities
                        openAddDialog={this.openDialog}
                        nameEntities={ReadableField.RADIOSTATIONS}
                        headers={headers}
                        renderRows={this.renderRows}
                    />
                    :
                    <AbsentEntities openAddDialog={this.openDialog} nameEntities={ReadableField.RADIOSTATIONS}/>
                }
                <EditRadiostationDialog
                    handleClose={this.closeDialog}
                    open={this.state.show}
                    idBranch={this.props.selectedBranch.id}
                />

                <DeleteDialog
                    closeDialog={this.closeDeleteDialog}
                    isOpen={this.state.showDeleteDialog}
                    idBranch={this.props.selectedBranch.id}
                    deleteCommEquipment={this.onDelete}
                    commEquipment={this.state.selectedForDelete}
                />
            </>
        )
    }

    private renderRows = (classNames: string) => this.props.items.map(row => {
            let id = Math.random();
            return (
                <TableRow key={row.id} hover>
                    {renderCell(row.model.radiostation.name, classNames)}
                    {renderCell(row.model.name, classNames)}
                    {this.isActiveColumn(ReadableField.DIAPASON) && renderCell(row.model.radiostation.diapason.name, classNames)}
                    {this.isActiveColumn(ReadableField.PRESENCE_SC_DEVICE) && renderCell(row.model.radiostation.presenceScDevice, classNames)}
                    {this.isActiveColumn(ReadableField.SPECIFICITY) && renderCell(row.model.radiostation.specificity.name, classNames)}
                    {this.isActiveColumn(ReadableField.SERIAL_NUM) && renderCell(row.serialNum, classNames)}
                    {this.isActiveColumn(ReadableField.INVENTORY_NUM) && renderCell(row.inventoryNum, classNames)}
                    {this.isActiveColumn(ReadableField.LIFE_TIME) && renderCell(row.model.lifeTime, classNames)}
                    {this.isActiveColumn(ReadableField.DISTRIBUTION_PLAN) && renderCell(row.distributionPlan, classNames)}
                    {this.isActiveColumn(ReadableField.RELEASE_DATE) && renderCell(row.releaseDate, classNames)}
                    {this.isActiveColumn(ReadableField.COMMISSIONING_DATE) && renderCell(row.commissioningDate, classNames)}
                    {this.isActiveColumn(ReadableField.PRICE) && renderCell(row.price, classNames)}
                    {this.isActiveColumn(ReadableField.MODEL_OPERABILITTY) && renderCell(row.modelOperability, classNames)}
                    {this.isActiveColumn(ReadableField.PROVIDER) && renderCell(row.supply.provider.name, classNames)}
                    {this.isActiveColumn(ReadableField.INVOICE_NUM) && renderCell(row.supply.invoiceNum, classNames)}
                    {this.isActiveColumn(ReadableField.INVOICE_DATA) && renderCell(row.supply.date, classNames)}
                    {this.isActiveColumn(ReadableField.EQUIPMENT) && renderCell(row.equipment.composition, classNames)}
                    {this.isActiveColumn(ReadableField.DESCRIPTION) && renderCell(row.equipment.description, classNames)}
                    <TableCell
                        className={classNames}
                        scope="row"
                        align={'right'}
                        key={id}
                        style={{width: 100, fontSize: 11}}
                    >
                        <IconButton onClick={this.openDialog(row)}>
                            <Edit color="primary"/>
                        </IconButton>

                        <IconButton onClick={this.openDeleteDialog(row)}>
                            <DeleteIcon color={'error'}/>
                        </IconButton>
                    </TableCell>
                </TableRow>
            )
        }
    );

    private onDelete = (radiostation: ICommEquipment | null, idBranch: number) => {
        this.setState({
            showDeleteDialog: false,
            selectedForDelete: null,
        });

        this.props.deleteRadiostation(radiostation, idBranch);
    }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators({
        loadRadiostations,
        selectRadiostation,
        deleteRadiostation,
    }, dispatch);

const mapStateToProps = (state: any): IStateProps => ({
    items: state.radiostations.items,
    selectedBranch: state.branches.selected,
    selectedColumns: state.selectedColumns,
});

export default connect<IStateProps, IDispatchProps, null>(
    mapStateToProps,
    mapDispatchToProps,
)(TableRadiostation);

