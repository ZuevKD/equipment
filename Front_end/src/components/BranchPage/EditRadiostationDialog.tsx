import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import {TextField, withStyles} from "@material-ui/core";
import {
    ICommEquipment,
    IDiapason,
    IProvider,
    ISpecificity,
    ReadableField,
} from "../../consts/consts";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormLabel from "@material-ui/core/FormLabel";
import {connect} from "react-redux";
import {bindActionCreators, Dispatch} from "redux";
import {
    addRadiostation,
    editRadiostation,
    loadDiapasons,
    loadProviders,
    loadSpecificities
} from "../../actions/acitons";
import MenuItem from '@material-ui/core/MenuItem';
import {
    getBoolValue,
    getValueForRadioGroup,
    isEmpty,
    MoneyFormatCustom, NumberFormatCustom,
    stylesForEditedDialog,
    Transition
} from "../common/Utils";

interface IDispatchProps {
    addRadiostation: (data: ICommEquipment, branchId: number) => any;
    editRadiostation: (data: ICommEquipment, branchId: number) => any;
    loadProviders: () => any;
    loadSpecificities: () => any;
    loadDiapasons: () => any;
}

interface IStateProps {
    selectedRadiostation: ICommEquipment;
    providers: IProvider[],
    specificities: ISpecificity[],
    diapasons: IDiapason[],
}

interface IProps {
    handleClose: () => any;
    open: boolean;
    idBranch: number
    classes: {
        textField: string
    },
}

type TProps = IProps & IDispatchProps & IStateProps;

const EditRadiostationDialog = ({
                                    handleClose, open, classes, idBranch,
                                    selectedRadiostation, addRadiostation,
                                    editRadiostation, loadProviders, providers, loadDiapasons, loadSpecificities,
                                    diapasons, specificities,
                                }: TProps) => {
    const [name, setName] = React.useState('');
    const [model, setModel] = React.useState('');
    const [inventoryNum, setInventoryNum] = React.useState();
    const [serialNum, setSerialNum] = React.useState();
    const [lifeTime, setLifeTime] = React.useState();
    const [price, setPrice] = React.useState();
    const [releaseDate, setReleaseDate] = React.useState();
    const [commissioningDate, setCommissioningDate] = React.useState();
    const [distributionPlan, setDistributionPlan] = React.useState();
    const [modelOperability, setOperability] = React.useState();
    const [presenceScDevice, setPresenceScDevice] = React.useState();
    const [diapason, setDiapason] = React.useState();
    const [specificity, setSpecificity] = React.useState();
    const [composition, setComposition] = React.useState('');
    const [description, setDescription] = React.useState('');
    const [provider, setProvider] = React.useState();
    const [invoiceNum, setInvoiceNum] = React.useState();
    const [invoiceDate, setInvoiceDate] = React.useState();
    const [isChecked, setChecked] = React.useState(false);

    React.useEffect(() => {
        loadProviders();
        loadDiapasons();
        loadSpecificities();
    }, []);

    React.useEffect(() => {
        if (selectedRadiostation) {
            console.log(selectedRadiostation);
            const {model, inventoryNum, serialNum, supply, releaseDate, commissioningDate, distributionPlan, equipment, modelOperability, price} = selectedRadiostation;
            const {
                name: nameModel,
                lifeTime,
                radiostation,
            } = model;
            const {provider, date, invoiceNum} = supply;
            const {description, composition} = equipment;
            const {name, diapason, presenceScDevice, specificity} = radiostation;

            setName(name);
            setModel(nameModel);
            setSerialNum(serialNum);
            setInventoryNum(inventoryNum);
            setLifeTime(lifeTime);
            setReleaseDate(releaseDate);
            setCommissioningDate(commissioningDate);
            setDistributionPlan(distributionPlan);
            setOperability(modelOperability);
            setPresenceScDevice(presenceScDevice);
            setDiapason(diapason.id);
            setComposition(composition);
            setDescription(description);
            setSpecificity(specificity.id);
            setProvider(provider.id);
            setInvoiceNum(invoiceNum);
            setInvoiceDate(date);
            setPrice(price);
        } else {
            resetFields();
        }
    }, [selectedRadiostation]);

    function handleChangeName(event: any) {
        setName(event.target.value);
    }

    function handleChangeModel(event: any) {
        setModel(event.target.value);
    }

    function handleChangeInventoryNum(event: any) {
        setInventoryNum(event.target.value);
    }

    function handleSerialNum(event: any) {
        setSerialNum(event.target.value);
    }

    function handleLifeTime(event: any) {
        setLifeTime(event.target.value);
    }

    function handlePrice(event: any) {
        setPrice(event.target.value);
    }

    function handleReleaseDate(event: any) {
        setReleaseDate(event.target.value);
    }

    function handleCommissioningDate(event: any) {
        setCommissioningDate(event.target.value);
    }

    function handleDistributionPlan(event: any) {
        setDistributionPlan(event.target.value);
    }

    function handleOperability(event: any) {
        setOperability(event.target.value);
    }

    function handlePresenceScDevice(event: any) {
        setPresenceScDevice(event.target.value);
    }

    function handleComposition(event: any) {
        setComposition(event.target.value);
    }

    function handleDiapason(event: any) {
        setDiapason(event.target.value);
    }

    function handleChangeSpecificity(event: any) {
        setSpecificity(event.target.value);
    }

    function handleChangeDescription(event: any) {
        setDescription(event.target.value);
    }

    function handleChangeProvider(event: any) {
        setProvider(event.target.value);

        if (event.target.value === 0 || event.target.value === '') {
            setInvoiceNum("");
            setInvoiceDate("")
        }
    }

    function handleChangeInvoiceNum(event: any) {
        setInvoiceNum(event.target.value);
    }

    function handleChangeInvoiceDate(event: any) {
        setInvoiceDate(event.target.value);
    }

    const addOrUpdateRadiostation = async () => {
        setChecked(true);

        if (isNotFillingFields()) {
            return;
        }

        const selectedProvider: IProvider | undefined = providers.find(x => x.id === provider);
        const selectedDiapason: IDiapason | undefined = diapasons.find(x => x.id === diapason);
        const selectedSpecificity: ISpecificity | undefined = specificities.find(x => x.id === specificity);

        const data = {
            id: selectedRadiostation ? selectedRadiostation.id : 0,
            commissioningDate,
            distributionPlan,
            releaseDate,
            price,
            idBranch: idBranch,
            modelOperability: getBoolValue(modelOperability),
            serialNum,
            supply: {
                date: invoiceDate,
                invoiceNum,
                provider: {
                    id: selectedProvider ? selectedProvider.id : 0,
                    name: selectedProvider ? selectedProvider.name : '',
                },
            },
            equipment: {
                composition,
                description,
            },
            inventoryNum,
            model: {
                name: model,
                lifeTime,
                radiostation: {
                    diapason: {
                        id: selectedDiapason ? selectedDiapason.id : 0,
                        name: selectedDiapason ? selectedDiapason.name : '',
                    },
                    specificity: {
                        id: selectedSpecificity ? selectedSpecificity.id : 0,
                        name: selectedSpecificity ? selectedSpecificity.name : '',
                    },
                    name,
                    presenceScDevice: getBoolValue(presenceScDevice),
                },
            },
        } as ICommEquipment;

        if (selectedRadiostation) {
            await editRadiostation(data, idBranch);
        } else {
            await addRadiostation(data, idBranch);
        }

        resetFields();

        if (selectedRadiostation) {
            handleClose();
        }
    };

    const isNotFillingFields = () => {
        return isEmpty(name)
            || isEmpty(lifeTime)
            || isEmpty(modelOperability)
            || isEmpty(presenceScDevice)
            || isEmpty(diapason) || diapason === '';
    };

    const resetFields = () => {
        setName('');
        setModel('');
        setSerialNum('');
        setInventoryNum('');
        setLifeTime('');
        setReleaseDate('');
        setCommissioningDate('');
        setDistributionPlan('');
        setOperability('');
        setPresenceScDevice('');
        setDiapason('');
        setComposition('');
        setDescription('');
        setSpecificity('');
        setProvider('');
        setInvoiceNum('');
        setInvoiceDate('');
        setPrice('');

        setChecked(false);
    };

    return (
        <Dialog
            open={open}
            TransitionComponent={Transition}
            maxWidth={'xl'}
            onClose={handleClose}
        >
            <DialogTitle>{`${selectedRadiostation ? "Изменить" : "Добавить"} радиостанцию`}</DialogTitle>
            <DialogContent>
                <TextField
                    label={ReadableField.RADIOSTATION}
                    margin="normal"
                    fullWidth
                    required
                    value={name}
                    onChange={handleChangeName}
                    error={isChecked && isEmpty(name)}
                />
                <TextField
                    label={ReadableField.MODEL}
                    margin="normal"
                    fullWidth
                    required
                    value={model}
                    onChange={handleChangeModel}
                    error={isChecked && isEmpty(model)}
                />

                <TextField
                    label={ReadableField.SERIAL_NUM}
                    margin="normal"
                    className={classes.textField}
                    value={serialNum}
                    onChange={handleSerialNum}
                />

                <TextField
                    label={ReadableField.INVENTORY_NUM}
                    margin="normal"
                    className={classes.textField}
                    value={inventoryNum}
                    onChange={handleChangeInventoryNum}
                />

                <TextField
                    label={ReadableField.LIFE_TIME}
                    margin="normal"
                    required
                    className={classes.textField}
                    InputProps={{
                        inputComponent: NumberFormatCustom,
                    }}
                    value={lifeTime}
                    onChange={handleLifeTime}
                    error={isChecked && isEmpty(lifeTime)}
                />

                <TextField
                    label={ReadableField.PRICE}
                    margin="normal"
                    className={classes.textField}
                    InputProps={{
                        inputComponent: MoneyFormatCustom,
                    }}
                    value={price}
                    onChange={handlePrice}
                />

                <TextField
                    label={ReadableField.RELEASE_DATE}
                    type="date"
                    margin="normal"
                    className={classes.textField}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    value={releaseDate}
                    onChange={handleReleaseDate}
                />

                <TextField
                    label={ReadableField.COMMISSIONING_DATE}
                    type="date"
                    margin="normal"
                    className={classes.textField}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    value={commissioningDate}
                    onChange={handleCommissioningDate}
                />

                <TextField
                    label={ReadableField.DISTRIBUTION_PLAN}
                    type="date"
                    margin="normal"
                    className={classes.textField}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    value={distributionPlan}
                    onChange={handleDistributionPlan}
                />

                <div style={{display: 'flex', alignItems: 'center'}}>
                    <div style={{display: 'block'}}>
                        <FormLabel error={isChecked && isEmpty(modelOperability)} style={{fontSize: 12}}
                                   required>{ReadableField.MODEL_OPERABILITTY}</FormLabel>
                        <RadioGroup
                            aria-label="Operability"
                            name="operability"
                            row
                            value={getValueForRadioGroup(modelOperability)}
                            onChange={handleOperability}
                        >
                            <FormControlLabel value={ReadableField.YES} control={<Radio/>} label={ReadableField.YES}/>
                            <FormControlLabel value={ReadableField.NO} control={<Radio/>} label={ReadableField.NO}/>
                        </RadioGroup>
                    </div>
                    <div style={{display: 'block', paddingLeft: 50}}>
                        <FormLabel error={isChecked && isEmpty(presenceScDevice)} style={{fontSize: 12}}
                                   required>{ReadableField.PRESENCE_SC_DEVICE}</FormLabel>
                        <RadioGroup
                            aria-label="presenceScDevice"
                            name="presenceScDevice"
                            row
                            value={getValueForRadioGroup(presenceScDevice)}
                            onChange={handlePresenceScDevice}
                        >
                            <FormControlLabel value={ReadableField.YES} control={<Radio/>} label={ReadableField.YES}/>
                            <FormControlLabel value={ReadableField.NO} control={<Radio/>} label={ReadableField.NO}/>
                        </RadioGroup>
                    </div>
                </div>

                <TextField
                    select
                    label={ReadableField.DIAPASON}
                    className={classes.textField}
                    margin="normal"
                    required
                    value={diapason}
                    onChange={handleDiapason}
                    error={isChecked && isEmpty(diapason)}
                >
                    <MenuItem value=""> </MenuItem>
                    {diapasons.map(diapason =>
                        <MenuItem value={diapason.id}>
                            {diapason.name}
                        </MenuItem>
                    )}
                </TextField>

                <TextField
                    select
                    label={ReadableField.SPECIFICITY}
                    className={classes.textField}
                    margin="normal"
                    value={specificity}
                    onChange={handleChangeSpecificity}
                >
                    <MenuItem value=""> </MenuItem>
                    {specificities.map(specificity =>
                        <MenuItem value={specificity.id}>
                            {specificity.name}
                        </MenuItem>
                    )}
                </TextField>

                <TextField
                    label={ReadableField.EQUIPMENT}
                    margin="normal"
                    className={classes.textField}
                    value={composition}
                    onChange={handleComposition}
                />

                <TextField
                    label={ReadableField.DESCRIPTION}
                    margin="normal"
                    className={classes.textField}
                    value={description}
                    onChange={handleChangeDescription}
                />

                <div style={{display: 'block'}}>
                    <TextField
                        select
                        label={ReadableField.PROVIDER}
                        className={classes.textField}
                        margin="normal"
                        value={provider}
                        onChange={handleChangeProvider}
                    >
                        <MenuItem value={0}/>
                        {providers.map(provider =>
                            <MenuItem value={provider.id}>
                                {provider.name}
                            </MenuItem>
                        )}
                    </TextField>

                    <TextField
                        label={ReadableField.INVOICE_NUM}
                        margin="normal"
                        className={classes.textField}
                        value={provider === "" || provider === 0 ? "" : invoiceNum}
                        onChange={handleChangeInvoiceNum}
                        disabled={provider === "" || provider === 0}
                    />

                    <TextField
                        label={ReadableField.INVOICE_DATA}
                        type="date"
                        margin="normal"
                        className={classes.textField}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        value={provider === "" || provider === 0 ? "" : invoiceDate}
                        onChange={handleChangeInvoiceDate}
                        disabled={provider === "" || provider === 0}
                    />
                </div>
            </DialogContent>
            <DialogActions>
                <Button onClick={addOrUpdateRadiostation} color="primary">
                    {selectedRadiostation ? 'Изменить' : 'Добавить'}
                </Button>
                <Button onClick={handleClose} color="primary">
                    Отменить
                </Button>
            </DialogActions>
        </Dialog>
    );
};

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators({
        addRadiostation,
        editRadiostation,
        loadProviders,
        loadSpecificities,
        loadDiapasons,
    }, dispatch);

const mapStateToProps = (state: any): IStateProps => ({
    selectedRadiostation: state.radiostations.selected,
    providers: state.providers.items,
    specificities: state.specificities,
    diapasons: state.diapasons,
});

export default connect<IStateProps, IDispatchProps>(
    mapStateToProps,
    mapDispatchToProps,
)(withStyles(stylesForEditedDialog)(EditRadiostationDialog));

