import React from "react";
import Input from "@material-ui/core/Input";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import ListItemText from "@material-ui/core/ListItemText";
import Select from "@material-ui/core/Select";
import Checkbox from "@material-ui/core/Checkbox";
import {Theme, withStyles} from "@material-ui/core";
import createStyles from "@material-ui/core/styles/createStyles";
import {IColumn, columnNames} from "../../consts/consts";
import {bindActionCreators, Dispatch} from "redux";
import { selectColumns} from "../../actions/acitons";
import {connect} from "react-redux";

const styles = (theme: Theme) => createStyles({
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 150,
        maxWidth: 150
    }
});

interface IStateProps {
    selectedColumns: IColumn[];
}

interface IDispatchProps {
    selectColumns: (columns: IColumn[]) => any;
}

interface IProps {
    classes: {
        formControl: string,
    },
}

type TProps = IProps & IDispatchProps & IStateProps;

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250
        }
    }
};

const SelectDisplayColumn = ({classes, selectColumns, selectedColumns}: TProps) => {
    function handleChange(event: any) {
        const name = event.target.value[event.target.value.length - 1];

        const newColumns: IColumn[] = selectedColumns.map(x => {
            if (x.name === name) {
                x.selected = !x.selected;
            }

            return x;
        });

        selectColumns(newColumns);
    }

    const isSelectColumn = (column: IColumn) => {
        let foundColumn = null;

        for (let i = 0; i < selectedColumns.length; i++) {

            if (column.name === selectedColumns[i].name) {
                foundColumn = selectedColumns[i];
            }
        }

        return foundColumn ? foundColumn.selected : false;
    };

    return (
        <FormControl className={classes.formControl}>
            <Select
                multiple
                displayEmpty
                value={selectedColumns}
                onChange={handleChange}
                input={<Input id="select-multiple-checkbox"/>}
                renderValue={() => "Показывать"}
                MenuProps={MenuProps}
            >
                {columnNames.map((column: IColumn) => (
                    <MenuItem key={column.name} value={column.name}>
                        <Checkbox checked={isSelectColumn(column)}/>
                        <ListItemText primary={column.name}/>
                    </MenuItem>
                ))}
            </Select>
        </FormControl>
    );
};

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators({
        selectColumns,
    }, dispatch);

const mapStateToProps = (state: any): IStateProps => ({
    selectedColumns: state.selectedColumns,
});

export default connect<IStateProps, IDispatchProps>(
    mapStateToProps,
    mapDispatchToProps,
)(withStyles(styles)(SelectDisplayColumn));