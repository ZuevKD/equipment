import React from 'react';
import TableRadiostation from "./TableRadiostation";
import TableTechnique from "./TableTechnique";
import {connect} from "react-redux";
import {IBranch, IDepartment} from "../../consts/consts";
import AppTabs from "../common/AppTabs";
import PasswordDialog from "../common/PasswordDialog";

interface IStateProps {
    selectedBranch: IBranch;
    selectedDepartment: IDepartment;
}

const BranchPage = ({selectedBranch, selectedDepartment}: IStateProps) => (
    <>
        <AppTabs/>

        <div style={{textAlign: 'center'}}>
            <h5 style={{marginTop: 5, marginBottom: 0}}>
                {selectedDepartment ? selectedDepartment.name : ''}
            </h5>
        </div>

        <div style={{textAlign: 'center'}}>
            <h3 style={{marginTop: 5, marginBottom: 0}}>
                {selectedBranch ? selectedBranch.name : ''}
            </h3>
        </div>

        <TableRadiostation/>
        <TableTechnique/>

        <PasswordDialog/>
    </>
);

const mapStateToProps = (state: any): IStateProps => ({
    selectedBranch: state.branches.selected,
    selectedDepartment: state.departments.selected,
});

export default connect<IStateProps>(
    mapStateToProps,
)(BranchPage);
