import React from 'react';
import CommonTable from "../common/CommonTable";
import {IBranch, IDepartment, ITableHeader} from "../../consts/consts";
import {TableRow} from "@material-ui/core";
import {Edit} from "@material-ui/icons";
import TableCell from "@material-ui/core/es/TableCell";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';
import {bindActionCreators, Dispatch} from "redux";
import {
    deleteBranch,
    loadBranch,
    loadRadiostations,
    loadTechniques,
    selectBranch,
    selectBranchForEdit,
} from "../../actions/acitons";
import {connect} from "react-redux";
import {Link} from 'react-router-dom'

const headers = [
    {id: 0, title: 'Службы', align: 'left'} as ITableHeader,
    {id: 1, title: '', align: 'right'} as ITableHeader
];

interface IDispatchProps {
    loadRadiostations: (id: number) => any;
    loadTechniques: (id: number) => any;
    loadBranch: (id: number) => any;
    selectBranch: (branch: IBranch) => any;
    selectBranchForEdit: (branch: IBranch) => any;
    deleteBranch: (department: IDepartment, id: number) => any;
}

interface IStateProps {
    items: IBranch[];
    selectedDepartment: IDepartment;
}

const LinkToBranch = (props: any) => <Link to="/branch" {...props} />;

class TableBranches extends React.Component<IDispatchProps & IStateProps, {}> {
    componentWillUpdate(nextProps: Readonly<IDispatchProps & IStateProps>, nextState: Readonly<{}>, nextContext: any): void {
        if (nextProps.selectedDepartment !== null && nextProps.selectedDepartment !== this.props.selectedDepartment) {
            this.props.loadBranch(nextProps.selectedDepartment.id);
        }
    }

    render() {
        return (
            <CommonTable
                headers={headers}
                renderRows={this.renderRows}
                height={627}
            />
        );
    }

    private renderRows = (classNames: string) => this.props.items.map(row => {
            const {loadTechniques, loadRadiostations, selectBranch} = this.props;
            return (
                <TableRow key={row.id} hover>
                    <TableCell
                        className={classNames}
                        component={LinkToBranch}
                        scope="row"
                        variant="body"
                        onClick={() => {
                            selectBranch(row);
                            loadTechniques(row.id);
                            loadRadiostations(row.id)
                        }}
                    >
                        {row.name}
                    </TableCell>
                    <TableCell
                        className={classNames}
                        component="th"
                        scope="row"
                        align={'right'}
                        style={{width: 100}}
                    >
                        <IconButton onClick={this.onClickEdit(row)}>
                            <Edit color="primary"/>
                        </IconButton>
                        <IconButton onClick={this.onClickDelete(this.props.selectedDepartment, row.id)}>
                            <DeleteIcon color={'error'}/>
                        </IconButton>
                    </TableCell>
                </TableRow>
            )
        }
    );

    private onClickEdit = (branch: IBranch) => (event: any) => {
        event.stopPropagation();
        this.props.selectBranchForEdit(branch);
    };

    private onClickDelete = (department: IDepartment, id: number) => (event: any) => {
        event.stopPropagation();
        this.props.deleteBranch(department, id)
    };
}

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators({
        loadRadiostations,
        loadTechniques,
        selectBranch,
        loadBranch,
        selectBranchForEdit,
        deleteBranch,
    }, dispatch);

const mapStateToProps = (state: any): IStateProps => ({
    items: state.branches.items,
    selectedDepartment: state.departments.selected,
});

export default connect<IStateProps, IDispatchProps, null>(
    mapStateToProps,
    mapDispatchToProps,
)(TableBranches);
