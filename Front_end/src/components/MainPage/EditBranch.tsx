import React from 'react';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import {Grid, TextField} from "@material-ui/core";
import {connect} from "react-redux";
import {bindActionCreators, Dispatch} from "redux";
import {addBranch, updateBranch} from "../../actions/acitons";
import {Edit} from "@material-ui/icons";
import {IBranch, IDepartment, ReadableField} from "../../consts/consts";

interface IDispatchProps {
    addBranch: (name: string, idDepartment: number) => any;
    updateBranch: (idDepartment: number, id: number, name: string) => any;
}

interface IStateProps {
    isEdit: boolean;
    selected: IBranch;
    selectedDepartment: IDepartment;
}

type TProps = IDispatchProps & IStateProps;

interface IState {
    nameValue: string,
}

class EditBranch extends React.Component<TProps, IState> {
    state: IState = {
        nameValue: '',
    };

    componentDidUpdate(prevProps: Readonly<TProps>): void {
        if ((prevProps.isEdit !== this.props.isEdit || prevProps.selected !== this.props.selected) ) {
            let value = "";

            if (this.props.isEdit) {
                value = this.props.selected.name;
            }

            this.setState({
                nameValue: value,
            })
        }
    }

    private onChange = (event: any) => this.setState({nameValue: event.target.value});

    private onClickAdd = () => {
        this.props.addBranch(this.state.nameValue, this.props.selectedDepartment.id);
        this.setState({
            nameValue: '',
        })
    };

    private onClickUpdate = () => {
        const {updateBranch, selectedDepartment, selected} = this.props;

        updateBranch(selectedDepartment.id, selected.id, this.state.nameValue);
        this.setState({
            nameValue: '',
        })
    };

    render() {
        return (
            <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
            >
                <TextField
                    id="standard-name"
                    label={ReadableField.BRANCH}
                    value={this.state.nameValue}
                    onChange={this.onChange}
                    margin="normal"
                    fullWidth
                />

                {this.props.isEdit ?
                    <Fab
                        color="secondary"
                        aria-label="Edit"
                        style={{marginTop: 16}}
                        onClick={this.onClickUpdate}
                    >
                        <Edit />
                    </Fab>
                    :
                    <Fab
                        color="primary"
                        aria-label="Add"
                        style={{marginTop: 16}}
                        onClick={this.onClickAdd}
                    >
                        <AddIcon />
                    </Fab>
                }
            </Grid>
        )
    }
}

const mapStateToProps = (state: any): IStateProps => ({
    isEdit: state.branches.isEdit,
    selected: state.branches.selected,
    selectedDepartment: state.departments.selected,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators({
        addBranch,
        updateBranch,
    }, dispatch);

export default connect<IStateProps, IDispatchProps, null>(
    mapStateToProps,
    mapDispatchToProps,
)(EditBranch);
