import React from 'react';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import {Grid, TextField} from "@material-ui/core";
import {connect} from "react-redux";
import {bindActionCreators, Dispatch} from "redux";
import {
    addDepartment,
    updateDepartment,
} from "../../actions/acitons";
import {Edit} from "@material-ui/icons";
import {IDepartment, ReadableField} from "../../consts/consts";

interface IDispatchProps {
    addDepartment: (name: string) => any;
    updateDepartment: (id: number, name: string) => any;
}

interface IStateProps {
    isEdit: boolean;
    selected: IDepartment;
}

type TProps = IDispatchProps & IStateProps;

interface IState {
    nameValue: string,
}

class EditDepartment extends React.Component<TProps, IState> {
    state: IState = {
        nameValue: '',
    };

    componentDidUpdate(prevProps: Readonly<TProps>): void {
        if (prevProps.isEdit !== this.props.isEdit || prevProps.selected !== this.props.selected) {
            let value = "";

            if (this.props.isEdit) {
                value = this.props.selected.name;
            }

            this.setState({
                nameValue: value,
            })
        }
    }

    private onChange = (event: any) => this.setState({nameValue: event.target.value});

    private onClickAdd = () => {
        this.props.addDepartment(this.state.nameValue);
        this.setState({
            nameValue: '',
        })
    };

    private onClickUpdate = () => {
        this.props.updateDepartment(this.props.selected.id, this.state.nameValue);
        this.setState({
            nameValue: '',
        })
    };

    render() {
        return (
            <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
            >
                <TextField
                    id="standard-name"
                    label={ReadableField.DEPARTMENT}
                    value={this.state.nameValue}
                    onChange={this.onChange}
                    margin="normal"
                    fullWidth
                />

                {this.props.isEdit ?
                    <Fab
                        color="secondary"
                        aria-label="Edit"
                        style={{marginTop: 16}}
                        onClick={this.onClickUpdate}
                    >
                        <Edit/>
                    </Fab>
                    :
                    <Fab
                        color="primary"
                        aria-label="Add"
                        style={{marginTop: 16}}
                        onClick={this.onClickAdd}
                    >
                        <AddIcon/>
                    </Fab>
                }
            </Grid>
        )
    }
}

const mapStateToProps = (state: any): IStateProps => ({
    isEdit: state.departments.isEdit,
    selected: state.departments.selected,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators({
        addDepartment,
        updateDepartment,
    }, dispatch);

export default connect<IStateProps, IDispatchProps, null>(
    mapStateToProps,
    mapDispatchToProps,
)(EditDepartment);


