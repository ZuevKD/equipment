import React from 'react';
import TableDepartment from "./TableDepartment";
import EditDepartment from "./EditDepartment";
import EditBranch from "./EditBranch";
import {Grid} from "@material-ui/core";
import TableBranches from "./TableBranches";
import AppTabs from "../common/AppTabs";
import PasswordDialog from "../common/PasswordDialog";

const MainPage = () => (
    <>
        <AppTabs/>

        <Grid
            container
            spacing={24}
            direction="row"
            justify="center"
            alignItems="center"
            style={{width: '100%'}}
        >
            <Grid item xs={4} style={{marginTop: 16}}>
                <TableDepartment/>

                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                >
                    <EditDepartment/>
                </Grid>
            </Grid>

            <Grid item xs={4} style={{marginTop: 16}}>
                <TableBranches/>

                <Grid
                    container
                    direction="column"
                    justify="center"
                    alignItems="center"
                >
                    <EditBranch/>
                </Grid>
            </Grid>
        </Grid>

        <PasswordDialog/>
    </>
);

export default (MainPage);
