import React from 'react';
import CommonTable from "../common/CommonTable";
import {IDepartment, ITableHeader} from "../../consts/consts";
import {bindActionCreators, Dispatch} from "redux";
import {
    deleteDepartment,
    loadDepartment,
    selectDepartment,
    selectDepartmentForEdit
} from "../../actions/acitons";
import {connect} from "react-redux";
import {Edit} from "@material-ui/icons";
import {TableRow} from "@material-ui/core";
import TableCell from "@material-ui/core/es/TableCell";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';
import {isEqual} from 'lodash';
import axios from "axios";

//Шапка таблицы
const headers = [
    {id: 0, title: 'Подразделения', align: 'left'} as ITableHeader,
    {id: 1, title: '', align: 'right'} as ITableHeader
];

interface IDispatchProps {
    loadDepartment: () => any;
    selectDepartment: (department: IDepartment) => any;
    selectDepartmentForEdit: (department: IDepartment) => any;
    deleteDepartment: (id: number) => any;
}

interface IStateProps {
    items: IDepartment[];
    selectedDepartment: IDepartment;
}

class TableDepartment extends React.Component<IDispatchProps & IStateProps, {}> {
    componentDidMount(): void {
        this.props.loadDepartment();
        axios.post(`/api/auth/signup`, {
            "name": "ZKD132",
            "username": "ZKD132",
            "email": "zuev.kirill95@yandex.ru",
            "password": "124356"
        })
    }

    componentWillUpdate(nextProps: Readonly<IDispatchProps & IStateProps>, nextState: Readonly<{}>, nextContext: any): void {
        if (!isEqual(nextProps.items, this.props.items)) {
            this.props.loadDepartment();
        }
    }

    //Таблица
    render() {
        return (
            <CommonTable
                headers={headers}
                renderRows={this.renderRows}
                height={627}
            />
        );
    }

    //Строки в таблице
    private renderRows = (classNames: string) => this.props.items.map(row => {
            const {selectedDepartment} = this.props;

            return (
                <TableRow
                    key={row.id}
                    hover
                    onClick={() => this.props.selectDepartment(row)}
                    selected={selectedDepartment && row.id === selectedDepartment.id}
                >
                    <TableCell
                        className={classNames}
                        scope="row"
                        variant="body"
                    >
                        {row.name}
                    </TableCell>
                    <TableCell
                        className={classNames}
                        scope="row"
                        align={'right'}
                    >
                        <IconButton onClick={this.onClickEdit(row)}>
                            <Edit color="primary"/>
                        </IconButton>
                        <IconButton onClick={this.onClickDelete(row.id)}>
                            <DeleteIcon color={'error'}/>
                        </IconButton>
                    </TableCell>
                </TableRow>
            )
        }
    );

    private onClickEdit = (department: IDepartment) => (event: any) => {
        console.log(department);
        event.stopPropagation();
        this.props.selectDepartmentForEdit(department);
    };

    private onClickDelete = (id: number) => (event: any) => {
        event.stopPropagation();
        this.props.deleteDepartment(id)
    };
}

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators({
        loadDepartment,
        selectDepartment,
        selectDepartmentForEdit,
        deleteDepartment,
    }, dispatch);

const mapStateToProps = (state: any): IStateProps => ({
    items: state.departments.items,
    selectedDepartment: state.departments.selected,
});

export default connect<IStateProps, IDispatchProps, null>(
    mapStateToProps,
    mapDispatchToProps,
)(TableDepartment);

