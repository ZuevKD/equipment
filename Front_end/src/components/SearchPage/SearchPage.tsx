import React from 'react';
import AppTabs from "../common/AppTabs";
import classNames from 'classnames';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import {ICommEquipment, IFilter, ReadableField} from "../../consts/consts";
import {TextField, Theme, withStyles} from "@material-ui/core";
import createStyles from "@material-ui/core/styles/createStyles";
import {Search} from "@material-ui/icons";
import SelectDisplayColumn from "../BranchPage/SelectDisplayColumn";
import Table from "./Table";
import {bindActionCreators, Dispatch} from "redux";
import {createReport, getAllTechniques} from "../../actions/acitons";
import {connect} from "react-redux";
import Button from '@material-ui/core/Button';
import PasswordDialog from "../common/PasswordDialog";

const drawerWidth = 240;

const styles = (theme: Theme) => createStyles({
    root: {
        display: 'flex',
    },
    appBar: {
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 20,
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: '0 8px',
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    },
    content: {
        flexGrow: 1,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        marginLeft: -drawerWidth,
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginLeft: 0,
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
});

interface IProps {
    classes: {
        appBar: string,
        appBarShift: string,
        menuButton: string,
        hide: string,
        drawer: string,
        drawerPaper: string,
        drawerHeader: string,
        content: string,
        contentShift: string,
        root: string,
        textField: string,
    },
}

interface IDispatchProps {
    getAllTechniques: (filter: IFilter) => any;
    createReport: (filter: IFilter) => any;
}

interface IStateProps {
    commEquipments: ICommEquipment[],
}

type TProps = IProps & IDispatchProps & IStateProps;

const SearchPage = (props: TProps) => {
    const {classes, getAllTechniques, commEquipments} = props;
    const [open, setOpen] = React.useState(true);
    const [filter, setFilter] = React.useState({} as IFilter);

    function handleDrawerOpen() {
        setOpen(true);
    }

    function handleDrawerClose() {
        setOpen(false);
    }

    const handleChangeFilter = (nameFilter: string) => ( event: any) =>{
        setFilter({...filter, [nameFilter]: event.target.value});
    };

    const handleSearchTechniques = () => {
        getAllTechniques(filter);
    };

    const handleCreateReport =() => {
        if (filter !== null) {
            createReport(filter);
        }
    };

    return (
        <>
            <div className={classes.root}>
                <AppTabs classNames={classNames(classes.appBar, {
                        [classes.appBarShift]: open,
                    }
                )}
                         position={'fixed'}
                />

                <Drawer
                    className={classes.drawer}
                    variant="persistent"
                    anchor="left"
                    open={open}
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                >
                    <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
                        <text style={{marginLeft: 8}}> Поиск</text>
                        <div>
                            <IconButton onClick={handleDrawerClose}>
                                {open ? <ChevronLeftIcon/> : <ChevronRightIcon/>}
                            </IconButton>
                        </div>
                    </div>
                    <Divider/>
                    <TextField
                        label={ReadableField.DEPARTMENT}
                        margin="normal"
                        className={classes.textField}
                        value={filter.department}
                        onChange={handleChangeFilter('department')}
                    />

                    <TextField
                        label={ReadableField.BRANCH}
                        margin="normal"
                        className={classes.textField}
                        value={filter.branch}
                        onChange={handleChangeFilter('branch')}
                    />

                    <TextField
                        label={ReadableField.NAME}
                        margin="normal"
                        className={classes.textField}
                        value={filter.technique}
                        onChange={handleChangeFilter('technique')}
                    />

                    <TextField
                        label={ReadableField.MODEL}
                        margin="normal"
                        className={classes.textField}
                        value={filter.model}
                        onChange={handleChangeFilter('model')}
                    />

                    <TextField
                        label={ReadableField.RELEASE_DATE}
                        type="date"
                        className={classes.textField}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        value={filter.date}
                        onChange={handleChangeFilter('date')}
                    />

                    <TextField
                        label={ReadableField.DISTRIBUTION_PLAN}
                        type="date"
                        className={classes.textField}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        value={filter.distributionPlan}
                        onChange={handleChangeFilter('distributionPlan')}
                    />

                    <TextField
                        label={ReadableField.SERIAL_NUM}
                        margin="normal"
                        className={classes.textField}
                        value={filter.serialNum}
                        onChange={handleChangeFilter('serialNum')}
                    />

                    <TextField
                        label={ReadableField.INVENTORY_NUM}
                        margin="normal"
                        className={classes.textField}
                        value={filter.inventoryNum}
                        onChange={handleChangeFilter('inventoryNum')}
                    />

                    <div style={{display: "flex", justifyContent: 'center'}}>
                        <IconButton onClick={handleSearchTechniques}>
                            <Search/>
                        </IconButton>
                    </div>

                    <Button variant="contained" style={{margin: 16}} onClick = {handleCreateReport}>
                        Сформировать отчет
                    </Button>
                </Drawer>
                <main
                    className={classNames(classes.content, {
                        [classes.contentShift]: open,
                    })}
                >
                    <div className={classes.drawerHeader}/>

                    <div style={{display: "flex", justifyContent: 'center'}}>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={handleDrawerOpen}
                            className={classNames(classes.menuButton, open && classes.hide)}
                        >
                            <MenuIcon/>
                        </IconButton>

                        <p>
                            Количество: {commEquipments.length}
                        </p>

                        <div style={{marginLeft: 'auto'}}>
                            <SelectDisplayColumn/>
                        </div>
                    </div>

                    <Table
                        items={[...commEquipments] as ICommEquipment[]}
                    />
                </main>
            </div>

            <PasswordDialog/>
        </>
    );
};

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators({
        getAllTechniques: getAllTechniques,
        createReport: createReport,
    }, dispatch);

const mapStateToProps = (state: any): IStateProps => ({
    commEquipments: state.allTechniques.commEquipments,
});

export default connect<IStateProps, IDispatchProps>(
    mapStateToProps,
    mapDispatchToProps,
)(withStyles(styles)(SearchPage));
