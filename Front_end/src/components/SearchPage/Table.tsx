import React from 'react';
import {
    IColumn,
    ICommEquipment,
    IRadiostation,
    ITableHeader,
    ITechnique,
    ReadableField
} from "../../consts/consts";
import {TableRow} from "@material-ui/core";
import {renderCell} from "../common/Utils";
import CommonTable from "../common/CommonTable";
import {connect} from "react-redux";

let headers: ITableHeader[] = [];

interface IState {
    show: boolean;
    items: ITechnique[] | IRadiostation[];
}

interface IStateProps {
    selectedColumns: IColumn[];
}

interface IProps {
    radiostations?: IRadiostation[];
    techniques?: ITechnique[];
    items?: ICommEquipment[];
}

type TProps = IStateProps & IProps;

class Table extends React.Component<TProps, IState> {
    state: IState = {
        show: false,
        items: [],
    };

    render() {
        this.createHeaders();
        return (
            <>
                <CommonTable
                    headers={headers}
                    renderRows={this.renderRows}
                    height={''}
                />
            </>
        )
    }

    private isActiveColumn = (name: string): boolean => {
        const {selectedColumns} = this.props;
        const selectedColumn = selectedColumns.find(x => x.name === name);

        return selectedColumn ? selectedColumn.selected : true;
    };

    private createHeaders = () => {
        let i = 100;
        headers = [];

        const createHeader = (name: string) => {
            this.isActiveColumn(name) && headers.push({
                id: i,
                title: name,
                align: 'left'
            } as ITableHeader);
            i++;
        };

        createHeader(ReadableField.DEPARTMENT);
        createHeader(ReadableField.BRANCH);
        createHeader(ReadableField.NAME);
        createHeader(ReadableField.MODEL);
        createHeader(ReadableField.DIAPASON);
        createHeader(ReadableField.PRESENCE_SC_DEVICE);
        createHeader(ReadableField.SPECIFICITY);
        createHeader(ReadableField.SERIAL_NUM);
        createHeader(ReadableField.INVENTORY_NUM);
        createHeader(ReadableField.LIFE_TIME);
        createHeader(ReadableField.DISTRIBUTION_PLAN);
        createHeader(ReadableField.RELEASE_DATE);
        createHeader(ReadableField.COMMISSIONING_DATE);
        createHeader(ReadableField.PRICE);
        createHeader(ReadableField.MODEL_OPERABILITTY);
        createHeader(ReadableField.PROVIDER);
        createHeader(ReadableField.INVOICE_NUM);
        createHeader(ReadableField.INVOICE_DATA);
        createHeader(ReadableField.EQUIPMENT);
        createHeader(ReadableField.DESCRIPTION);
    };

    private renderRows = (classNames: string) => {
        return this.props.items ? this.props.items.map(row => {
                const {model, inventoryNum, serialNum, supply, releaseDate, commissioningDate, distributionPlan, equipment, nameBranch, nameDepartment, modelOperability, price} = row;
                const {
                    name: nameModel,
                    lifeTime,
                    radiostation,
                    technique,
                } = model;
                return (
                    <TableRow key={1} hover>
                        {renderCell(nameDepartment, classNames)}
                        {renderCell(nameBranch, classNames)}
                        {renderCell(technique === null ? radiostation.name : technique.name, classNames)}
                        {renderCell(nameModel, classNames)}
                        {this.isActiveColumn(ReadableField.DIAPASON) && (radiostation && radiostation.diapason !== null) ? renderCell(radiostation.diapason.name, classNames) : renderCell('', classNames)}
                        {this.isActiveColumn(ReadableField.PRESENCE_SC_DEVICE) && (radiostation && radiostation.presenceScDevice !== null) ? renderCell(radiostation.presenceScDevice, classNames) : renderCell('', classNames)}
                        {this.isActiveColumn(ReadableField.SPECIFICITY) && (radiostation && radiostation.specificity) ? renderCell(radiostation.specificity.name, classNames) : renderCell('', classNames)}
                        {this.isActiveColumn(ReadableField.SERIAL_NUM) && renderCell(serialNum, classNames)}
                        {this.isActiveColumn(ReadableField.INVENTORY_NUM) && renderCell(inventoryNum, classNames)}
                        {this.isActiveColumn(ReadableField.LIFE_TIME) && renderCell(lifeTime, classNames)}
                        {this.isActiveColumn(ReadableField.DISTRIBUTION_PLAN) && renderCell(distributionPlan, classNames)}
                        {this.isActiveColumn(ReadableField.RELEASE_DATE) && renderCell(releaseDate, classNames)}
                        {this.isActiveColumn(ReadableField.COMMISSIONING_DATE) && renderCell(commissioningDate, classNames)}
                        {this.isActiveColumn(ReadableField.PRICE) && renderCell(price, classNames)}
                        {this.isActiveColumn(ReadableField.MODEL_OPERABILITTY) && renderCell(modelOperability, classNames)}
                        {this.isActiveColumn(ReadableField.PROVIDER) && renderCell(supply.provider.name, classNames)}
                        {this.isActiveColumn(ReadableField.INVOICE_NUM) && renderCell(supply.invoiceNum, classNames)}
                        {this.isActiveColumn(ReadableField.INVOICE_DATA) && renderCell(supply.date, classNames)}
                        {this.isActiveColumn(ReadableField.EQUIPMENT) && renderCell(equipment.composition, classNames)}
                        {this.isActiveColumn(ReadableField.DESCRIPTION) && renderCell(equipment.description, classNames)}
                    </TableRow>
                )
            }
        ) : []
    }
}

const mapStateToProps = (state: any): IStateProps => ({
    selectedColumns: state.selectedColumns,
});

export default connect<IStateProps, null, null>(
    mapStateToProps,
)(Table);

