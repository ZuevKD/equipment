import React from 'react';
import {Grid} from "@material-ui/core";
import Table from "./Table";
import EditProvider from "./EditProvider";
import AppTabs from "../common/AppTabs";
import PasswordDialog from "../common/PasswordDialog";

const ProviderListPage = () => (
    <>
        <AppTabs/>

        <Grid
            container
            spacing={24}
            direction="column"
            justify="center"
            alignItems="center"
            style={{width: '100%'}}
        >
            <Grid item style={{marginTop: 16}}>
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                    style={{width: 800}}
                >
                    <Table/>

                    <EditProvider/>
                </Grid>
            </Grid>
        </Grid>

        <PasswordDialog/>
    </>
);

export default (ProviderListPage);
