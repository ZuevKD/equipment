import React from 'react';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import {Grid, TextField} from "@material-ui/core";
import {connect} from "react-redux";
import {bindActionCreators, Dispatch} from "redux";
import {addProvider, selectProvider, updateProvider} from "../../actions/acitons";
import {Edit} from "@material-ui/icons";
import {IProvider, ReadableField} from "../../consts/consts";

interface IDispatchProps {
    addProvider: (name: string) => any;
    updateProvider: (id: number, name: string) => any;
    selectProvider: (provider: IProvider | null) => any;
}

interface IStateProps {
    selected: IProvider;
    isEdit: boolean;
}

type TProps = IDispatchProps & IStateProps;

interface IState {
    nameValue: string,
}

class EditProvider extends React.Component<TProps, IState> {
    state: IState = {
        nameValue: '',
    };

    componentDidUpdate(prevProps: Readonly<TProps>): void {
        if (prevProps.isEdit !== this.props.isEdit || prevProps.selected !== this.props.selected) {
            let value = "";

            if (this.props.isEdit) {
                value = this.props.selected.name;
            }

            this.setState({
                nameValue: value,
            })
        }
    }

    private onChange = (event: any) => this.setState({nameValue: event.target.value});

    private onClickAdd = () => {
        this.props.addProvider(this.state.nameValue);
        this.setState({
            nameValue: '',
        })
    };

    private onClickUpdate = () => {
        if (this.props.selected.id !== undefined) {
            this.props.updateProvider(this.props.selected.id, this.state.nameValue);

            this.setState({
                nameValue: '',
            })
        }
    };

    render() {
        return (
            <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
            >
                <TextField
                    id="standard-name"
                    label={ReadableField.PROVIDER}
                    value={this.state.nameValue}
                    onChange={this.onChange}
                    margin="normal"
                    fullWidth
                />

                {this.props.isEdit ?
                    <Fab
                        color="secondary"
                        aria-label="Edit"
                        style={{marginTop: 16}}
                        onClick={this.onClickUpdate}
                    >
                        <Edit/>
                    </Fab>
                    :
                    <Fab
                        color="primary"
                        aria-label="Add"
                        style={{marginTop: 16}}
                        onClick={this.onClickAdd}
                    >
                        <AddIcon/>
                    </Fab>
                }
            </Grid>
        )
    }
}

const mapStateToProps = (state: any): IStateProps => ({
    selected: state.providers.selected,
    isEdit: state.providers.isEdit,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators({
        addProvider,
        updateProvider,
        selectProvider,
    }, dispatch);

export default connect<IStateProps, IDispatchProps, null>(
    mapStateToProps,
    mapDispatchToProps,
)(EditProvider);


