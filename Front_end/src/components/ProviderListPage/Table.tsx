import React from 'react';
import CommonTable from "../common/CommonTable";
import {IProvider, ITableHeader, ReadableField} from "../../consts/consts";
import {bindActionCreators, Dispatch} from "redux";
import {
    deleteProvider,
    loadProviders,
    selectProvider,
    selectProviderForEdit
} from "../../actions/acitons";
import {connect} from "react-redux";
import {Edit} from "@material-ui/icons";
import {TableRow} from "@material-ui/core";
import TableCell from "@material-ui/core/es/TableCell";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';
import {isEqual} from 'lodash';

const headers = [
    {id: 0, title: ReadableField.PROVIDER, align: 'left'} as ITableHeader,
    {id: 1, title: '', align: 'right'} as ITableHeader
];

interface IDispatchProps {
    loadProviders: () => any;
    selectProvider: (provider: IProvider) => any;
    selectProviderForEdit: (provider: IProvider) => any;
    deleteProvider: (id: number) => any;
}

interface IStateProps {
    items: IProvider[];
}

class Table extends React.Component<IDispatchProps & IStateProps, {}> {
    componentDidMount(): void {
        this.props.loadProviders();
    }

    componentWillUpdate(nextProps: Readonly<IDispatchProps & IStateProps>, nextState: Readonly<{}>, nextContext: any): void {
        if (!isEqual(nextProps.items, this.props.items)) {
            this.props.loadProviders();
        }
    }

    render() {
        return (
            <CommonTable
                headers={headers}
                renderRows={this.renderRows}
                height={627}
            />
        );
    }

    private renderRows = (classNames: string) => this.props.items.map(row => {
            return (
                <TableRow key={row.id} hover onClick={() => this.props.selectProvider(row)}>
                    <TableCell
                        className={classNames}
                        scope="row"
                        variant="body"
                    >
                        {row.name}
                    </TableCell>
                    <TableCell
                        className={classNames}
                        scope="row"
                        align={'right'}
                    >
                        <IconButton onClick={this.onClickEdit(row)}>
                            <Edit color="primary"/>
                        </IconButton>
                        <IconButton onClick={this.onClickDelete(row.id)}>
                            <DeleteIcon color={'error'}/>
                        </IconButton>
                    </TableCell>
                </TableRow>
            )
        }
    );

    private onClickEdit = (providers: IProvider) => (event: any) => {
        event.stopPropagation();
        this.props.selectProviderForEdit(providers);
    };

    private onClickDelete = (id: number | undefined) => (event: any) => {
        event.stopPropagation();
        if (id !== undefined) {
            this.props.deleteProvider(id)
        }
    };
}

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators({
        loadProviders,
        selectProvider,
        deleteProvider,
        selectProviderForEdit,
    }, dispatch);

const mapStateToProps = (state: any): IStateProps => ({
    items: state.providers.items,
});

export default connect<IStateProps, IDispatchProps, null>(
    mapStateToProps,
    mapDispatchToProps,
)(Table);
