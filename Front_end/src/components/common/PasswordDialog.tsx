import Dialog from "@material-ui/core/Dialog";
import {stylesForEditedDialog, Transition} from "./Utils";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import React from "react";
import {TextField, withStyles} from "@material-ui/core";
import {bindActionCreators, Dispatch} from "redux";
import {unlockApp} from "../../actions/acitons";
import {connect} from "react-redux";

interface IDispatchProps {
    unlockApp: () => any;
}

interface IStateProps {
    isAppLock: boolean;
}

type IProps = IDispatchProps & IStateProps;

const PASSWORD = 'WBv5f0Sx';

const PasswordDialog = ({isAppLock, unlockApp}: IProps) => {
    const [password, setPassword] = React.useState('');
    const [error, setError] = React.useState(false);

    const unlock = (event: any) => {
       if (event.keyCode === 13) {
           if (password === PASSWORD) {
               unlockApp();
           }

           setError(true);
       }
    };

    const onChange = (event: any) => {
        setPassword(event.target.value);
    };

    return (
        <Dialog
            open={isAppLock}
            TransitionComponent={Transition}
            maxWidth={'xl'}
        >
            <DialogTitle>Введите пароль</DialogTitle>
            <DialogContent>
                <TextField
                    label={"Пароль"}
                    margin="normal"
                    fullWidth
                    type={"password"}
                    value={password}
                    onKeyUp={unlock}
                    error={error}
                    onChange={onChange}
                />
            </DialogContent>
        </Dialog>
    )
};

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators({
        unlockApp,
    }, dispatch);

const mapStateToProps = (state: any): IStateProps => ({
    isAppLock: state.isAppLock,
});

export default connect<IStateProps, IDispatchProps>(
    mapStateToProps,
    mapDispatchToProps,
)(withStyles(stylesForEditedDialog)(PasswordDialog));
