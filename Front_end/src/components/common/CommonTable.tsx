import React from 'react';
import {Theme, withStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import createStyles from "@material-ui/core/styles/createStyles";
import classNames from "classnames";
import {ITableHeader} from "../../consts/consts";

const styles = (theme: Theme) => createStyles({
    header: {
        position: "sticky",
        top: 0,
        backgroundColor: "#fff",
        zIndex: 10,
    },
    root: {
        width: '100%',
        overflowX: 'auto',
    },
    table: {
        minWidth: 150,
    },
    tableRow: {
        cursor: 'pointer',
    },
    tableRowHover: {
        '&:hover': {
            backgroundColor: theme.palette.grey[200],
        },
    },
});

interface IProps {
    classes: {
        table: string,
        root: string,
        tableRow: string,
        tableRowHover: string,
        header: string,
    },
    headers: ITableHeader[],
    renderRows: (classNames: string) => JSX.Element[];
    height: number | string;
}

function CommonTable(props: IProps) {
    const {classes, headers, renderRows, height} = props;

    return (
        <Paper className={classes.root} style={{height: height}}>
            <Table className={classes.table} padding={"none"}>
                <TableHead >
                    <TableRow>
                        {headers.map(header => {
                            return (
                                <TableCell padding={"checkbox"} className={classes.header}
                                    key={header.id}
                                    align={header.align}>
                                    <b style={{fontSize: 11}}>
                                        {header.title}
                                    </b>
                                </TableCell>
                            );
                        })}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {renderRows && renderRows(classNames(classes.tableRow, classes.tableRowHover))}
                </TableBody>
            </Table>
        </Paper>
    );
}

export default withStyles(styles)(CommonTable);