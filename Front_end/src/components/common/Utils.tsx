import Slide from '@material-ui/core/Slide';
import {default as NumberFormat} from "react-number-format";
import React from "react";
import {IProvider, ReadableField} from "../../consts/consts";
import {isNil, trim} from 'lodash';
import {TextField, Theme} from "@material-ui/core";
import createStyles from "@material-ui/core/styles/createStyles";
import TableCell from "@material-ui/core/es/TableCell";
import MenuItem from '@material-ui/core/MenuItem';

export const stylesForEditedDialog = (theme: Theme) => createStyles({
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
});

export function Transition(props: any) {
    return <Slide direction="up" {...props} />;
}

export function MoneyFormatCustom(props: any) {
    const {inputRef, onChange, ...other} = props;

    return (
        <NumberFormat
            {...other}
            getInputRef={inputRef}
            onValueChange={values => {
                onChange({
                    target: {
                        value: values.value,
                    },
                });
            }}
            isNumericString
            thousandSeparator = {' '}
            decimalScale={2}
            decimalSeparator={','}
            suffix=" ₽"
        />
    );
}

export function NumberFormatCustom(props: any) {
    const {inputRef, onChange, ...other} = props;

    return (
        <NumberFormat
            {...other}
            getInputRef={inputRef}
            onValueChange={values => {
                onChange({
                    target: {
                        value: values.value,
                    },
                });
            }}
            isNumericString
            thousandSeparator = {' '}
            decimalScale={2}
            decimalSeparator={','}
        />
    );
}

export const SelectProvider = (props: any) => {
    const {classes, provider, handleChangeProvider, providers} = props;

    return (
        <TextField
            select
            label={ReadableField.PROVIDER}
            className={classes.textField}
            margin="normal"
            value={provider}
            onChange={handleChangeProvider}
        >
            <MenuItem value={0}/>
            {providers.map((provider: IProvider) =>
                <MenuItem value={provider.id}>
                    {provider.name}
                </MenuItem>
            )}
        </TextField>
    )
};

export const isEmpty = (value: string) => {
    return (isNil(value) || trim(value).length === 0);
};

export const getBoolValue = (value: string) => {
    return value === ReadableField.YES;
};

export const getValueForRadioGroup = (value: boolean | string) => {
    if (typeof value === 'boolean') {
        return value ? ReadableField.YES : ReadableField.NO;
    }

    return value;
};

export const renderCell = (item: any, classNames: string) => {
    let text = item;

    if (typeof item === 'boolean') {
        text = item ? ReadableField.YES : ReadableField.NO;
    }

    return (
        <TableCell
            className={classNames}
            component="th"
            scope="row"
            variant="body"
            padding={"checkbox"}
        >
            {text}
        </TableCell>
    )
};