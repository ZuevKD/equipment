import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import React from "react";
import AppBar from '@material-ui/core/AppBar';
import {Link} from "react-router-dom";
import {bindActionCreators, Dispatch} from "redux";
import {selectTab} from "../../actions/acitons";
import {connect} from "react-redux";

interface IStateProps {
    tabIndex: number;
}

interface IDispatchProps {
    selectTab: (tabIndex: number) => void;
}

interface IProps {
    position?: 'fixed' | 'absolute' | 'sticky' | 'static' | 'relative';
    classNames?: string;
}

type TProps = IDispatchProps & IStateProps & IProps;

const AppTabs = ({tabIndex, selectTab, classNames = '', position = 'static'}: TProps) => {
    const LinkToMain = (props: any) => <Link to="/" {...props} />;
    const LinkToProviderList = (props: any) => <Link to="/providerList" {...props} />;
    const LinkToSearch = (props: any) => <Link to="/search" {...props} />;

    const handleChange = (event: any, value: any) => {
        selectTab(value);
    };

    return (
        <AppBar position={position} className={classNames}>
            <Tabs value={tabIndex} onChange={handleChange}>
                <Tab label="Подразделения" component={LinkToMain}/>
                <Tab label="Поставщики" component={LinkToProviderList}/>
                <Tab label="Поиск" component={LinkToSearch}/>
            </Tabs>
        </AppBar>
    )
};

const mapStateToProps = (state: any): IStateProps => ({
    tabIndex: state.tabIndex,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators({
        selectTab
    }, dispatch);

export default connect<IStateProps, IDispatchProps, null>(
    mapStateToProps,
    mapDispatchToProps,
)(AppTabs);