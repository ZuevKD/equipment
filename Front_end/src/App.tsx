import React, {Component} from 'react';
import MainPage from "./components/MainPage/MainPage";
import {Provider} from "react-redux";
import {applyMiddleware, compose, createStore, Store} from "redux";
import {reducer} from "./reducers/reducers";
import thunk from 'redux-thunk';
import {Route} from 'react-router';
import {HashRouter} from 'react-router-dom';
import BranchPage from "./components/BranchPage/BranchPage";
import ProviderListPage from "./components/ProviderListPage/ProviderListPage";
import SearchPage from "./components/SearchPage/SearchPage";

class App extends Component {
    render() {
        const store: Store = createStore(reducer, compose(
            applyMiddleware(thunk),
            (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__()
        ));

        return (
            <Provider store={store}>
                <HashRouter>
                    <Route path="/" exact component={MainPage}/>
                </HashRouter>
                <HashRouter>
                    <Route path="/branch" component={BranchPage}/>
                </HashRouter>
                <HashRouter>
                    <Route path="/providerList" component={ProviderListPage}/>
                </HashRouter>
                <HashRouter>
                    <Route path="/search" component={SearchPage}/>
                </HashRouter>
            </Provider>
        );
    }
}

export default App;
