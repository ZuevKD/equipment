import {initialState} from "../initialState";
import {IAction} from "../consts/consts";
import {
    ADD_BRANCH,
    ADD_DEPARTMENT, ADD_PROVIDER,
    ADD_RADIOSTATION, ADD_TECHNIQUE,
    DELETE_BRANCH,
    DELETE_DEPARTMENT, DELETE_PROVIDER,
    DELETE_RADIOSTATION, DELETE_TECHNIQUE,
    LOAD_BRANCH,
    LOAD_DEPARTMENT, LOAD_DIAPASONS, LOAD_PROVIDER,
    LOAD_RADIOSTATION, LOAD_SPECIFICITIES,
    LOAD_TECHNIQUE, SELECT_ALL_TECHNIQUES,
    SELECT_BRANCH,
    SELECT_BRANCH_FOR_EDIT, SELECT_COLUMN,
    SELECT_DEPARTMENT,
    SELECT_DEPARTMENT_FOR_EDIT, SELECT_PROVIDER, SELECT_PROVIDER_FOR_EDIT,
    SELECT_RADIOSTATION, SELECT_TAB,
    SELECT_TECHNIQUE, UNLOCK_APP,
    UPDATE_BRANCH,
    UPDATE_DEPARTMENT, UPDATE_PROVIDER,
    UPDATE_RADIOSTATION, UPDATE_TECHNIQUE
} from "../consts/actionTypes";

export function reducer(
    state: any = initialState,
    action: IAction,
) {
    const payload = action.payload || {};

    const getDepartments = (state: any) => {
        return {
            ...state, departments: {
                ...state.departments,
                items: payload.response,
            }
        };
    };

    const getBranches = (state: any) => {
        return {
            ...state, branches: {
                ...state.branches,
                items: payload.response,
            }
        };
    };

    const getRadiostation = (state: any) => {
        return {
            ...state,
            radiostations: {
                ...state.radiostations,
                items: payload.response,
            }
        };
    };

    const getTechniques = (state: any) => {
        return {
            ...state,
            techniques: {
                ...state.techniques,
                items: payload.response,
            }
        };
    };

    const getProviders = (state: any) => {
        return {
            ...state,
            providers: {
                ...state.providers,
                items: payload.response,
                isEdit: false,
            }
        };
    };

    switch (action.type) {
        case LOAD_DEPARTMENT:
            return getDepartments(state);

        case SELECT_DEPARTMENT:
            return {
                ...state,
                departments: {
                    ...state.departments,
                    isEdit: false,
                    selected: payload.selected,
                },
                branches: {
                    ...state.branches,
                    isEdit: false,
                }
            };

        case SELECT_DEPARTMENT_FOR_EDIT:
            console.log(payload);
            return {
                ...state,
                departments: {
                    ...state.departments,
                    isEdit: true,
                    selected: payload.selected,
                }
            };

        case UPDATE_DEPARTMENT:
            return {
                ...state,
                departments: {
                    ...state.departments,
                    items: payload.response,
                    isEdit: false,
                }
            };

        case DELETE_DEPARTMENT:
            return {
                ...state,
                departments: {
                    ...state.departments,
                    items: payload.response,
                    selected: null,
                    isEdit: false,
                }
            };

        case ADD_DEPARTMENT:
            return getDepartments(state);

        case LOAD_BRANCH:
            return getBranches(state);

        case ADD_BRANCH:
            return getBranches(state);

        case SELECT_BRANCH:
            return {
                ...state,
                branches: {
                    ...state.branches,
                    selected: payload.selected,
                }
            };

        case SELECT_BRANCH_FOR_EDIT:
            return {
                ...state,
                branches: {
                    ...state.branches,
                    isEdit: true,
                    selected: payload.selected,
                }
            };

        case UPDATE_BRANCH:
            return {
                ...state,
                branches: {
                    ...state.branches,
                    items: payload.response,
                    isEdit: false,
                }
            };

        case DELETE_BRANCH:
            return {
                ...state,
                branches: {
                    ...state.branches,
                    items: payload.response,
                    selected: null,
                    isEdit: false,
                }
            };

        case LOAD_RADIOSTATION:
            return getRadiostation(state);

        case SELECT_RADIOSTATION:
            return {
                ...state,
                radiostations: {
                    ...state.radiostations,
                    selected: payload.selected,
                }
            };

        case ADD_RADIOSTATION:
            return getRadiostation(state);

        case UPDATE_RADIOSTATION:
            return getRadiostation(state);

        case DELETE_RADIOSTATION:
            return getRadiostation(state);

        case LOAD_TECHNIQUE:
            return {
                ...state, techniques: {
                    ...state.techniques,
                    items: payload.response,
                }
            };

        case SELECT_TECHNIQUE:
            return {
                ...state,
                techniques: {
                    ...state.techniques,
                    selected: payload.selected,
                }
            };

        case ADD_TECHNIQUE:
            return getTechniques(state);

        case UPDATE_TECHNIQUE:
            return getTechniques(state);

        case DELETE_TECHNIQUE:
            return getTechniques(state);

        case SELECT_PROVIDER:
            return {
                ...state,
                providers: {
                    ...state.providers,
                    selected: payload.selected,
                    isEdit: false,
                }
            };

        case SELECT_PROVIDER_FOR_EDIT:
            return {
                ...state,
                providers: {
                    ...state.providers,
                    selected: payload.selected,
                    isEdit: true,
                }
            };

        case LOAD_PROVIDER:
            return getProviders(state);

        case ADD_PROVIDER:
            return getProviders(state);

        case UPDATE_PROVIDER:
            return {
                ...state,
                providers: {
                    ...state.providers,
                    items: payload.response,
                    selected: null,
                    isEdit: false,
                }
            };

        case DELETE_PROVIDER:
            return getProviders(state);

        case LOAD_SPECIFICITIES:
            return {
                ...state,
                specificities: payload.response,
            };

        case LOAD_DIAPASONS:
            return {
                ...state,
                diapasons: payload.response,
            };

        case SELECT_COLUMN:
            return {
                ...state,
                selectedColumns: payload.selectedColumns,
            };

        case  SELECT_TAB:
            return {
                ...state,
                tabIndex: payload.tabIndex,
            };

        case UNLOCK_APP:
            return {
                ...state,
                isAppLock: false,
            };

        case SELECT_ALL_TECHNIQUES:
            return {
                ...state,
                allTechniques: {
                    commEquipments: payload.response,
                }
            };

        default:
            return state;
    }
}