export interface ITableHeader {
    id: number,
    title: string,
    align: "left" | "center" | "right";
}

/**
 * Общий интерфейс Действия.
 *
 * @prop {string} type - Тип Действия.
 * @prop {any} [payload] - Данные, передаваемые вместе с Действием.
 */
export interface IAction {
    type: string;
    payload?: any;
}

export interface IDepartment {
    id: number,
    name: string,
}

export interface IBranch {
    id: number,
    name: string,
}

export interface IEquipment {
    id?: number,
    composition: string,
    description: string,
}

export interface IProvider {
    id?: number,
    name: string,
}

interface ISupply {
    id?: number,
    date: Date,
    invoiceNum: number,
    provider: IProvider
}

export interface ICommEquipment {
    id?: number,
    idEquipment?: number,
    idBranch?: number,
    model: IModel,
    commissioningDate: Date,
    distributionPlan: Date,
    releaseDate: Date,
    equipment: IEquipment,
    supply: ISupply,
    serialNum: string,
    inventoryNum: number,
    price: number,
    modelOperability: boolean,
    nameDepartment: string,
    nameBranch: string,
}

export interface IModel {
    id?: number,
    name: string,
    lifeTime: number,
    radiostation: IRadiostation,
    technique: ITechnique,
}

export interface ITechnique {
    id?: number,
    name: string,
    nameBranch?: string,
    nameDepartment?: string,
}

export interface IDiapason {
    id?: number,
    name: string,
}

export interface ISpecificity {
    id?: number,
    name: string,
}

export interface IRadiostation {
    id?: number,
    name: string,
    presenceScDevice: boolean,
    diapason: IDiapason,
    specificity: ISpecificity,
    nameBranch?: string,
    nameDepartment?: string,
}

export interface ITechnique {
    id?: number,
    name: string,
    model: IModel,
    nameBranch?: string,
    nameDepartment?: string,
}

export enum ReadableField {
    DEPARTMENT = 'Подразделение',
    BRANCH = 'Служба',
    TECHNIQUE = 'Техника',
    RADIOSTATION = 'Радиостанция',
    RADIOSTATIONS = 'Радиостанции',
    NAME = 'Наименование',
    MODEL = 'Модель',
    INVENTORY_NUM = 'Инвентарный  номер',
    SERIAL_NUM = 'Заводской номер',
    LIFE_TIME = 'Срок службы',
    RELEASE_DATE = 'Дата выпуска',
    COMMISSIONING_DATE = 'Ввод в эксплуатацию',
    DISTRIBUTION_PLAN = 'План распределения',
    PRICE = 'Цена',
    MODEL_OPERABILITTY = 'Работоспособность',
    EQUIPMENT = 'Состав',
    PRESENCE_SC_DEVICE = 'Наличие УПР',
    PROVIDER = 'Поставщик',
    DIAPASON = 'Диапазон',
    DESCRIPTION = 'Примечание',
    SPECIFICITY = 'Особенности',
    INVOICE_NUM = 'Номер накладной',
    INVOICE_DATA = 'Дата накладной',
    YES = 'Да',
    NO = 'Нет',
}

export interface IFilter {
    department: string,
    branch: string,
    technique: string,
    date: string,
    model: string,
    distributionPlan: string,
    serialNum: string,
    inventoryNum: string,
}

export interface IColumn {
    name: ReadableField;
    selected: boolean;
}

export const columnNames: IColumn[] = [
    {name: ReadableField.INVENTORY_NUM, selected: true},
    {name: ReadableField.SERIAL_NUM, selected: true},
    {name: ReadableField.LIFE_TIME, selected: false},
    {name: ReadableField.RELEASE_DATE, selected: true},
    {name: ReadableField.COMMISSIONING_DATE, selected: false},
    {name: ReadableField.DISTRIBUTION_PLAN, selected: true},
    {name: ReadableField.PRICE, selected: true},
    {name: ReadableField.MODEL_OPERABILITTY, selected: true},
    {name: ReadableField.EQUIPMENT, selected: true},
    {name: ReadableField.PROVIDER, selected: true},
    {name: ReadableField.INVOICE_NUM, selected: false},
    {name: ReadableField.INVOICE_DATA, selected: false},
    {name: ReadableField.DESCRIPTION, selected: false},
];