import {columnNames} from "./consts/consts";

const entityParameters = {
    items: [],
    selected: null,
    isEdit: false,
};

export const initialState: any = {
    tabIndex: 0,

    departments: {
        ...entityParameters,
    },

    branches: {
        ...entityParameters,
    },

    radiostations: {
        ...entityParameters,
    },

    techniques: {
        ...entityParameters,
    },

    providers: {
        ...entityParameters,
    },

    specificities: [],
    diapasons: [],
    selectedColumns: columnNames,

    allTechniques: {
        commEquipments: [],
    },

    isAppLock: true,
};