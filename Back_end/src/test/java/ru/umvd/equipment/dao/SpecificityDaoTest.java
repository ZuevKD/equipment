package ru.umvd.equipment.dao;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import ru.umvd.equipment.entities.Specificity;

public class SpecificityDaoTest {
    private EmbeddedDatabase embeddedDatabase;
    private SpecificityDao specificityDao;

    @Before
    public void setUp() {
        // Создадим базу данных для тестирования
        embeddedDatabase = new EmbeddedDatabaseBuilder()
                .addScript("specificities/schema.sql")
                .addScripts("specificities/data.sql")
                .setType(EmbeddedDatabaseType.H2)// Используем базу H2
                .build();

        specificityDao = new SpecificityDao(embeddedDatabase);
    }

    @After
    public void tearDown() {
        embeddedDatabase.shutdown();
    }

    @Test
    public void testAdd() {
        long id = specificityDao.add(new Specificity("Оссобенность1"));

        Specificity actual = specificityDao.getRecord(id);
        Assert.assertEquals("Оссобенность1", actual.getName());
    }

    @Test
    public void testUpdate() {
        long id = specificityDao.add(new Specificity("Оссобенность2"));

        specificityDao.update(id, new Specificity("Обновлённая оссобенность2"));
        Specificity actual = specificityDao.getRecord(id);

        Assert.assertEquals("Обновлённая оссобенность2", actual.getName());
    }

    @Test
    public void testRemove() {
        long id = specificityDao.add(new Specificity("Оссобенность3"));

        int size = specificityDao.getRecords().size();

        specificityDao.remove(id);

        int newSize = specificityDao.getRecords().size();

        Assert.assertEquals(size - 1, newSize);
    }

    @Test
    public void testGetRecords() {
        Assert.assertEquals(5, specificityDao.getRecords().size());
    }
}