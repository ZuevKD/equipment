package ru.umvd.equipment.dao;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import ru.umvd.equipment.entities.Branch;
import ru.umvd.equipment.entities.Department;

public class DepartmentDaoTest {
    private EmbeddedDatabase embeddedDatabase;
    private DepartmentDao departmentDao;

    @Before
    public void setUp() {
        // Создадим базу данных для тестирования
        embeddedDatabase = new EmbeddedDatabaseBuilder()
                .addScripts("departments/schema.sql", "departments/data.sql")
                .setType(EmbeddedDatabaseType.H2)// Используем базу H2
                .build();

        departmentDao = new DepartmentDao(embeddedDatabase);
    }

    @After
    public void tearDown() {
        embeddedDatabase.shutdown();
    }

    @Test
    public void testAdd() {
        long departmentId = departmentDao.add(new Department("Подразделение1"));

        Department actual = departmentDao.getRecord(departmentId);

        Assert.assertEquals("Подразделение1", actual.getName());
    }

    @Test
    public void testUpdate() {
        long departmentId = departmentDao.add(new Department("Подразделение2"));

        departmentDao.update(departmentId, new Department("Обновлённое Подразделение3"));

        Department actual = departmentDao.getRecord(departmentId);

        Assert.assertEquals("Обновлённое Подразделение3", actual.getName());
    }

    @Test
    public void testRemove() {
        long departmentId = departmentDao.add(new Department("Подразделение3"));

        int size = departmentDao.getRecords().size();

        departmentDao.remove(departmentId);

        int newSize = departmentDao.getRecords().size();

        Assert.assertEquals(size - 1, newSize);
    }

    @Test
    public void testGetRecords() {
        Assert.assertEquals(5, departmentDao.getRecords().size());
    }
}
