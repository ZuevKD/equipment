//package ru.umvd.equipment.dao;
//
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
//import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
//import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
//import ru.umvd.equipment.entities.Provider;
//
//import java.sql.Date;
//
//public class ProviderDaoTest {
//    private EmbeddedDatabase embeddedDatabase;
//    private ProviderDao providerDao;
//
//    @Before
//    public void setUp() {
//        // Создадим базу данных для тестирования
//        embeddedDatabase = new EmbeddedDatabaseBuilder()
//                .addScript("providers/schema.sql")
//                .addScripts("providers/data.sql")
//                .setType(EmbeddedDatabaseType.H2)// Используем базу H2
//                .build();
//
//        providerDao = new ProviderDao(embeddedDatabase);
//    }
//
//    @After
//    public void tearDown() {
//        embeddedDatabase.shutdown();
//    }
//
//    @Test
//    public void testAdd() {
//        long id = providerDao.add(new Provider("Поставщик1", 1, Date.valueOf("2018-06-18")));
//
//        Provider expected = providerDao.getRecord(id);
//        Assert.assertEquals("Поставщик1", expected.getName());
//        Assert.assertEquals(1, expected.getInvoiceNum());
//        Assert.assertEquals("2018-06-18", expected.getDate().toString());
//    }
//
//    @Test
//    public void testUpdate() {
//        long id = providerDao.add(new Provider("Поставщик2", 2, Date.valueOf("2018-02-19")));
//
//        providerDao.update(id, new Provider("Обновлённый поставщик2", 3, Date.valueOf("2018-02-20")));
//        Provider actual = providerDao.getRecord(id);
//
//        Assert.assertEquals("Обновлённый поставщик2", actual.getName());
//        Assert.assertEquals(3, actual.getInvoiceNum());
//        Assert.assertEquals("2018-02-20", actual.getDate().toString());
//    }
//
//    @Test
//    public void testRemove() {
//        long id = providerDao.add(new Provider("Поставщик3", 3, Date.valueOf("2018-02-19")));
//
//        int size = providerDao.getRecords().size();
//
//        providerDao.remove(id);
//
//        int newSize = providerDao.getRecords().size();
//
//        Assert.assertEquals(size - 1, newSize);
//    }
//
//    @Test
//    public void testGetRecords() {
//        Assert.assertEquals(2, providerDao.getRecords().size());
//    }
//}