//package ru.umvd.equipment.dao;
//
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
//import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
//import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
//import ru.umvd.equipment.entities.Equipment;
//import ru.umvd.equipment.entities.Model;
//import ru.umvd.equipment.entities.Provider;
//
//import java.sql.Date;
//
//public class ModelDaoTest {
//    private EmbeddedDatabase embeddedDatabase;
//    private ModelDao modelDao;
//    private EquipmentDao equipmentDao;
//    private ProviderDao providerDao;
//
//    @Before
//    public void setUp() {
//        // Создадим базу данных для тестирования
//        embeddedDatabase = new EmbeddedDatabaseBuilder()
//                .addScripts("equipments/schema.sql", "equipments/data.sql",
//                        "providers/schema.sql", "providers/data.sql",
//                        "models/schema.sql", "models/data.sql"
//                )
//                .setType(EmbeddedDatabaseType.H2)// Используем базу H2
//                .build();
//
//        modelDao = new ModelDao(embeddedDatabase);
//        equipmentDao = new EquipmentDao(embeddedDatabase);
//        providerDao = new ProviderDao(embeddedDatabase);
//    }
//
//    @After
//    public void tearDown() {
//        embeddedDatabase.shutdown();
//    }
//
//    @Test
//    public void testAdd() {
//        long idProvider = providerDao.add(new Provider("Поставщик1", 2, Date.valueOf("2018-02-19")));
//        long idEquipment = equipmentDao.add(new Equipment("Состав1", "Комментарий1"));
//
//        long id = modelDao.add(new Model(
//                "Модель1",
//                "123",
//                "321",
//                10,
//                12,
//                Date.valueOf("2012-06-18"),
//                Date.valueOf("2013-06-18"),
//                Date.valueOf("2014-06-18"),
//                idProvider,
//                idEquipment,
//                true
//        ));
//
//        Model actual = modelDao.getRecord(id);
//
//        Assert.assertEquals("Модель1", actual.getName());
//        Assert.assertEquals("123", actual.getSerialNum());
//        Assert.assertEquals("321", actual.getInventoryNum());
//        Assert.assertEquals(10, actual.getLifeTime());
//        Assert.assertEquals(12, actual.getPrice());
//        Assert.assertEquals("2012-06-18", actual.getReleaseDate().toString());
//        Assert.assertEquals("2013-06-18", actual.getCommissioningDate().toString());
//        Assert.assertEquals("2014-06-18", actual.getDistributionPlan().toString());
//        Assert.assertEquals(idProvider, actual.getIdProvider());
//        Assert.assertEquals(idEquipment, actual.getIdEquipment());
//        Assert.assertTrue(actual.getModelOperability());
//    }
//
//    @Test
//    public void testUpdate() {
//        long idProvider1 = providerDao.add(new Provider("Поставщик1", 2, Date.valueOf("2018-02-19")));
//        long idEquipment1 = equipmentDao.add(new Equipment("Состав1", "Комментарий1"));
//        long id = modelDao.add(new Model(
//                "Модель1",
//                "123",
//                "321",
//                10,
//                12,
//                Date.valueOf("2012-06-18"),
//                Date.valueOf("2013-06-18"),
//                Date.valueOf("2014-06-18"),
//                idProvider1,
//                idEquipment1,
//                true
//        ));
//
//        long idProvider2 = providerDao.add(new Provider("Поставщик2", 2, Date.valueOf("2018-02-19")));
//        long idEquipment2 = equipmentDao.add(new Equipment("Состав2", "Комментарий2"));
//
//        Model updateModal = new Model(
//                "Модель2",
//                "1231",
//                "3211",
//                11,
//                14,
//                Date.valueOf("2012-06-18"),
//                Date.valueOf("2013-06-18"),
//                Date.valueOf("2014-06-18"),
//                idProvider2,
//                idEquipment2,
//                true
//        );
//
//        modelDao.update(id, updateModal);
//
//        Model actual = modelDao.getRecord(id);
//
//        Assert.assertEquals("Модель2", actual.getName());
//        Assert.assertEquals("1231", actual.getSerialNum());
//        Assert.assertEquals("3211", actual.getInventoryNum());
//        Assert.assertEquals(11, actual.getLifeTime());
//        Assert.assertEquals(14, actual.getPrice());
//        Assert.assertEquals("2012-06-18", actual.getReleaseDate().toString());
//        Assert.assertEquals("2013-06-18", actual.getCommissioningDate().toString());
//        Assert.assertEquals("2014-06-18", actual.getDistributionPlan().toString());
//        Assert.assertEquals(idProvider2, actual.getIdProvider());
//        Assert.assertEquals(idEquipment2, actual.getIdEquipment());
//        Assert.assertTrue(actual.getModelOperability());
//    }
//
//    @Test
//    public void testRemove() {
//        long idProvider = providerDao.add(new Provider("Поставщик1", 2, Date.valueOf("2018-02-19")));
//        long idEquipment = equipmentDao.add(new Equipment("Состав1", "Комментарий1"));
//
//        long id = modelDao.add(new Model(
//                "Модель1",
//                "123",
//                "321",
//                10,
//                12,
//                Date.valueOf("2012-06-18"),
//                Date.valueOf("2013-06-18"),
//                Date.valueOf("2014-06-18"),
//                idProvider,
//                idEquipment,
//                true
//        ));
//
//        int size = modelDao.getRecords().size();
//
//        modelDao.remove(id);
//
//        int newSize = modelDao.getRecords().size();
//
//        Assert.assertEquals(size - 1, newSize);
//    }
//
//    @Test
//    public void testGetRecords() {
//        Assert.assertEquals(1, modelDao.getRecords().size());
//    }
//}