//package ru.umvd.equipment.dao;
//
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
//import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
//import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
//import ru.umvd.equipment.entities.CommEquipment;
//
//public class CommEquipmentsDaoTest {
//    private EmbeddedDatabase embeddedDatabase;
//    private CommEquipmentsDao commEquipmentsDao;
//
//    @Before
//    public void setUp() {
//        // Создадим базу данных для тестирования
//        embeddedDatabase = new EmbeddedDatabaseBuilder()
//                .addScripts("branches/schema.sql", "branches/data.sql",
//                        "equipments/schema.sql", "equipments/data.sql",
//                        "providers/schema.sql", "providers/data.sql",
//                        "models/schema.sql", "models/data.sql",
//                        "specificities/schema.sql", "specificities/data.sql",
//                        "diapasons/schema.sql", "diapasons/data.sql",
//                        "technics/schema.sql", "technics/data.sql",
//                        "radiostations/schema.sql", "radiostations/data.sql",
//                        "comm_equipments/schema.sql", "comm_equipments/data.sql")
//                .setType(EmbeddedDatabaseType.H2)// Используем базу H2
//                .build();
//
//        commEquipmentsDao = new CommEquipmentsDao(embeddedDatabase);
//    }
//
//    @After
//    public void tearDown() {
//        embeddedDatabase.shutdown();
//    }
//
//    @Test
//    public void testAdd() {
//        long id = commEquipmentsDao.add(new CommEquipment(1, 1,1));
//
//        CommEquipment actual = commEquipmentsDao.getRecord(id);
//
//        Assert.assertEquals(1, actual.getIdBranch());
//        Assert.assertEquals(1, actual.getIdTechnique());
//        Assert.assertEquals(1, actual.getIdRadiostation());
//    }
//
//    @Test
//    public void testUpdate() {
//        long id = commEquipmentsDao.add(new CommEquipment(1, 1,1));
//
//        commEquipmentsDao.update(id, new CommEquipment(2, 2,1));
//
//        CommEquipment actual = commEquipmentsDao.getRecord(id);
//
//        Assert.assertEquals(2, actual.getIdBranch());
//        Assert.assertEquals(2, actual.getIdTechnique());
//        Assert.assertEquals(1, actual.getIdRadiostation());
//    }
//
//    @Test
//    public void testRemove() {
//        long id = commEquipmentsDao.add(new CommEquipment(1, 1,1));
//
//        int size = commEquipmentsDao.getRecords().size();
//
//        commEquipmentsDao.remove(id);
//
//        int newSize = commEquipmentsDao.getRecords().size();
//
//        Assert.assertEquals(size - 1, newSize);
//    }
//
//    @Test
//    public void testGetRecords() {
//        Assert.assertEquals(4, commEquipmentsDao.getRecords().size());
//    }
//}