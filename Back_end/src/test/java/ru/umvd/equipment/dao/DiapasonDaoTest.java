package ru.umvd.equipment.dao;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import ru.umvd.equipment.entities.Diapason;

public class DiapasonDaoTest {
    private EmbeddedDatabase embeddedDatabase;
    private DiapasonDao diapasonDao;

    @Before
    public void setUp() {
        // Создадим базу данных для тестирования
        embeddedDatabase = new EmbeddedDatabaseBuilder()
                .addScript("diapasons/schema.sql")
                .addScripts("diapasons/data.sql")
                .setType(EmbeddedDatabaseType.H2)// Используем базу H2
                .build();

        diapasonDao = new DiapasonDao(embeddedDatabase);
    }

    @After
    public void tearDown() {
        embeddedDatabase.shutdown();
    }

    @Test
    public void testAdd() {
        long id = diapasonDao.add(new Diapason("Диапозон1"));

        Diapason actual = diapasonDao.getRecord(id);
        Assert.assertEquals("Диапозон1", actual.getName());
    }

    @Test
    public void testUpdate() {
        long id = diapasonDao.add(new Diapason("Диапозон2"));

        diapasonDao.update(id, new Diapason("Обновлённый диапозон2"));
        Diapason actual = diapasonDao.getRecord(id);

        Assert.assertEquals("Обновлённый диапозон2", actual.getName());
    }

    @Test
    public void testRemove() {
        long id = diapasonDao.add(new Diapason("Диапозон3"));

        int size = diapasonDao.getRecords().size();

        diapasonDao.remove(id);

        int newSize = diapasonDao.getRecords().size();

        Assert.assertEquals(size - 1, newSize);
    }

    @Test
    public void testGetRecords() {
        Assert.assertEquals(5, diapasonDao.getRecords().size());
    }
}