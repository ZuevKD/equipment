package ru.umvd.equipment.dao;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import ru.umvd.equipment.entities.Branch;

public class BranchDaoTest {
    private EmbeddedDatabase embeddedDatabase;
    private BranchDao branchDao;

    @Before
    public void setUp() {
        // Создадим базу данных для тестирования
        embeddedDatabase = new EmbeddedDatabaseBuilder()
                .addScripts("departments/schema.sql", "departments/data.sql",
                        "branches/schema.sql", "branches/data.sql")
                .setType(EmbeddedDatabaseType.H2)// Используем базу H2
                .build();

        branchDao = new BranchDao(embeddedDatabase);
    }

    @After
    public void tearDown() {
        embeddedDatabase.shutdown();
    }

    @Test
    public void testAdd() {
        long id = branchDao.add(new Branch("Служба1" , 1));

        Branch actual = branchDao.getRecord(id);
        Assert.assertEquals("Служба1", actual.getName());
        Assert.assertEquals(1, actual.getIdDepartment());
    }

    @Test
    public void testUpdate() {
        long id = branchDao.add(new Branch("Служба2", 1));

        branchDao.update(id, new Branch("Обновлённая служба2", 2));
        Branch actual = branchDao.getRecord(id);

        Assert.assertEquals("Обновлённая служба2", actual.getName());
        Assert.assertEquals(2, actual.getIdDepartment());
    }

    @Test
    public void testRemove() {
        long id = branchDao.add(new Branch("Служба3", 3));

        int size = branchDao.getRecords().size();

        branchDao.remove(id);

        int newSize = branchDao.getRecords().size();

        Assert.assertEquals(size - 1, newSize);
    }

    @Test
    public void testGetRecords() {
        Assert.assertEquals(5, branchDao.getRecords().size());
    }
}
