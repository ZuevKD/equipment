//package ru.umvd.equipment.dao;
//
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
//import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
//import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
//import ru.umvd.equipment.entities.Diapason;
//import ru.umvd.equipment.entities.Equipment;
//import ru.umvd.equipment.entities.Model;
//import ru.umvd.equipment.entities.Provider;
//import ru.umvd.equipment.entities.Radiostation;
//import ru.umvd.equipment.entities.Specificity;
//
//import java.sql.Date;
//
//public class RadiostationDaoTest {
//    private EmbeddedDatabase embeddedDatabase;
//    private ModelDao modelDao;
//    private EquipmentDao equipmentDao;
//    private ProviderDao providerDao;
//    private SpecificityDao specificityDao;
//    private DiapasonDao diapasonDao;
//    private RadiostationDao radiostationDao;
//
//    @Before
//    public void setUp() {
//        // Создадим базу данных для тестирования
//        embeddedDatabase = new EmbeddedDatabaseBuilder()
//                .addScripts("equipments/schema.sql", "equipments/data.sql",
//                        "providers/schema.sql", "providers/data.sql",
//                        "models/schema.sql", "models/data.sql",
//                        "specificities/schema.sql", "specificities/data.sql",
//                        "diapasons/schema.sql", "diapasons/data.sql",
//                        "radiostations/schema.sql", "radiostations/data.sql"
//                )
//                .setType(EmbeddedDatabaseType.H2)// Используем базу H2
//                .build();
//
//        modelDao = new ModelDao(embeddedDatabase);
//        equipmentDao = new EquipmentDao(embeddedDatabase);
//        providerDao = new ProviderDao(embeddedDatabase);
//        specificityDao = new SpecificityDao(embeddedDatabase);
//        diapasonDao = new DiapasonDao(embeddedDatabase);
//        radiostationDao = new RadiostationDao(embeddedDatabase);
//    }
//
//    @After
//    public void tearDown() {
//        embeddedDatabase.shutdown();
//    }
//
//    @Test
//    public void testAdd() {
//        long idProvider = providerDao.add(new Provider("Поставщик1", 2, Date.valueOf("2018-02-19")));
//        long idEquipment = equipmentDao.add(new Equipment("Состав1", "Комментарий1"));
//
//        long idModal = modelDao.add(new Model(
//                "Модель1",
//                "123",
//                "321",
//                10,
//                12,
//                Date.valueOf("2012-06-18"),
//                Date.valueOf("2013-06-18"),
//                Date.valueOf("2014-06-18"),
//                idProvider,
//                idEquipment,
//                true
//        ));
//
//        long idSpecificity = specificityDao.add(new Specificity("Оссобенность1"));
//        long idDiapason = diapasonDao.add(new Diapason("Диапозон1"));
//
//        long id = radiostationDao.add(new Radiostation("Радиостанция1", idModal,
//                idSpecificity, idDiapason, true)
//        );
//
//        Radiostation actual = radiostationDao.getRecord(id);
//
//        Assert.assertEquals("Радиостанция1", actual.getName());
//        Assert.assertEquals(idModal, actual.getIdModel());
//        Assert.assertEquals(idSpecificity, actual.getIdSpecificity());
//        Assert.assertEquals(idDiapason, actual.getIdDiapason());
//        Assert.assertTrue(actual.isPresenceScDevice());
//    }
//
//    @Test
//    public void testUpdate() {
//        long idProvider = providerDao.add(new Provider("Поставщик1", 2, Date.valueOf("2018-02-19")));
//        long idEquipment = equipmentDao.add(new Equipment("Состав1", "Комментарий1"));
//
//        long idModal = modelDao.add(new Model(
//                "Модель1",
//                "123",
//                "321",
//                10,
//                12,
//                Date.valueOf("2012-06-18"),
//                Date.valueOf("2013-06-18"),
//                Date.valueOf("2014-06-18"),
//                idProvider,
//                idEquipment,
//                true
//        ));
//
//        long idSpecificity = specificityDao.add(new Specificity("Оссобенность1"));
//        long idDiapason = diapasonDao.add(new Diapason("Диапозон1"));
//
//        long id = radiostationDao.add(new Radiostation("Радиостанция1", idModal,
//                idSpecificity, idDiapason, true)
//        );
//
//        radiostationDao.update(id, new Radiostation("Радиостанция2", 2,
//                2, 2, true));
//
//        Radiostation actual = radiostationDao.getRecord(id);
//
//        Assert.assertEquals("Радиостанция2", actual.getName());
//        Assert.assertEquals(2, actual.getIdModel());
//        Assert.assertEquals(2, actual.getIdSpecificity());
//        Assert.assertEquals(2, actual.getIdDiapason());
//        Assert.assertTrue(actual.isPresenceScDevice());
//    }
//
//    @Test
//    public void testRemove() {
//        long idProvider = providerDao.add(new Provider("Поставщик1", 2, Date.valueOf("2018-02-19")));
//        long idEquipment = equipmentDao.add(new Equipment("Состав1", "Комментарий1"));
//
//        long idModal = modelDao.add(new Model(
//                "Модель1",
//                "123",
//                "321",
//                10,
//                12,
//                Date.valueOf("2012-06-18"),
//                Date.valueOf("2013-06-18"),
//                Date.valueOf("2014-06-18"),
//                idProvider,
//                idEquipment,
//                true
//        ));
//
//        long idSpecificity = specificityDao.add(new Specificity("Оссобенность1"));
//        long idDiapason = diapasonDao.add(new Diapason("Диапозон1"));
//
//        long id = radiostationDao.add(new Radiostation("Радиостанция1", idModal,
//                idSpecificity, idDiapason, true)
//        );
//
//        int size = radiostationDao.getRecords().size();
//
//        radiostationDao.remove(id);
//
//        int newSize = radiostationDao.getRecords().size();
//
//        Assert.assertEquals(size - 1, newSize);
//    }
//
//    @Test
//    public void testGetRecords() {
//        Assert.assertEquals(1, radiostationDao.getRecords().size());
//    }
//}