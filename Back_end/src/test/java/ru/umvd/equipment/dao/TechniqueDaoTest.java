//package ru.umvd.equipment.dao;
//
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
//import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
//import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
//import ru.umvd.equipment.entities.Equipment;
//import ru.umvd.equipment.entities.Model;
//import ru.umvd.equipment.entities.Provider;
//import ru.umvd.equipment.entities.Technique;
//
//import java.sql.Date;
//
//public class TechniqueDaoTest {
//    private EmbeddedDatabase embeddedDatabase;
//    private ModelDao modelDao;
//    private EquipmentDao equipmentDao;
//    private ProviderDao providerDao;
//    private TechniqueDao techniqueDao;
//
//    @Before
//    public void setUp() {
//        // Создадим базу данных для тестирования
//        embeddedDatabase = new EmbeddedDatabaseBuilder()
//                .addScripts("equipments/schema.sql", "equipments/data.sql",
//                        "providers/schema.sql", "providers/data.sql",
//                        "models/schema.sql", "models/data.sql",
//                        "technics/schema.sql", "technics/data.sql"
//                )
//                .setType(EmbeddedDatabaseType.H2)// Используем базу H2
//                .build();
//
//        modelDao = new ModelDao(embeddedDatabase);
//        equipmentDao = new EquipmentDao(embeddedDatabase);
//        providerDao = new ProviderDao(embeddedDatabase);
//        techniqueDao = new TechniqueDao(embeddedDatabase);
//    }
//
//    @After
//    public void tearDown() {
//        embeddedDatabase.shutdown();
//    }
//
//    @Test
//    public void testAdd() {
//        long idProvider = providerDao.add(new Provider("Поставщик1", 2, Date.valueOf("2018-02-19")));
//        long idEquipment = equipmentDao.add(new Equipment("Состав1", "Комментарий1"));
//
//        long idModal = modelDao.add(new Model(
//                "Модель1",
//                "123",
//                "321",
//                10,
//                12,
//                Date.valueOf("2012-06-18"),
//                Date.valueOf("2013-06-18"),
//                Date.valueOf("2014-06-18"),
//                idProvider,
//                idEquipment,
//                true
//        ));
//
//        long id = techniqueDao.add(new Technique("Техника1", idModal));
//
//        Technique actual = techniqueDao.getRecord(id);
//
//        Assert.assertEquals("Техника1", actual.getName());
//        Assert.assertEquals(idModal, actual.getIdModal());
//    }
//
//    @Test
//    public void testUpdate() {
//        long idProvider1 = providerDao.add(new Provider("Поставщик1", 2, Date.valueOf("2018-02-19")));
//        long idEquipment1 = equipmentDao.add(new Equipment("Состав1", "Комментарий1"));
//        long idModal1 = modelDao.add(new Model(
//                "Модель1",
//                "123",
//                "321",
//                10,
//                12,
//                Date.valueOf("2012-06-18"),
//                Date.valueOf("2013-06-18"),
//                Date.valueOf("2014-06-18"),
//                idProvider1,
//                idEquipment1,
//                true
//        ));
//
//        long id = techniqueDao.add(new Technique("Техника2", idModal1));
//
//        long idProvider2 = providerDao.add(new Provider("Поставщик2", 2, Date.valueOf("2018-02-19")));
//        long idEquipment2 = equipmentDao.add(new Equipment("Состав2", "Комментарий2"));
//
//        long idModal2 = modelDao.add(new Model(
//                "Модель2",
//                "1231",
//                "3211",
//                11,
//                14,
//                Date.valueOf("2012-06-18"),
//                Date.valueOf("2013-06-18"),
//                Date.valueOf("2014-06-18"),
//                idProvider2,
//                idEquipment2,
//                true
//        ));
//
//        techniqueDao.update(id, new Technique("Обновлённая техника2", idModal2));
//        Technique actual = techniqueDao.getRecord(id);
//
//        Assert.assertEquals("Обновлённая техника2", actual.getName());
//        Assert.assertEquals(idModal2, actual.getIdModal());
//    }
//
//    @Test
//    public void testRemove() {
//        long idProvider = providerDao.add(new Provider("Поставщик1", 2, Date.valueOf("2018-02-19")));
//        long idEquipment = equipmentDao.add(new Equipment("Состав1", "Комментарий1"));
//
//        long idModal = modelDao.add(new Model(
//                "Модель1",
//                "123",
//                "321",
//                10,
//                12,
//                Date.valueOf("2012-06-18"),
//                Date.valueOf("2013-06-18"),
//                Date.valueOf("2014-06-18"),
//                idProvider,
//                idEquipment,
//                true
//        ));
//
//        long id = techniqueDao.add(new Technique("Техника3", idModal));
//
//        int size = techniqueDao.getRecords().size();
//
//        techniqueDao.remove(id);
//
//        int newSize = techniqueDao.getRecords().size();
//
//        Assert.assertEquals(size - 1, newSize);
//    }
//
//    @Test
//    public void testGetRecords() {
//        Assert.assertEquals(5, techniqueDao.getRecords().size());
//    }
//}