package ru.umvd.equipment.dao;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import ru.umvd.equipment.entities.Equipment;

public class EquipmentDaoTest {
    private EmbeddedDatabase embeddedDatabase;
    private EquipmentDao equipmentDao;

    @Before
    public void setUp() {
        // Создадим базу данных для тестирования
        embeddedDatabase = new EmbeddedDatabaseBuilder()
                .addScript("equipments/schema.sql")
                .addScripts("equipments/data.sql")
                .setType(EmbeddedDatabaseType.H2)// Используем базу H2
                .build();

        equipmentDao = new EquipmentDao(embeddedDatabase);
    }

    @After
    public void tearDown() {
        embeddedDatabase.shutdown();
    }

    @Test
    public void testAdd() {
        long id = equipmentDao.add(new Equipment("Состав1", "Комментарий1"));

        Equipment expected = equipmentDao.getRecord(id);
        Assert.assertEquals("Состав1", expected.getComposition());
        Assert.assertEquals("Комментарий1", expected.getDescription());
    }

    @Test
    public void testUpdate() {
        long id = equipmentDao.add(new Equipment("Состав2", "Комментарий2"));

        equipmentDao.update(id, new Equipment("Обновлённый состав2", "Новый комментарий2"));
        Equipment actual = equipmentDao.getRecord(id);

        Assert.assertEquals("Обновлённый состав2", actual.getComposition());
        Assert.assertEquals("Новый комментарий2", actual.getDescription());
    }

    @Test
    public void testRemove() {
        long id = equipmentDao.add(new Equipment("Состав3", "Комментарий3"));

        int size = equipmentDao.getRecords().size();

        equipmentDao.remove(id);

        int newSize = equipmentDao.getRecords().size();

        Assert.assertEquals(size - 1, newSize);
    }

    @Test
    public void testGetRecords() {
        Assert.assertEquals(5, equipmentDao.getRecords().size());
    }
}