package ru.umvd.equipment.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import ru.umvd.equipment.entities.Model;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.List;

import static ru.umvd.equipment.dao.Utils.getModel;

public class ModelDao {
    private static final String TABLE = "models";
    private static final String COD = "cod_model";
    private static final String NAME_MODEL = "name_model";
    private static final String SERIAL_NUMBER = "serial_number";
    private static final String INVENTORY_NUMBER = "inventory_number";
    private static final String LIFETIME = "lifetime";
    private static final String PRICE = "price";
    private static final String RELEASE_DATE = "release_date";
    private static final String COMMISSIONING_DATE = "commissioning_date";
    private static final String DISTRIBUTION_PLAN = "distribution_plan";
    private static final String COD_SUPPLY = "cod_supply";
    private static final String COD_EQUIPMENT = "cod_equipment";
    private static final String MODEL_OPERABILITY = "model_operability";
    private static final String NAME_RADIOSTATION = "name_radiostation";
    private static final String NAME_TECHNIC = "name_technic";
    private static final String COD_TECHNIQUE = "cod_technique";
    private static final String COD_RADIOSTATION = "cod_radiostation";
    private static final String COD_SPECIFICITY = "cod_specificity";
    private static final String COD_DIAPASON = "cod_diapason";
    private static final String PRESENCE_SC_DEVICE = "presence_sc_device";

    private static final String COD_MODEL = "cod_model";

    private static final String NAME_SPECIFICITY = "name_specificity";

    private static final String NAME_DIAPASON = "name_diapason";

    private static final String[] PK_COLUMN = {COD};

    private static final String SQL_INSERT = "INSERT INTO " + TABLE
            + " ("
            + NAME_MODEL + ", "
            + LIFETIME + ", "
            + COD_TECHNIQUE + ", "
            + COD_RADIOSTATION
            + ") VALUES(?, ?, ?, ?)";

    private static final String SQL_UPDATE = "UPDATE " + TABLE
            + " SET "
            + NAME_MODEL + " = ?, "
            + LIFETIME + " = ?, "
            + COD_TECHNIQUE + "= ?, "
            + COD_RADIOSTATION + "= ?"
            + " WHERE " + COD + " = ?";

    private static final String SQL_DELETE = "DELETE FROM " + TABLE + " WHERE " + COD + " = ?";
    private static final String SELECT = "SELECT models.cod_model,\n" +
            "       models.name_model,\n" +
            "       models.lifetime,\n" +
            "       models.cod_radiostation,\n" +
            "       diapasons.cod_diapason,\n" +
            "       diapasons.name_diapason,\n" +
            "       radiostations.cod_radiostation,\n" +
            "       radiostations.name_radiostation,\n" +
            "       radiostations.presence_sc_device,\n" +
            "       specificities.cod_specificity,\n" +
            "       specificities.name_specificity,\n" +
            "       technics.cod_technique,\n" +
            "       technics.name_technique\n" +
            "FROM models\n" +
            "         LEFT JOIN radiostations on radiostations.cod_radiostation = models.cod_radiostation\n" +
            "         LEFT JOIN specificities on radiostations.cod_specificity = specificities.cod_specificity\n" +
            "         LEFT JOIN diapasons on diapasons.cod_diapason = radiostations.cod_diapason\n" +
            "         left JOIN technics on technics.cod_technique = models.cod_technique\n" +
            "WHERE models.cod_model = ?";
    private static final String SELECT_ALL = "SELECT * FROM " + TABLE;

    private JdbcTemplate template;

    public ModelDao(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
    }

    public long add(Model model) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        template.update(connection -> {
            final PreparedStatement ps = connection.prepareStatement(SQL_INSERT, PK_COLUMN);
            ps.setString(1, model.getName());
            ps.setInt(2, model.getLifeTime());
            if (model.getTechnique() == null) {
                ps.setNull(3, java.sql.Types.INTEGER);
            } else {
                ps.setLong(3, model.getTechnique().getId());
            }
            if (model.getRadiostation() == null) {
                ps.setNull(4, java.sql.Types.INTEGER);
            } else {
                ps.setLong(4, model.getRadiostation().getId());
            }

            return ps;
        }, keyHolder);

        return keyHolder.getKey().longValue();
    }

    public void update(long id, Model model) {
        template.update(SQL_UPDATE,
                model.getName(),
                model.getLifeTime(),
                model.getTechnique() == null ? null : model.getTechnique().getId(),
                model.getRadiostation() == null ? null : model.getRadiostation().getId(),
                id);
    }

    public void remove(long id) {
        template.update(SQL_DELETE, id);
    }

    public List<Model> getRecords() {
        return template.query(SELECT_ALL,
                (rs, rowNum) -> getModel(rs));
    }

    public Model getRecord(long id) {
        return template.queryForObject(SELECT, new Object[]{id},
                (rs, rowNum) -> getModel(rs));
    }
}
