package ru.umvd.equipment.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import ru.umvd.equipment.entities.Diapason;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.List;

public class DiapasonDao {
    private static final String TABLE = "diapasons";
    private static final String COD = "cod_diapason";
    private static final String NAME_DIAPASON = "name_diapason";
    private static final String[] PK_COLUMN = {COD};

    private static final String SQL_INSERT = "INSERT INTO " + TABLE + " (" + NAME_DIAPASON + ") VALUES(?)";
    private static final String SQL_UPDATE = "UPDATE " + TABLE + " SET " + NAME_DIAPASON + " = ? WHERE " + COD + " = ?";
    private static final String SQL_DELETE = "DELETE FROM " + TABLE + " WHERE " + COD + " = ?";
    private static final String SELECT = "SELECT * FROM " + TABLE + " WHERE " + COD + " = ?";
    private static final String SELECT_ALL = "SELECT * FROM " + TABLE;

    private JdbcTemplate template;

    public DiapasonDao(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
    }

    public long add(Diapason diapason) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        template.update(connection -> {
            final PreparedStatement ps = connection.prepareStatement(SQL_INSERT, PK_COLUMN);
            ps.setString(1, diapason.getName());
            return ps;
        }, keyHolder);

        return keyHolder.getKey().longValue();
    }

    public void update(long id, Diapason diapason) {
        template.update(SQL_UPDATE, diapason.getName(), id);
    }

    public void remove(long id) {
        template.update(SQL_DELETE, id);
    }

    public List<Diapason> getRecords() {
        return template.query(SELECT_ALL,
                (rs, rowNum) -> new Diapason(rs.getLong(COD), rs.getString(NAME_DIAPASON)));
    }

    public Diapason getRecord(long id) {
        return template.queryForObject(SELECT, new Object[]{id},
                (rs, rowNum) -> new Diapason(rs.getLong(COD), rs.getString(NAME_DIAPASON)));
    }
}
