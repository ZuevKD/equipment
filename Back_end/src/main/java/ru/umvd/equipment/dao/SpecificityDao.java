package ru.umvd.equipment.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import ru.umvd.equipment.entities.Specificity;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.List;

public class SpecificityDao {
    private static final String TABLE = "specificities";
    private static final String COD = "cod_specificity";
    private static final String NAME_SPECIFICITY = "name_specificity";
    private static final String[] PK_COLUMN = {COD};

    private static final String SQL_INSERT = "INSERT INTO " + TABLE + " (" + NAME_SPECIFICITY + ") VALUES(?)";
    private static final String SQL_UPDATE = "UPDATE " + TABLE + " SET " + NAME_SPECIFICITY + " = ? WHERE " + COD + " = ?";
    private static final String SQL_DELETE = "DELETE FROM " + TABLE + " WHERE " + COD + " = ?";
    private static final String SELECT = "SELECT * FROM " + TABLE + " WHERE " + COD + " = ?";
    private static final String SELECT_ALL = "SELECT * FROM " + TABLE;

    private JdbcTemplate template;

    public SpecificityDao(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
    }

    public long add(Specificity specificity) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        template.update(connection -> {
            final PreparedStatement ps = connection.prepareStatement(SQL_INSERT, PK_COLUMN);
            ps.setString(1, specificity.getName());
            return ps;
        }, keyHolder);

        return keyHolder.getKey().longValue();
    }

    public void update(long id, Specificity specificity) {
        template.update(SQL_UPDATE, specificity.getName(), id);
    }

    public void remove(long id) {
        template.update(SQL_DELETE, id);
    }

    public List<Specificity> getRecords() {
        return template.query(SELECT_ALL,
                (rs, rowNum) -> new Specificity(rs.getLong(COD), rs.getString(NAME_SPECIFICITY)));
    }

    public Specificity getRecord(long id) {
        return template.queryForObject(SELECT, new Object[]{id},
                (rs, rowNum) -> new Specificity(rs.getLong(COD), rs.getString(NAME_SPECIFICITY)));
    }
}
