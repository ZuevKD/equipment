package ru.umvd.equipment.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import ru.umvd.equipment.entities.CommEquipment;
import ru.umvd.equipment.filters.Filter;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.Types;
import java.util.List;

import static ru.umvd.equipment.dao.Utils.getCommEquipments;

public class CommEquipmentsDao {
    private static final String TABLE = "comm_equipments";
    private static final String COD = "cod_comm_equipment";
    private static final String COD_BRANCH = "cod_branch";
    private static final String COD_TECHNIC = "cod_technic";
    private static final String COD_RADIOSTATION = "cod_radiostation";
    private static final String[] PK_COLUMN = {COD};
    private static final String NAME_MODEL = "name_model";
    private static final String SERIAL_NUMBER = "serial_number";
    private static final String INVENTORY_NUMBER = "inventory_number";
    private static final String LIFETIME = "lifetime";
    private static final String PRICE = "price";
    private static final String RELEASE_DATE = "release_date";
    private static final String COMMISSIONING_DATE = "commissioning_date";
    private static final String DISTRIBUTION_PLAN = "distribution_plan";
    private static final String COD_SUPPLY = "cod_supply";
    private static final String COD_EQUIPMENT = "cod_equipment";
    private static final String MODEL_OPERABILITY = "model_operability";
    private static final String COD_MODEL = "cod_model";

    private static final String SQL_INSERT = "INSERT INTO " + TABLE
            + " (" + COD_BRANCH + ", "
            + SERIAL_NUMBER + ", "
            + INVENTORY_NUMBER + ", "
            + PRICE + ", "
            + RELEASE_DATE + ", "
            + COMMISSIONING_DATE + ", "
            + DISTRIBUTION_PLAN + ", "
            + COD_SUPPLY + ", "
            + COD_EQUIPMENT + ", "
            + COD_MODEL + ", "
            + MODEL_OPERABILITY +
            ") VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    private static final String SQL_UPDATE = "UPDATE " + TABLE
            + " SET " + COD_BRANCH + " = ?,"
            + SERIAL_NUMBER + " = ?, "
            + INVENTORY_NUMBER + " = ?, "
            + PRICE + " = ?, "
            + RELEASE_DATE + " = ?, "
            + COMMISSIONING_DATE + " = ?, "
            + DISTRIBUTION_PLAN + " = ?, "
            + COD_SUPPLY + " = ?, "
            + COD_EQUIPMENT + " = ?, "
            + COD_MODEL + " = ?, "
            + MODEL_OPERABILITY + " = ? "
            + "WHERE " + COD + " = ?";

    private static final String SQL_DELETE = "DELETE FROM " + TABLE + " WHERE " + COD + " = ?";
//    private static final String SQL_DELETE_BY_RADIOSTATION = "DELETE FROM " + TABLE + " WHERE " + COD_RADIOSTATION + " = ?";
//    private static final String SQL_DELETE_BY_TECHNIC = "DELETE FROM " + TABLE + " WHERE " + COD_TECHNIC + " = ?";
    private static final String SELECT = "SELECT comm_equipments.cod_comm_equipment,\n" +
        "       comm_equipments.inventory_number,\n" +
        "       comm_equipments.commissioning_date,\n" +
        "       comm_equipments.distribution_plan,\n" +
        "       comm_equipments.model_operability,\n" +
        "       comm_equipments.price,\n" +
        "       comm_equipments.cod_branch,\n" +
        "       comm_equipments.release_date,\n" +
        "       comm_equipments.serial_number,\n" +
        "       models.cod_model,\n" +
        "       models.name_model,\n" +
        "       models.lifetime,\n" +
        "       diapasons.cod_diapason,\n" +
        "       diapasons.name_diapason,\n" +
        "       radiostations.cod_radiostation,\n" +
        "       radiostations.name_radiostation,\n" +
        "       radiostations.presence_sc_device,\n" +
        "       specificities.cod_specificity,\n" +
        "       specificities.name_specificity,\n" +
        "       equipments.cod_equipment,\n" +
        "       equipments.composition,\n" +
        "       equipments.commentary,\n" +
        "       technics.cod_technique,\n" +
        "       technics.name_technique,\n" +
        "       providers.cod_provider,\n" +
        "       providers.name_provider,\n" +
        "       supplies.cod_supply,\n" +
        "       supplies.invoice_number,\n" +
        "       supplies.invoice_date\n" +
        "FROM comm_equipments\n" +
        "         JOIN models On models.cod_model = comm_equipments.cod_model\n" +
        "         LEFT JOIN supplies on comm_equipments.cod_supply = supplies.cod_supply\n" +
        "         LEFT JOIN providers on supplies.cod_provider = providers.cod_provider\n" +
        "         LEFT JOIN radiostations on radiostations.cod_radiostation = models.cod_radiostation\n" +
        "         LEFT JOIN specificities on radiostations.cod_specificity = specificities.cod_specificity\n" +
        "         LEFT JOIN diapasons on diapasons.cod_diapason = radiostations.cod_diapason\n" +
        "        LEFT JOIN equipments on equipments.cod_equipment = comm_equipments.cod_equipment\n" +
        "         left JOIN technics on technics.cod_technique = models.cod_technique\n" +
        "WHERE comm_equipments.cod_branch = ?\n" +
        "  AND comm_equipments.cod_comm_equipment = ?\n" +
        "ORDER BY cod_radiostation ASC;";
    private static final String SELECT_ALL = "SELECT comm_equipments.cod_comm_equipment,\n" +
            "       comm_equipments.inventory_number,\n" +
            "       comm_equipments.commissioning_date,\n" +
            "       comm_equipments.distribution_plan,\n" +
            "       comm_equipments.model_operability,\n" +
            "       comm_equipments.price,\n" +
            "       comm_equipments.cod_branch,\n" +
            "       comm_equipments.release_date,\n" +
            "       comm_equipments.serial_number,\n" +
            "       models.cod_model,\n" +
            "       models.name_model,\n" +
            "       models.lifetime,\n" +
            "       diapasons.cod_diapason,\n" +
            "       diapasons.name_diapason,\n" +
            "       radiostations.cod_radiostation,\n" +
            "       radiostations.name_radiostation,\n" +
            "       radiostations.presence_sc_device,\n" +
            "       specificities.cod_specificity,\n" +
            "       specificities.name_specificity,\n" +
            "       equipments.cod_equipment,\n" +
            "       equipments.composition,\n" +
            "       equipments.commentary,\n" +
            "       technics.cod_technique,\n" +
            "       technics.name_technique,\n" +
            "       providers.cod_provider,\n" +
            "       providers.name_provider,\n" +
            "       supplies.cod_supply,\n" +
            "       supplies.invoice_number,\n" +
            "       supplies.invoice_date,\n" +
            "       branches.name_branch,\n" +
            "       departments.cod_department,\n" +
            "       departments.name_department\n" +
            "FROM comm_equipments\n" +
            "         JOIN branches on comm_equipments.cod_branch = branches.cod_branch\n" +
            "         JOIN departments on departments.cod_department = branches.cod_department\n" +
            "         JOIN models On models.cod_model = comm_equipments.cod_model\n" +
            "         LEFT JOIN supplies on comm_equipments.cod_supply = supplies.cod_supply\n" +
            "         LEFT JOIN providers on supplies.cod_provider = providers.cod_provider\n" +
            "         LEFT JOIN radiostations on radiostations.cod_radiostation = models.cod_radiostation\n" +
            "         LEFT JOIN specificities on radiostations.cod_specificity = specificities.cod_specificity\n" +
            "         LEFT JOIN diapasons on diapasons.cod_diapason = radiostations.cod_diapason\n" +
            "        LEFT JOIN equipments on equipments.cod_equipment = comm_equipments.cod_equipment\n" +
            "         left JOIN technics on technics.cod_technique = models.cod_technique\n" +
            "         %s\n" +
            "ORDER BY cod_radiostation ASC;";
    private static final String SELECT_ONLY_RADIOSTATIONS_BY_BRANCH_ID = "SELECT\n" +
            "       comm_equipments.cod_comm_equipment,\n" +
            "       comm_equipments.inventory_number,\n" +
            "       comm_equipments.commissioning_date,\n" +
            "       comm_equipments.distribution_plan,\n" +
            "       comm_equipments.model_operability,\n" +
            "       comm_equipments.price,\n" +
            "       comm_equipments.cod_branch,\n" +
            "       comm_equipments.release_date,\n" +
            "       comm_equipments.serial_number,\n" +
            "       models.cod_model,\n" +
            "       models.name_model,\n" +
            "       models.lifetime,\n" +
            "       models.cod_technique,\n" +
            "       diapasons.cod_diapason,\n" +
            "       diapasons.name_diapason,\n" +
            "       radiostations.cod_radiostation,\n" +
            "       radiostations.name_radiostation,\n" +
            "       radiostations.presence_sc_device,\n" +
            "       specificities.cod_specificity,\n" +
            "       specificities.name_specificity,\n" +
            "       equipments.cod_equipment,\n" +
            "       equipments.composition,\n" +
            "       equipments.commentary,\n" +
            "       providers.cod_provider,\n" +
            "       providers.name_provider,\n" +
            "       supplies.cod_supply,\n" +
            "       supplies.invoice_number,\n" +
            "       supplies.invoice_date\n" +
            "FROM comm_equipments\n" +
            "         JOIN models On models.cod_model = comm_equipments.cod_model\n" +
            "         LEFT JOIN supplies on comm_equipments.cod_supply = supplies.cod_supply\n" +
            "         LEFT JOIN providers on supplies.cod_provider = providers.cod_provider\n" +
            "         JOIN radiostations on radiostations.cod_radiostation = models.cod_radiostation\n" +
            "         LEFT JOIN specificities on radiostations.cod_specificity = specificities.cod_specificity\n" +
            "        LEFT JOIN diapasons on diapasons.cod_diapason = radiostations.cod_diapason\n" +
            "        LEFT JOIN equipments on equipments.cod_equipment = comm_equipments.cod_equipment\n" +
            "WHERE comm_equipments.cod_branch = ?\n" +
            "  AND models.cod_radiostation IS NOT NULL\n" +
            "ORDER BY cod_radiostation ASC;";

    private static final String SELECT_ONLY_TECNIQUES_BY_BRANCH_ID = "SELECT comm_equipments.cod_comm_equipment,\n" +
            "       comm_equipments.inventory_number,\n" +
            "       comm_equipments.commissioning_date,\n" +
            "       comm_equipments.distribution_plan,\n" +
            "       comm_equipments.model_operability,\n" +
            "       comm_equipments.price,\n" +
            "       comm_equipments.cod_branch,\n" +
            "       comm_equipments.release_date,\n" +
            "       comm_equipments.serial_number,\n" +
            "       models.cod_model,\n" +
            "       models.name_model,\n" +
            "       models.lifetime,\n" +
            "       technics.cod_technique,\n" +
            "       technics.name_technique,\n" +
            "       equipments.cod_equipment,\n" +
            "       equipments.composition,\n" +
            "       equipments.commentary,\n" +
            "       providers.cod_provider,\n" +
            "       providers.name_provider,\n" +
            "       supplies.cod_supply,\n" +
            "       supplies.invoice_number,\n" +
            "       supplies.invoice_date\n" +
            "FROM comm_equipments\n" +
            "         JOIN models On models.cod_model = comm_equipments.cod_model\n" +
            "         LEFT JOIN equipments on comm_equipments.cod_equipment = equipments.cod_equipment\n" +
            "         LEFT JOIN supplies on comm_equipments.cod_supply = supplies.cod_supply\n" +
            "         LEFT JOIN providers on supplies.cod_provider = providers.cod_provider\n" +
            "         LEFT JOIN technics on technics.cod_technique = models.cod_technique\n" +
            "WHERE comm_equipments.cod_branch = ?\n" +
            "  AND models.cod_technique IS NOT NULL\n" +
            "ORDER BY cod_technique ASC;";

    private JdbcTemplate template;

    public CommEquipmentsDao(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
    }

    public long add(CommEquipment commEquipment) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        template.update(connection -> {
            final PreparedStatement ps = connection.prepareStatement(SQL_INSERT, PK_COLUMN);
            ps.setLong(1, commEquipment.getIdBranch());
            ps.setString(2, commEquipment.getSerialNum());
            ps.setString(3, commEquipment.getInventoryNum());
            ps.setBigDecimal(4, commEquipment.getPrice());
            ps.setDate(5, commEquipment.getReleaseDate());
            ps.setDate(6, commEquipment.getCommissioningDate());
            ps.setDate(7, commEquipment.getDistributionPlan());
            if (commEquipment.getSupply() != null) {
                ps.setLong(8, commEquipment.getSupply().getId());
            } else {
                ps.setNull(8, java.sql.Types.INTEGER);
            }
            if (commEquipment.getEquipment() != null) {
                ps.setLong(9, commEquipment.getEquipment().getId());
            } else  {
                ps.setNull(9, Types.INTEGER);
            }
            ps.setLong(10, commEquipment.getModel().getId());
            ps.setBoolean(11, commEquipment.isModelOperability());

            return ps;
        }, keyHolder);

        return keyHolder.getKey().longValue();
    }

    public void update(long id, CommEquipment commEquipment) {
        template.update(SQL_UPDATE,
                commEquipment.getIdBranch(),
                commEquipment.getSerialNum(),
                commEquipment.getInventoryNum(),
                commEquipment.getPrice(),
                commEquipment.getReleaseDate(),
                commEquipment.getCommissioningDate(),
                commEquipment.getDistributionPlan(),
                commEquipment.getSupply() != null ? commEquipment.getSupply().getId() : null,
                commEquipment.getEquipment() != null ? commEquipment.getEquipment().getId() : null,
                commEquipment.getModel().getId(),
                commEquipment.isModelOperability(),
                id);
    }

    public void remove(long id) {
        template.update(SQL_DELETE, id);
    }

    public List<CommEquipment> getRecords(Filter filter) {
        return template.query(Utils.getQuery(SELECT_ALL, filter),
                (rs, rowNum) -> getCommEquipments(rs));
    }

    public List<CommEquipment> getOnlyRadiostationsByBranchId(long branchId) {
        return template.query(SELECT_ONLY_RADIOSTATIONS_BY_BRANCH_ID,  new Object[]{branchId},
                (rs, rowNum) -> getCommEquipments(rs));
    }

    public List<CommEquipment> getOnlyTechniquesByBranchId(long branchId) {
        return template.query(SELECT_ONLY_TECNIQUES_BY_BRANCH_ID,  new Object[]{branchId},
                (rs, rowNum) -> getCommEquipments(rs));
    }

    public CommEquipment getRecord(long id, long commEquipmentId) {
        return template.queryForObject(SELECT, new Object[]{id, commEquipmentId},
                (rs, rowNum) -> getCommEquipments(rs));
    }
}
