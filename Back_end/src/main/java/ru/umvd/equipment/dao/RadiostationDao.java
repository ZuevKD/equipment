package ru.umvd.equipment.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import ru.umvd.equipment.entities.Diapason;
import ru.umvd.equipment.entities.Model;
import ru.umvd.equipment.entities.Radiostation;
import ru.umvd.equipment.entities.Specificity;
import ru.umvd.equipment.filters.Filter;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static ru.umvd.equipment.dao.Utils.getCommEquipments;

public class RadiostationDao {
    private static final String TABLE = "radiostations";
    private static final String COD = "cod_radiostation";
    private static final String NAME_RADIOSTATION = "name_radiostation";
    private static final String NAME_BRANCH = "name_branch";
    private static final String NAME_DEPARTMENT = "name_department";

    private static final String COD_SPECIFICITY = "cod_specificity";
    private static final String COD_DIAPASON = "cod_diapason";
    private static final String PRESENCE_SC_DEVICE = "presence_sc_device";

    private static final String COD_MODEL = "cod_model";

    private static final String NAME_SPECIFICITY = "name_specificity";

    private static final String NAME_DIAPASON = "name_diapason";

    private static final String[] PK_COLUMN = {COD};

    private static final String SQL_INSERT = "INSERT INTO " + TABLE
            + " ("
            + NAME_RADIOSTATION + ", "
            + COD_SPECIFICITY + ", "
            + COD_DIAPASON + ", "
            + PRESENCE_SC_DEVICE
            + ") VALUES(?, ?, ?, ?)";

    private static final String SQL_UPDATE = "UPDATE " + TABLE
            + " SET "
            + NAME_RADIOSTATION + " = ?,"
            + COD_SPECIFICITY + " = ?,"
            + COD_DIAPASON + " = ?,"
            + PRESENCE_SC_DEVICE + " = ?"
            + "WHERE " + COD + " = ?";

    private static final String SQL_DELETE = "DELETE FROM " + TABLE + " WHERE " + COD + " = ?";
    private static final String SELECT = "SELECT radiostations.cod_radiostation,\n" +
            "       radiostations.name_radiostation,\n" +
            "       radiostations.presence_sc_device,\n" +
            "       diapasons.cod_diapason,\n" +
            "       diapasons.name_diapason,\n" +
            "       specificities.cod_specificity,\n" +
            "       specificities.name_specificity\n" +
            "FROM radiostations\n" +
            "         LEFT JOIN specificities on radiostations.cod_specificity = specificities.cod_specificity\n" +
            "         LEFT JOIN diapasons on diapasons.cod_diapason = radiostations.cod_diapason\n" +
            "WHERE radiostations.cod_radiostation = ?";

    private static final String SELECT_BY_ID_BRANCH = " SELECT radiostations.cod_radiostation,        radiostations.name_radiostation,        models.cod_model,        models.name_model,        diapasons.cod_diapason,        diapasons.name_diapason,        radiostations.presence_sc_device,        models.inventory_number,        models.lifetime,        models.commissioning_date,        models.distribution_plan,        models.model_operability,        models.price,        models.release_date,        models.serial_number,        specificities.cod_specificity,        specificities.name_specificity,        equipments.cod_equipment,        equipments.composition,        equipments.commentary,        providers.cod_provider,        providers.name_provider,        supplies.cod_supply,        supplies.invoice_number,        supplies.invoice_date FROM comm_equipments        JOIN radiostations On comm_equipments.cod_radiostation = radiostations.cod_radiostation        JOIN models On models.cod_model = radiostations.cod_model        LEFT JOIN supplies on models.cod_supply = supplies.cod_supply        LEFT JOIN providers on supplies.cod_provider = providers.cod_provider        LEFT JOIN specificities on radiostations.cod_specificity = specificities.cod_specificity        JOIN diapasons on diapasons.cod_diapason = radiostations.cod_diapason        JOIN equipments on equipments.cod_equipment = models.cod_equipment WHERE comm_equipments.cod_branch = ?   AND comm_equipments.cod_radiostation IS NOT NULL ORDER  BY cod_radiostation ASC;";
    private static final String SELECT_ALL = "SELECT radiostations.cod_radiostation,\n" +
            "       radiostations.name_radiostation,\n" +
            "       models.cod_model,\n" +
            "       models.name_model,\n" +
            "       diapasons.cod_diapason,\n" +
            "       diapasons.name_diapason,\n" +
            "       radiostations.presence_sc_device,\n" +
            "       models.inventory_number,\n" +
            "       models.lifetime,\n" +
            "       models.commissioning_date,\n" +
            "       models.distribution_plan,\n" +
            "       models.model_operability,\n" +
            "       models.price,\n" +
            "       models.release_date,\n" +
            "       models.serial_number,\n" +
            "       specificities.cod_specificity,\n" +
            "       specificities.name_specificity,\n" +
            "       equipments.cod_equipment,\n" +
            "       equipments.composition,\n" +
            "       equipments.commentary,\n" +
            "       providers.cod_provider,\n" +
            "       providers.name_provider,\n" +
            "       supplies.cod_supply,\n" +
            "       supplies.invoice_number,\n" +
            "       supplies.invoice_date,\n" +
            "       departments.name_department,\n" +
            "       branches.name_branch\n" +
            "FROM comm_equipments\n" +
            "         JOIN radiostations On comm_equipments.cod_radiostation = radiostations.cod_radiostation\n" +
            "         JOIN models On models.cod_model = radiostations.cod_model\n" +
            "         LEFT JOIN supplies on models.cod_supply = supplies.cod_supply\n" +
            "         LEFT JOIN providers on supplies.cod_provider = providers.cod_provider\n" +
            "         LEFT JOIN specificities on radiostations.cod_specificity = specificities.cod_specificity\n" +
            "         JOIN diapasons on diapasons.cod_diapason = radiostations.cod_diapason\n" +
            "         JOIN equipments on equipments.cod_equipment = models.cod_equipment\n" +
            "         JOIN branches on comm_equipments.cod_branch = branches.cod_branch\n" +
            "         JOIN departments on branches.cod_department = departments.cod_department\n" +
            "WHERE comm_equipments.cod_radiostation IS NOT NULL %s \n" +
            "ORDER BY cod_radiostation ASC;";

    private JdbcTemplate template;

    public RadiostationDao(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
    }

    public long add(Radiostation radiostation) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        template.update(connection -> {
            final PreparedStatement ps = connection.prepareStatement(SQL_INSERT, PK_COLUMN);
            ps.setString(1, radiostation.getName());
            if (radiostation.getSpecificity() != null) {
                ps.setLong(2, radiostation.getSpecificity().getId());
            } else {
                ps.setNull(2, java.sql.Types.INTEGER);
            }
            ps.setLong(3, radiostation.getDiapason().getId());
            ps.setBoolean(4, radiostation.isPresenceScDevice());

            return ps;
        }, keyHolder);

        return keyHolder.getKey().longValue();
    }

    public void update(long id, Radiostation radiostation) {
        template.update(SQL_UPDATE,
                radiostation.getName(),
                radiostation.getSpecificity() == null ? null : radiostation.getSpecificity().getId(),
                radiostation.getDiapason().getId(),
                radiostation.isPresenceScDevice(),
                id);
    }

    public void remove(long id) {
        template.update(SQL_DELETE, id);
    }

    public List<Radiostation> getRecords(long idBranch) {
        return template.query(SELECT_BY_ID_BRANCH, new Object[]{idBranch},
                (rs, rowNum) -> getRaioststion(rs));
    }

    public List<Radiostation> getRecords(Filter filter) {
        return template.query(Utils.getQuery(SELECT_ALL, filter),
                (rs, rowNum) -> getRaioststion(rs));
    }

    public Radiostation getRecord(long id) {
        return template.queryForObject(SELECT, new Object[]{id},
                (rs, rowNum) -> getRaioststion(rs));
    }

    private Radiostation getRaioststion(ResultSet rs) throws SQLException {
        Specificity specificity = new Specificity(
                rs.getLong(COD_SPECIFICITY),
                rs.getString(NAME_SPECIFICITY));

        Diapason diapason = new Diapason(
                rs.getLong(COD_DIAPASON),
                rs.getString(NAME_DIAPASON));

        return new Radiostation(
                rs.getLong(COD),
                rs.getString(NAME_RADIOSTATION),
                specificity,
                diapason,
                rs.getBoolean(PRESENCE_SC_DEVICE)
        );
    }
}
