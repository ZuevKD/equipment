package ru.umvd.equipment.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import ru.umvd.equipment.entities.Provider;
import ru.umvd.equipment.entities.Supply;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.List;

public class SupplyDao {
    private static final String TABLE = "supplies";
    private static final String COD = "cod_supply";
    private static final String INVOICE_NUMBER = "invoice_number";
    private static final String INVOICE_DATE = "invoice_date";
    private static final String COD_PROVIDER = "cod_provider";
    private static final String[] PK_COLUMN = {COD};

    private static final String NAME_PROVIDER = "name_provider";

    private static final String SQL_INSERT = "INSERT INTO " + TABLE
            + " ("
            + INVOICE_NUMBER + ", "
            + INVOICE_DATE + ", "
            + COD_PROVIDER
            + ") VALUES(?, ?, ?)";

    private static final String SQL_UPDATE = "UPDATE " + TABLE
            + " SET "
            + INVOICE_NUMBER + " = ?, "
            + INVOICE_DATE + " = ?, "
            + COD_PROVIDER + " = ?"
            + "WHERE " + COD + " = ?";

    private static final String SQL_DELETE = "DELETE FROM " + TABLE + " WHERE " + COD + " = ?";
    private static final String SELECT = "SELECT supplies.cod_supply,\n" +
            "       supplies.invoice_number,\n" +
            "       supplies.invoice_date,\n" +
            "       providers.cod_provider,\n" +
            "       providers.name_provider\n" +
            "FROM supplies\n" +
            "         LEFT JOIN providers on supplies.cod_provider = providers.cod_provider\n" +
            "WHERE supplies.cod_supply = ?;";
    private static final String SELECT_ALL = "SELECT * FROM " + TABLE;


    private JdbcTemplate template;

    public SupplyDao(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
    }

    public long add(Supply supply) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        template.update(connection -> {
            final PreparedStatement ps = connection.prepareStatement(SQL_INSERT, PK_COLUMN);
            ps.setString(1, supply.getInvoiceNum());
            ps.setDate(2, supply.getDate());
            ps.setLong(3, supply.getProvider().getId());
            return ps;
        }, keyHolder);

        return keyHolder.getKey().longValue();
    }

    public void update(long id, Supply supply) {
        template.update(SQL_UPDATE,
                supply.getInvoiceNum(),
                supply.getDate(),
                supply.getProvider().getId(),
                id);
    }

    public void remove(long id) {
        template.update(SQL_DELETE, id);
    }

    public List<Supply> getRecords() {
        return template.query(SELECT_ALL,
                (rs, rowNum) -> {
                    Provider provider = new Provider(
                            rs.getLong(COD_PROVIDER),
                            rs.getString(NAME_PROVIDER)
                    );

                    return new Supply(
                            rs.getLong(COD),
                            rs.getString(INVOICE_NUMBER),
                            rs.getDate(INVOICE_DATE),
                            provider
                    );
                });
    }

    public Supply getRecord(long id) {
        return template.queryForObject(SELECT, new Object[]{id},
                (rs, rowNum) -> {
                    Provider provider = new Provider(
                            rs.getLong(COD_PROVIDER),
                            rs.getString(NAME_PROVIDER)
                    );

                    return new Supply(
                            rs.getLong(COD),
                            rs.getString(INVOICE_NUMBER),
                            rs.getDate(INVOICE_DATE),
                            provider
                    );
                });
    }
}
