package ru.umvd.equipment.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import ru.umvd.equipment.entities.Model;
import ru.umvd.equipment.entities.Technique;
import ru.umvd.equipment.filters.Filter;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.List;

import static ru.umvd.equipment.dao.Utils.getCommEquipments;

public class TechniqueDao {
    private static final String TABLE = "technics";
    private static final String COD = "cod_technique";
    private static final String NAME_TECHNIC = "name_technique";
    private static final String NAME_BRANCH = "name_branch";
    private static final String NAME_DEPARTMENT = "name_department";

    private static final String[] PK_COLUMN = {COD};

    private static final String COD_MODEL = "cod_model";

    private static final String SELECT = "SELECT * FROM " + TABLE + " WHERE " + COD + " = ?";
    private static final String SELECT_ALL = " SELECT technics.cod_technic,\n" +
            "       technics.name_technic,\n" +
            "       models.cod_model,\n" +
            "       models.name_model,\n" +
            "       models.inventory_number,\n" +
            "       models.lifetime,\n" +
            "       models.commissioning_date,\n" +
            "       models.distribution_plan,\n" +
            "       models.model_operability,\n" +
            "       models.price,\n" +
            "       models.release_date,\n" +
            "       models.serial_number,\n" +
            "       equipments.cod_equipment,\n" +
            "       equipments.composition,\n" +
            "       equipments.commentary,\n" +
            "       providers.cod_provider,\n" +
            "       providers.name_provider,\n" +
            "       supplies.cod_supply,\n" +
            "       supplies.invoice_number,\n" +
            "       supplies.invoice_date,\n" +
            "       departments.name_department,\n" +
            "       branches.name_branch\n" +
            "FROM comm_equipments\n" +
            "         JOIN technics On comm_equipments.cod_technic = technics.cod_technic\n" +
            "         JOIN models On models.cod_model = technics.cod_model\n" +
            "         LEFT JOIN supplies on models.cod_supply = supplies.cod_supply\n" +
            "         LEFT JOIN providers on supplies.cod_provider = providers.cod_provider\n" +
            "         LEFT JOIN equipments on equipments.cod_equipment = models.cod_equipment\n" +
            "         JOIN branches on comm_equipments.cod_branch = branches.cod_branch\n" +
            "         JOIN departments on branches.cod_department = departments.cod_department\n" +
            "WHERE comm_equipments.cod_technic IS NOT NULL %s \n" +
            "ORDER BY cod_technic ASC;";

    private JdbcTemplate template;

    public TechniqueDao(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
    }

    private static final String SQL_INSERT = "INSERT INTO " + TABLE
            + " (" + NAME_TECHNIC + ") VALUES(?)";

    public long add(Technique technique) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        template.update(connection -> {
            final PreparedStatement ps = connection.prepareStatement(SQL_INSERT, PK_COLUMN);
            ps.setString(1, technique.getName());
            return ps;
        }, keyHolder);

        return keyHolder.getKey().longValue();
    }

    private static final String SQL_UPDATE = "UPDATE " + TABLE
            + " SET " + NAME_TECHNIC + " = ? "
            + "WHERE " + COD + " = ?";

    public void update(long id, Technique technique) {
        template.update(SQL_UPDATE, technique.getName(), id);
    }

    private static final String SQL_DELETE = "DELETE FROM " + TABLE + " WHERE " + COD + " = ?";

    public void remove(long id) {
        template.update(SQL_DELETE, id);
    }

    private static final String SELECT_BY_BRANCH_ID = "SELECT technics.cod_technic,\n" +
            "       technics.name_technic,\n" +
            "       models.cod_model,\n" +
            "       models.name_model,\n" +
            "       models.inventory_number,\n" +
            "       models.lifetime,\n" +
            "       models.commissioning_date,\n" +
            "       models.distribution_plan,\n" +
            "       models.model_operability,\n" +
            "       models.price,\n" +
            "       models.release_date,\n" +
            "       models.serial_number,\n" +
            "       equipments.cod_equipment,\n" +
            "       equipments.composition,\n" +
            "       supplies.invoice_number,\n" +
            "       supplies.invoice_date\n" +
            "FROM comm_equipments\n" +
            "         JOIN technics On comm_equipments.cod_technic = technics.cod_technic\n" +
            "         JOIN models On models.cod_model = technics.cod_model\n" +
            "         LEFT JOIN supplies on models.cod_supply = supplies.cod_supply\n" +
            "         LEFT JOIN providers on supplies.cod_provider = providers.cod_provider\n" +
            "         LEFT JOIN equipments on equipments.cod_equipment = models.cod_equipment\n" +
            "WHERE comm_equipments.cod_branch = ?\n" +
            "  AND comm_equipments.cod_technic IS NOT NULL\n" +
            "ORDER BY cod_technic ASC;";

    public List<Technique> getRecords(long idBranch) {
        return template.query(SELECT_BY_BRANCH_ID, new Object[]{idBranch},
                (rs, rowNum) -> new Technique(
                        rs.getLong(COD),
                        rs.getString(NAME_TECHNIC)
                ));
    }

    public List<Technique> getRecords(Filter filter) {
        return template.query(Utils.getQuery(SELECT_ALL, filter),
                (rs, rowNum) -> new Technique(
                        rs.getLong(COD),
                        rs.getString(NAME_TECHNIC)
                ));
    }

    public Technique getRecord(long id) {
        return template.queryForObject(SELECT, new Object[]{id},
                (rs, rowNum) ->
                        new Technique(
                                rs.getLong(COD),
                                rs.getString(NAME_TECHNIC)
                        )
        );
    }
}
