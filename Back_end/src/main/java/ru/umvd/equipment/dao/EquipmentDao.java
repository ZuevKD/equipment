package ru.umvd.equipment.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import ru.umvd.equipment.entities.Equipment;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.List;

public class EquipmentDao {
    private static final String TABLE = "equipments";
    private static final String COD = "cod_equipment";
    private static final String COMPOSITION = "composition";
    private static final String COMMENTARY = "commentary";
    private static final String[] PK_COLUMN = {COD};

    private static final String SQL_INSERT = "INSERT INTO " + TABLE
            + " (" + COMPOSITION + ", " + COMMENTARY + ") VALUES(?, ?)";

    private static final String SQL_UPDATE = "UPDATE " + TABLE
            + " SET " + COMPOSITION + " = ?," + COMMENTARY + " = ? "
            + "WHERE " + COD + " = ?";

    private static final String SQL_DELETE = "DELETE FROM " + TABLE + " WHERE " + COD + " = ?";
    private static final String SELECT = "SELECT * FROM " + TABLE + " WHERE " + COD + " = ?";
    private static final String SELECT_ALL = "SELECT * FROM " + TABLE;

    private JdbcTemplate template;

    public EquipmentDao(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
    }

    public long add(Equipment equipment) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        template.update(connection -> {
            final PreparedStatement ps = connection.prepareStatement(SQL_INSERT, PK_COLUMN);
            ps.setString(1, equipment.getComposition());
            ps.setString(2, equipment.getDescription());
            return ps;
        }, keyHolder);

        return keyHolder.getKey().longValue();
    }

    public void update(long id, Equipment equipment) {
        template.update(SQL_UPDATE, equipment.getComposition(), equipment.getDescription(), id);
    }

    public void remove(long id) {
        template.update(SQL_DELETE, id);
    }

    public List<Equipment> getRecords() {
        return template.query(SELECT_ALL,
                (rs, rowNum) -> new Equipment(
                        rs.getLong(COD),
                        rs.getString(COMPOSITION),
                        rs.getString(COMMENTARY)
                ));
    }

    public Equipment getRecord(long id) {
        return template.queryForObject(SELECT, new Object[]{id},
                (rs, rowNum) -> new Equipment(rs.getLong(COD), rs.getString(COMPOSITION), rs.getString(COMMENTARY)));
    }
}
