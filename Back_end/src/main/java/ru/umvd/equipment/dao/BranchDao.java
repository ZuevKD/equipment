package ru.umvd.equipment.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import ru.umvd.equipment.entities.Branch;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.List;

public class BranchDao {
    private static final String TABLE = "branches";
    private static final String COD = "cod_branch";
    private static final String NAME_BRANCH = "name_branch";
    private static final String COD_DEPARTMENT = "cod_department";
    private static final String[] PK_COLUMN = {COD};

    private static final String SQL_INSERT = "INSERT INTO " + TABLE
            + "(" + NAME_BRANCH + ","
            + COD_DEPARTMENT + ")"
            + " VALUES(?, ?)";
    private static final String SQL_UPDATE = "UPDATE " + TABLE
            + " SET " + NAME_BRANCH + " = ?,"
            + COD_DEPARTMENT + " = ?"
            + " WHERE " + COD + " = ?";
    private static final String SQL_DELETE = "DELETE FROM " + TABLE + " WHERE " + COD + " = ?";
    private static final String SELECT = "SELECT * FROM " + TABLE + " WHERE " + COD + " = ?";
    private static final String SELECT_ALL = "SELECT * FROM " + TABLE + " ORDER BY " + COD + " ASC";
    private static final String SELECT_BY_COD_DEPARTMENT = "SELECT * FROM " + TABLE + " WHERE " + COD_DEPARTMENT + " = ?" + " ORDER BY " + COD + " ASC";

    private JdbcTemplate template;

    public BranchDao(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
    }

    public long add(Branch branch) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        template.update(connection -> {
            final PreparedStatement ps = connection.prepareStatement(SQL_INSERT, PK_COLUMN);
            ps.setString(1, branch.getName());
            ps.setLong(2, branch.getIdDepartment());

            return ps;
        }, keyHolder);

        return keyHolder.getKey().longValue();
    }

    public void update(long id, Branch branch) {
        template.update(SQL_UPDATE, branch.getName(), branch.getIdDepartment(), id);
    }

    public void remove(long id) {
        template.update(SQL_DELETE, id);
    }

    public List<Branch> getRecords() {
        return template.query(SELECT_ALL,
                (rs, rowNum) -> new Branch(rs.getLong(COD), rs.getString(NAME_BRANCH), rs.getLong(COD_DEPARTMENT)));
    }

    public Branch getRecord(long id) {
        return template.queryForObject(SELECT, new Object[]{id},
                (rs, rowNum) -> new Branch(rs.getLong(COD), rs.getString(NAME_BRANCH), rs.getLong(COD_DEPARTMENT)));
    }

    public List<Branch> getRecordsByCodDepartment(long idDepartment) {
        return template.query(SELECT_BY_COD_DEPARTMENT, new Object[]{idDepartment},
                (rs, rowNum) -> new Branch(rs.getLong(COD), rs.getString(NAME_BRANCH), rs.getLong(COD_DEPARTMENT)));
    }
}
