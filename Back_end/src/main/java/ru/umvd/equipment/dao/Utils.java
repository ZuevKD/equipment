package ru.umvd.equipment.dao;

import ru.umvd.equipment.entities.CommEquipment;
import ru.umvd.equipment.entities.Diapason;
import ru.umvd.equipment.entities.Equipment;
import ru.umvd.equipment.entities.Model;
import ru.umvd.equipment.entities.Provider;
import ru.umvd.equipment.entities.Radiostation;
import ru.umvd.equipment.entities.Specificity;
import ru.umvd.equipment.entities.Supply;
import ru.umvd.equipment.entities.Technique;
import ru.umvd.equipment.filters.Filter;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.apache.logging.log4j.util.Strings.isNotBlank;

public class Utils {
    private static final String COD_PROVIDER = "cod_provider";
    private static final String NAME_PROVIDER = "name_provider";

    private static final String COD_SUPPLY = "cod_supply";
    private static final String INVOICE_NUMBER = "invoice_number";
    private static final String INVOICE_DATE = "invoice_date";

    private static final String COD_EQUIPMENT = "cod_equipment";
    private static final String COMPOSITION = "composition";
    private static final String COMMENTARY = "commentary";

    private static final String COD_MODEL = "cod_model";
    private static final String NAME_MODEL = "name_model";
    private static final String LIFETIME = "lifetime";

    private static final String COD = "cod_comm_equipment";
    private static final String COD_BRANCH = "cod_branch";
    private static final String SERIAL_NUMBER = "serial_number";
    private static final String INVENTORY_NUMBER = "inventory_number";
    private static final String PRICE = "price";
    private static final String RELEASE_DATE = "release_date";
    private static final String COMMISSIONING_DATE = "commissioning_date";
    private static final String DISTRIBUTION_PLAN = "distribution_plan";

    private static final String MODEL_OPERABILITY = "model_operability";

    private static final String COD_TECHNIQUE = "cod_technique";
    private static final String COD_RADIOSTATION = "cod_radiostation";

    private static final String NAME_RADIOSTATION = "name_radiostation";
    private static final String NAME_TECHNIQUE = "name_technique";

    private static final String COD_SPECIFICITY = "cod_specificity";
    private static final String COD_DIAPASON = "cod_diapason";
    private static final String PRESENCE_SC_DEVICE = "presence_sc_device";


    private static final String NAME_SPECIFICITY = "name_specificity";

    private static final String NAME_DIAPASON = "name_diapason";

    private Utils() {

    }

    public static CommEquipment getCommEquipments(ResultSet rs) throws SQLException {
        Provider provider = new Provider(
                rs.getLong(COD_PROVIDER),
                rs.getString(NAME_PROVIDER)
        );

        Supply supply = new Supply(
                rs.getLong(COD_SUPPLY),
                rs.getString(INVOICE_NUMBER),
                rs.getDate(INVOICE_DATE),
                provider
        );

        Equipment equipment = new Equipment(
                rs.getLong(COD_EQUIPMENT),
                rs.getString(COMPOSITION),
                rs.getString(COMMENTARY)
        );

        Model model = getModel(rs);

        String nameDepartment;
        String nameBranch;

        try {
            nameDepartment =  rs.getString("name_department");
            nameBranch =  rs.getString("name_branch");
        } catch(Exception e) {
            nameBranch = "";
            nameDepartment = "";
        }

        return new CommEquipment(
                rs.getLong(COD),
                rs.getLong(COD_BRANCH),
                model,
                rs.getString(SERIAL_NUMBER),
                rs.getString(INVENTORY_NUMBER),
                rs.getBigDecimal(PRICE),
                rs.getDate(RELEASE_DATE),
                rs.getDate(COMMISSIONING_DATE),
                rs.getDate(DISTRIBUTION_PLAN),
                equipment,
                rs.getBoolean(MODEL_OPERABILITY),
                supply,
                nameDepartment,
                nameBranch
        );
    }

    public static Model getModel(ResultSet rs) throws SQLException {
        if (rs.getObject(COD_TECHNIQUE) == null) {
            Specificity specificity = new Specificity(
                    rs.getLong(COD_SPECIFICITY),
                    rs.getString(NAME_SPECIFICITY));

            Diapason diapason = new Diapason(
                    rs.getLong(COD_DIAPASON),
                    rs.getString(NAME_DIAPASON));

            Radiostation radiostation = new Radiostation(
                    rs.getLong(COD_RADIOSTATION),
                    rs.getString(NAME_RADIOSTATION),
                    specificity,
                    diapason,
                    rs.getBoolean(PRESENCE_SC_DEVICE)
            );

            return new Model(rs.getLong(COD_MODEL),
                    rs.getString(NAME_MODEL),
                    rs.getInt(LIFETIME),
                    radiostation,
                    null);
        } else {

            Technique technique = new Technique(
                    rs.getLong(COD_TECHNIQUE),
                    rs.getString(NAME_TECHNIQUE)
            );

            return new Model(rs.getLong(COD_MODEL),
                    rs.getString(NAME_MODEL),
                    rs.getInt(LIFETIME),
                    null,
                    technique);
        }
    }

    public static String getQuery(String query, Filter filter) {
        if (filter == null) {
            return String.format(query, "");
        }

        String where = "";

        if (isNotBlank(filter.getDepartment())) {
            where += getWhereOrAnd(where) + " lower(departments.name_department) LIKE lower('%" + filter.getDepartment() + "%') ";
        }

        if (isNotBlank(filter.getBranch())) {
            where +=  getWhereOrAnd(where) + " lower(branches.name_branch) LIKE lower('%" + filter.getBranch() + "%') ";
        }

        if (isNotBlank(filter.getModel())) {
            where += getWhereOrAnd(where) + " lower(models.name_model) LIKE lower('%" + filter.getModel() + "%') ";
        }

        if (isNotBlank(filter.getTechnique())) {
            where += getWhereOrAnd(where) + " (lower(radiostations.name_radiostation) LIKE lower('%" + filter.getTechnique() + "%') \n"
                    + "OR lower(technics.name_technique) LIKE lower('%" + filter.getTechnique() + "%')) ";
        }

        if (filter.getDate() != null) {
            where += getWhereOrAnd(where) + " comm_equipments.release_date = '" + new java.sql.Date(filter.getDate().getTime()) + "' ";
        }

        if (filter.getDistributionPlan() != null) {
            where += getWhereOrAnd(where) + " comm_equipments.distribution_plan = '" + new java.sql.Date(filter.getDistributionPlan().getTime()) + "' ";
        }

        if (isNotBlank(filter.getSerialNum())) {
            where += getWhereOrAnd(where) + " lower(comm_equipments.serial_number) LIKE lower('%" + filter.getSerialNum() + "%') ";
        }

        if (isNotBlank(filter.getInventoryNum())) {
            where += getWhereOrAnd(where) + " lower(comm_equipments.inventory_number) LIKE lower('%" + filter.getInventoryNum() + "%') ";
        }

        return String.format(query, where);
    }

    private static String getWhereOrAnd(String conditional) {
        if ("".contains(conditional)) {
            return " where ";
        }

        return " AND ";
    }
}
