package ru.umvd.equipment.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import ru.umvd.equipment.entities.Department;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.List;

public class DepartmentDao {
    private static final String TABLE = "departments";
    private static final String COD = "cod_department";
    private static final String NAME_DEPARTMENT = "name_department";
    private static final String[] PK_COLUMN = {COD};

    private static final String SQL_INSERT = "INSERT INTO " + TABLE
            + " (" + NAME_DEPARTMENT + ") VALUES(?)";

    private static final String SQL_UPDATE = "UPDATE " + TABLE
            + " SET " + NAME_DEPARTMENT + " = ?"
            + "WHERE " + COD + " = ?";

    private static final String SQL_DELETE = "DELETE FROM " + TABLE + " WHERE " + COD + " = ?";
    private static final String SELECT = "SELECT * FROM " + TABLE + " WHERE " + COD + " = ?";
    private static final String SELECT_ALL = "SELECT * FROM " + TABLE + " ORDER BY " + COD + " ASC";

    private JdbcTemplate template;

    public DepartmentDao(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
    }

    public long add(Department department) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        template.update(connection -> {
            final PreparedStatement ps = connection.prepareStatement(SQL_INSERT, PK_COLUMN);
            ps.setString(1, department.getName());
            return ps;
        }, keyHolder);

        return keyHolder.getKey().longValue();
    }

    public void update(long id, Department department) {
        template.update(SQL_UPDATE, department.getName(), id);
    }

    public void remove(long id) {
        template.update(SQL_DELETE, id);
    }

    public List<Department> getRecords() {
        return template.query(SELECT_ALL,
                (rs, rowNum) -> new Department(rs.getLong(COD), rs.getString(NAME_DEPARTMENT)));
    }

    public Department getRecord(long id) {
        return template.queryForObject(SELECT, new Object[]{id},
                (rs, rowNum) -> new Department(rs.getLong(COD), rs.getString(NAME_DEPARTMENT)));
    }
}