package ru.umvd.equipment.entities;

public class Specificity {
    private long id;
    private String name;

    public Specificity() {
    }

    public Specificity(String name) {
        this.name = name;
    }

    public Specificity(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
