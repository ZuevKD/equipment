package ru.umvd.equipment.entities;

import java.math.BigDecimal;
import java.sql.Date;

public class CommEquipment {
    private long id;
    private Long idBranch;
    private Model model;
    private String serialNum;
    private String inventoryNum;
    private BigDecimal price;
    private Date releaseDate;
    private Date commissioningDate;
    private Date distributionPlan;
    private Equipment equipment;
    private boolean modelOperability;
    private Supply supply;
    private String nameDepartment;
    private String nameBranch;

    public CommEquipment() {
    }

    public CommEquipment(Long idBranch, Model model, String serialNum, String inventoryNum, BigDecimal price,
                         Date releaseDate, Date commissioningDate, Date distributionPlan,
                         Equipment equipment, boolean modelOperability, Supply supply) {
        this.idBranch = idBranch;
        this.model = model;
        this.serialNum = serialNum;
        this.inventoryNum = inventoryNum;
        this.price = price;
        this.releaseDate = releaseDate;
        this.commissioningDate = commissioningDate;
        this.distributionPlan = distributionPlan;
        this.equipment = equipment;
        this.modelOperability = modelOperability;
        this.supply = supply;
    }

    public CommEquipment(long id, Long idBranch, Model model, String serialNum, String inventoryNum, BigDecimal price,
                         Date releaseDate, Date commissioningDate, Date distributionPlan, Equipment equipment,
                         boolean modelOperability, Supply supply) {
        this.id = id;
        this.idBranch = idBranch;
        this.model = model;
        this.serialNum = serialNum;
        this.inventoryNum = inventoryNum;
        this.price = price;
        this.releaseDate = releaseDate;
        this.commissioningDate = commissioningDate;
        this.distributionPlan = distributionPlan;
        this.equipment = equipment;
        this.modelOperability = modelOperability;
        this.supply = supply;
    }

    public CommEquipment(long id, Long idBranch, Model model, String serialNum, String inventoryNum, BigDecimal price, Date releaseDate, Date commissioningDate, Date distributionPlan, Equipment equipment, boolean modelOperability, Supply supply, String nameDepartment, String nameBranch) {
        this.id = id;
        this.idBranch = idBranch;
        this.model = model;
        this.serialNum = serialNum;
        this.inventoryNum = inventoryNum;
        this.price = price;
        this.releaseDate = releaseDate;
        this.commissioningDate = commissioningDate;
        this.distributionPlan = distributionPlan;
        this.equipment = equipment;
        this.modelOperability = modelOperability;
        this.supply = supply;
        this.nameDepartment = nameDepartment;
        this.nameBranch = nameBranch;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getIdBranch() {
        return idBranch;
    }

    public void setIdBranch(Long idBranch) {
        this.idBranch = idBranch;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public String getSerialNum() {
        return serialNum;
    }

    public void setSerialNum(String serialNum) {
        this.serialNum = serialNum;
    }

    public String getInventoryNum() {
        return inventoryNum;
    }

    public void setInventoryNum(String inventoryNum) {
        this.inventoryNum = inventoryNum;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Date getCommissioningDate() {
        return commissioningDate;
    }

    public void setCommissioningDate(Date commissioningDate) {
        this.commissioningDate = commissioningDate;
    }

    public Date getDistributionPlan() {
        return distributionPlan;
    }

    public void setDistributionPlan(Date distributionPlan) {
        this.distributionPlan = distributionPlan;
    }

    public Equipment getEquipment() {
        return equipment;
    }

    public void setEquipment(Equipment equipment) {
        this.equipment = equipment;
    }

    public boolean isModelOperability() {
        return modelOperability;
    }

    public void setModelOperability(boolean modelOperability) {
        this.modelOperability = modelOperability;
    }

    public Supply getSupply() {
        return supply;
    }

    public void setSupply(Supply supply) {
        this.supply = supply;
    }

    public String getNameDepartment() {
        return nameDepartment;
    }

    public void setNameDepartment(String nameDepartment) {
        this.nameDepartment = nameDepartment;
    }

    public String getNameBranch() {
        return nameBranch;
    }

    public void setNameBranch(String nameBranch) {
        this.nameBranch = nameBranch;
    }
}
