package ru.umvd.equipment.entities;

public class Technique implements Entity{
    private long id;
    private String name;

    public Technique() {
    }

    public Technique(String name) {
        this.name = name;
    }

    public Technique(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
