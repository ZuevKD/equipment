package ru.umvd.equipment.entities;

public interface ITechnique extends Entity{
    Model getModel();

    String getNameBranch();

    String getNameDepartment();
}
