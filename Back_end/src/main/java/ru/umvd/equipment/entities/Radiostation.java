package ru.umvd.equipment.entities;

public class Radiostation implements Entity {
    private long id;
    private String name;
    private Specificity specificity;
    private Diapason diapason;
    private boolean presenceScDevice;

    public Radiostation() {
    }

    public Radiostation(String name, Specificity specificity, Diapason diapason, boolean presenceScDevice) {
        this.name = name;
        this.specificity = specificity;
        this.diapason = diapason;
        this.presenceScDevice = presenceScDevice;
    }

    public Radiostation(long id, String name, Specificity specificity, Diapason diapason, boolean presenceScDevice) {
        this.id = id;
        this.name = name;
        this.specificity = specificity;
        this.diapason = diapason;
        this.presenceScDevice = presenceScDevice;
    }


    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Specificity getSpecificity() {
        return specificity;
    }

    public void setSpecificity(Specificity specificity) {
        this.specificity = specificity;
    }

    public Diapason getDiapason() {
        return diapason;
    }

    public void setDiapason(Diapason diapason) {
        this.diapason = diapason;
    }

    public boolean isPresenceScDevice() {
        return presenceScDevice;
    }

    public void setPresenceScDevice(boolean presenceScDevice) {
        this.presenceScDevice = presenceScDevice;
    }
}
