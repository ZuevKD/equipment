package ru.umvd.equipment.entities;

/**
 * Модели средств связи.
 */
public class Model {
    /**
     * Идентификатор модели.
     */
    private long id;
    /**
     * Наименование модели.
     */
    private String name;
    /**
     * Срок службы средства связи.
     */
    private int lifeTime;
    private Radiostation radiostation;
    private Technique technique;

    public Model() {

    }

    public Model(String name, int lifeTime, Radiostation radiostation, Technique technique) {
        this.name = name;
        this.lifeTime = lifeTime;
        this.radiostation = radiostation;
        this.technique = technique;
    }

    public Model(long id, String name, int lifeTime, Radiostation radiostation, Technique technique) {
        this.id = id;
        this.name = name;
        this.lifeTime = lifeTime;
        this.radiostation = radiostation;
        this.technique = technique;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLifeTime() {
        return lifeTime;
    }

    public void setLifeTime(int lifeTime) {
        this.lifeTime = lifeTime;
    }

    public Radiostation getRadiostation() {
        return radiostation;
    }

    public void setRadiostation(Radiostation radiostation) {
        this.radiostation = radiostation;
    }

    public Technique getTechnique() {
        return technique;
    }

    public void setTechnique(Technique technique) {
        this.technique = technique;
    }

    @Override
    public String toString() {
        return name;
    }
}
