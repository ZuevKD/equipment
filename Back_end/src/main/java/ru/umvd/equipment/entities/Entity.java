package ru.umvd.equipment.entities;

public interface Entity {
     long getId();

     String getName();
}
