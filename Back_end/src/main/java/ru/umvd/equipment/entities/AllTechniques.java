package ru.umvd.equipment.entities;

import java.util.List;

public class AllTechniques {
    private List<Radiostation> radiostations;
    private List<Technique> techniques;

    public AllTechniques(List<Radiostation> radiostations, List<Technique> techniques) {
        this.radiostations = radiostations;
        this.techniques = techniques;
    }

    public List<Radiostation> getRadiostations() {
        return radiostations;
    }

    public void setRadiostations(List<Radiostation> radiostations) {
        this.radiostations = radiostations;
    }

    public List<Technique> getTechniques() {
        return techniques;
    }

    public void setTechniques(List<Technique> techniques) {
        this.techniques = techniques;
    }
}
