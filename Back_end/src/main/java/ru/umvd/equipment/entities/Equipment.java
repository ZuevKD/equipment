package ru.umvd.equipment.entities;

public class Equipment {
    private long id;
    private String composition;
    private String description;

    public Equipment() {
    }

    public Equipment(String composition, String description) {
        this.composition = composition;
        this.description = description;
    }

    public Equipment(long id, String composition, String description) {
        this.id = id;
        this.composition = composition;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getComposition() {
        return composition;
    }

    public void setComposition(String composition) {
        this.composition = composition;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
