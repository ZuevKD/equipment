package ru.umvd.equipment.entities;

import java.sql.Date;

/**
 * Поставка.
 */
public class Supply {
    private long id;
    private String invoiceNum;
    private Date date;
    private Provider provider;

    public Supply() {
    }

    public Supply(String invoiceNum, Date date, Provider provider) {
        this.invoiceNum = invoiceNum;
        this.date = date;
        this.provider = provider;
    }

    public Supply(long id, String invoiceNum, Date date, Provider provider) {
        this.id = id;
        this.invoiceNum = invoiceNum;
        this.date = date;
        this.provider = provider;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getInvoiceNum() {
        return invoiceNum;
    }

    public void setInvoiceNum(String invoiceNum) {
        this.invoiceNum = invoiceNum;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }
}
