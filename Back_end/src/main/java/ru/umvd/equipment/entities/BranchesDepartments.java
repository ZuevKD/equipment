package ru.umvd.equipment.entities;

public class BranchesDepartments {
    private long id;
    private long idBranch;
    private long idDepartment;

    public BranchesDepartments() {
    }

    public BranchesDepartments(long idBranch, long idDepartments) {
        this.idBranch = idBranch;
        this.idDepartment = idDepartments;
    }

    public BranchesDepartments(long id, long idBranch, long idDepartments) {
        this.id = id;
        this.idBranch = idBranch;
        this.idDepartment = idDepartments;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdBranch() {
        return idBranch;
    }

    public void setIdBranch(long idBranch) {
        this.idBranch = idBranch;
    }

    public long getIdDepartment() {
        return idDepartment;
    }

    public void setIdDepartment(long idDepartment) {
        this.idDepartment = idDepartment;
    }
}
