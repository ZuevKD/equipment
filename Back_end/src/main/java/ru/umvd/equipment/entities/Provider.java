package ru.umvd.equipment.entities;

/**
 * Поставщик.
 */
public class Provider implements Entity {
    private long id;
    private String name;

    public Provider() {
    }

    public Provider(String name) {
        this.name = name;
    }

    public Provider(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
