package ru.umvd.equipment.entities;

public class Branch implements Entity {
    private long id;
    private String name;
    private long idDepartment;

    public Branch() {

    }

    public Branch(String name, long idDepartment) {
        this.name = name;
        this.idDepartment = idDepartment;
    }

    public Branch(long id, String name, long idDepartment) {
        this.id = id;
        this.name = name;
        this.idDepartment = idDepartment;
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getIdDepartment() {
        return idDepartment;
    }

    public void setIdDepartment(long idDepartment) {
        this.idDepartment = idDepartment;
    }
}
