package ru.umvd.equipment.filters;

import java.util.Date;

public class Filter {
    private String department;
    private String branch;
    private String technique;
    private Date date;
    private String model;
    private Date distributionPlan;
    private String serialNum;
    private String inventoryNum;

    public Filter() {
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getTechnique() {
        return technique;
    }

    public void setTechnique(String technique) {
        this.technique = technique;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Date getDistributionPlan() {
        return distributionPlan;
    }

    public void setDistributionPlan(Date distributionPlan) {
        this.distributionPlan = distributionPlan;
    }

    public String getSerialNum() {
        return serialNum;
    }

    public void setSerialNum(String serialNum) {
        this.serialNum = serialNum;
    }

    public String getInventoryNum() {
        return inventoryNum;
    }

    public void setInventoryNum(String inventoryNum) {
        this.inventoryNum = inventoryNum;
    }
}
