package ru.umvd.equipment.app;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.umvd.equipment.dao.CommEquipmentsDao;
import ru.umvd.equipment.entities.CommEquipment;
import ru.umvd.equipment.entities.Equipment;
import ru.umvd.equipment.entities.Model;
import ru.umvd.equipment.entities.Provider;
import ru.umvd.equipment.entities.Radiostation;
import ru.umvd.equipment.entities.Technique;
import ru.umvd.equipment.filters.Filter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

@RestController
public class CommonTableController {
    private CommEquipmentsDao commEquipmentsDao;

    @PostMapping(path = "/all-techniques", consumes = "application/json", produces = "application/json")
    public List<CommEquipment> get(@RequestBody Filter filter) {
        SimpleDriverDataSource ds = Utils.getDataSource();

        commEquipmentsDao = new CommEquipmentsDao(ds);

        return commEquipmentsDao.getRecords(filter);
    }

    @PostMapping(path = "/create-report", consumes = "application/json", produces = "application/json")
    public void createReport(@RequestBody Filter filter) {
        SimpleDriverDataSource ds = Utils.getDataSource();

        commEquipmentsDao = new CommEquipmentsDao(ds);

        writeIntoExcel(commEquipmentsDao.getRecords(filter));
    }

    private static void writeIntoExcel(List<CommEquipment> allTechniques) {
        try (Workbook book = new XSSFWorkbook()) {
            Sheet sheet = book.createSheet("Отчет");

            Row row = sheet.createRow(0);

            createCell("№ п/п", row);
            createCell("Подразделение", row);
            createCell("Служба", row);
            createCell("Название", row);
            createCell("Модель", row);
            createCell("Диапазон", row);
            createCell("Особенность", row);
            createCell("Заводской №", row);
            createCell("Инвертарный №", row);
            createCell("План распределения", row);
            createCell("Поставщик", row);
            createCell("Дата выпуска", row);
            createCell("Дата ввода в эксплуатацию", row);
            createCell("Срок службы", row);
            createCell("Цена за ед, в руб.", row);
            createCell("Исправно/неисправно", row);
            createCell("Состав изделия", row);
            createCell("Примечание", row);

            createRows(allTechniques, sheet);

            for (int i = 0; i < row.getLastCellNum(); i++) {
                sheet.autoSizeColumn(i);
            }

            book.write(new FileOutputStream(new File("report.xlsx")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void createRows(List<CommEquipment> commEquipments, Sheet sheet) {
        int indexRow = 1;
        for (CommEquipment commEquipment : commEquipments) {
            Row row = sheet.createRow(indexRow++);

            Model model = commEquipment.getModel();
            Radiostation radiostation = model.getRadiostation();
            Technique technique = model.getTechnique();
            Provider provider = commEquipment.getSupply().getProvider();
            Equipment equipment = commEquipment.getEquipment();
            BigDecimal price = commEquipment.getPrice();

            createCell(String.valueOf(indexRow - 1), row);
            createCell(commEquipment.getNameDepartment(), row);
            createCell(commEquipment.getNameBranch(), row);
            createCell(radiostation == null ? technique.getName() : radiostation.getName(), row);
            createCell(model.getName(), row);
            if (technique == null) {
                createCell(radiostation.getDiapason().getName(), row);
                createCell(radiostation.getSpecificity().getName(), row);
            } else {
                createCell("-", row);
                createCell("-", row);
            }
            createCell(commEquipment.getSerialNum(), row);
            createCell(commEquipment.getInventoryNum(), row);
            createCell(dateToString(commEquipment.getDistributionPlan()), row);
            createCell(provider == null ? "" : provider.getName(), row);
            createCell(dateToString(commEquipment.getReleaseDate()), row);
            createCell(dateToString(commEquipment.getCommissioningDate()), row);
            createCell(String.valueOf(model.getLifeTime()), row);
            createCell(price == null ? "" : String.valueOf(commEquipment.getPrice()), row);
            createCell(commEquipment.isModelOperability() ? "Исправно" : "Неисправно", row);
            createCell(equipment.getComposition(), row);
            createCell(equipment.getDescription(), row);
        }
    }

    private static void createCell(String name, Row row) {
        short numCell = row.getLastCellNum();
        Cell numColumn = row.createCell(numCell == -1 ? 0 : numCell);

        numColumn.setCellValue(name);
    }

    private static String dateToString(Date date) {
        if (date == null) {
            return "";
        }

        return String.valueOf(date);
    }
}
