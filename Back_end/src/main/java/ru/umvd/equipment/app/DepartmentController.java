package ru.umvd.equipment.app;

import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.umvd.equipment.dao.DepartmentDao;
import ru.umvd.equipment.entities.Department;

import java.util.List;

import static ru.umvd.equipment.app.Utils.validateRequest;

@RestController
public class DepartmentController {
    private DepartmentDao departmentDao;

    @GetMapping("/departments")
    public List<Department> get() {
        SimpleDriverDataSource ds = Utils.getDataSource();

        departmentDao = new DepartmentDao(ds);

        return departmentDao.getRecords();
    }

    @PostMapping("/department/add")
    public List<Department> add(@RequestBody Department department) {
        validateRequest(department);

        SimpleDriverDataSource ds = Utils.getDataSource();

        departmentDao = new DepartmentDao(ds);

        departmentDao.add(department);
        return departmentDao.getRecords();
    }

    @GetMapping("/department/delete/{id}")
    public List<Department> remove(@PathVariable("id") long id) {
        SimpleDriverDataSource ds = Utils.getDataSource();

        departmentDao = new DepartmentDao(ds);

        departmentDao.remove(id);
        return departmentDao.getRecords();
    }

    @PostMapping("/department/update/{id}")
    public List<Department> update(@PathVariable("id") long id, @RequestBody Department department) {
        validateRequest(department);

        SimpleDriverDataSource ds = Utils.getDataSource();

        departmentDao = new DepartmentDao(ds);

        departmentDao.update(id, department);
        return departmentDao.getRecords();
    }
}
