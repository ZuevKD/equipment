package ru.umvd.equipment.app;

import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.umvd.equipment.dao.CommEquipmentsDao;
import ru.umvd.equipment.dao.EquipmentDao;
import ru.umvd.equipment.dao.ModelDao;
import ru.umvd.equipment.dao.SupplyDao;
import ru.umvd.equipment.dao.TechniqueDao;
import ru.umvd.equipment.entities.CommEquipment;
import ru.umvd.equipment.entities.Equipment;
import ru.umvd.equipment.entities.Model;
import ru.umvd.equipment.entities.Supply;
import ru.umvd.equipment.entities.Technique;

import java.util.List;

import static ru.umvd.equipment.app.Utils.createEquipment;
import static ru.umvd.equipment.app.Utils.createSupply;
import static ru.umvd.equipment.app.Utils.setEquipmentId;
import static ru.umvd.equipment.app.Utils.setModelId;
import static ru.umvd.equipment.app.Utils.setSupplyAndProviderId;

@RestController
public class TechniqueController {
    private SupplyDao supplyDao;
    private EquipmentDao equipmentDao;
    private ModelDao modelDao;
    private CommEquipmentsDao commEquipmentsDao;
    private TechniqueDao techniqueDao;

    @GetMapping("/techniques/{id}")
    public List<CommEquipment> getTechniquesById(@PathVariable("id") long id) {
        SimpleDriverDataSource ds = Utils.getDataSource();

        commEquipmentsDao = new CommEquipmentsDao(ds);

        return commEquipmentsDao.getOnlyTechniquesByBranchId(id);
    }

    @PostMapping("/technique/add/{id}")
    public List<CommEquipment> add(@PathVariable("id") long id, @RequestBody CommEquipment commEquipmentRequest) {
        initialDao();

        Model modelRequest = commEquipmentRequest.getModel();
        Equipment equipmentRequest = commEquipmentRequest.getEquipment();
        Supply supplyRequest = commEquipmentRequest.getSupply();

        Supply supply = createSupply(supplyRequest);

        if (supply != null) {
            long idSupply = supplyDao.add(supply);
            supply.setId(idSupply);
        }

        Equipment equipment = createEquipment(equipmentRequest);

        if (equipment != null) {
            long idEquipment = equipmentDao.add(equipment);
            equipment.setId(idEquipment);
        }

        Technique technique = new Technique(
                modelRequest.getTechnique().getId(),
                modelRequest.getTechnique().getName()
        );

        long techniqueId = techniqueDao.add(technique);

        technique.setId(techniqueId);

        Model model = new Model(
                modelRequest.getName(),
                modelRequest.getLifeTime(),
                null,
                technique
        );

        long idModel = modelDao.add(model);
        model.setId(idModel);

        commEquipmentsDao.add(new CommEquipment(
                id,
                model,
                commEquipmentRequest.getSerialNum(),
                commEquipmentRequest.getInventoryNum(),
                commEquipmentRequest.getPrice(),
                commEquipmentRequest.getReleaseDate(),
                commEquipmentRequest.getCommissioningDate(),
                commEquipmentRequest.getDistributionPlan(),
                equipment,
                commEquipmentRequest.isModelOperability(),
                supply
        ));

        return commEquipmentsDao.getOnlyTechniquesByBranchId(id);
    }

    @PostMapping("/technique/update/{id}")
    public List<CommEquipment> update(@PathVariable("id") long id, @RequestBody CommEquipment commEquipmentRequest) {
        initialDao();

        Model modelRequest = commEquipmentRequest.getModel();
        Technique techniqueRequest = modelRequest.getTechnique();

        CommEquipment editedCommEquipment = commEquipmentsDao.getRecord(id, commEquipmentRequest.getId());
        Model editedModel = modelDao.getRecord(editedCommEquipment.getModel().getId());
        Technique editedTechnique = techniqueDao.getRecord(editedModel.getTechnique().getId());

        long techniqueId = editedTechnique.getId();
        techniqueDao.update(techniqueId, techniqueRequest);
        techniqueRequest.setId(techniqueId);
        modelRequest.setTechnique(techniqueRequest);

        setModelId(commEquipmentRequest, editedModel, modelRequest);
        setEquipmentId(commEquipmentRequest, editedCommEquipment);
        setSupplyAndProviderId(commEquipmentRequest, editedCommEquipment);

        commEquipmentsDao.update(editedCommEquipment.getId(), commEquipmentRequest);

        return commEquipmentsDao.getOnlyTechniquesByBranchId(id);
    }

    @PostMapping("/technique/delete/{id}")
    public List<CommEquipment> delete(@PathVariable("id") long id, @RequestBody CommEquipment commEquipmentRequest) {
        initialDao();

        commEquipmentsDao.remove(commEquipmentRequest.getId());
        modelDao.remove(commEquipmentRequest.getModel().getId());
        techniqueDao.remove(commEquipmentRequest.getModel().getTechnique().getId());
        supplyDao.remove(commEquipmentRequest.getSupply().getId());
        equipmentDao.remove(commEquipmentRequest.getEquipment().getId());

        return commEquipmentsDao.getOnlyTechniquesByBranchId(id);
    }

    private void initialDao() {
        SimpleDriverDataSource ds = Utils.getDataSource();

        techniqueDao = new TechniqueDao(ds);
        equipmentDao = new EquipmentDao(ds);
        supplyDao = new SupplyDao(ds);
        modelDao = new ModelDao(ds);
        commEquipmentsDao = new CommEquipmentsDao(ds);
    }
}
