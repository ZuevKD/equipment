package ru.umvd.equipment.app;

import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.umvd.equipment.dao.ProviderDao;
import ru.umvd.equipment.entities.Provider;

import java.util.List;

import static ru.umvd.equipment.app.Utils.validateRequest;

@RestController
public class ProviderController {
    private ProviderDao providerDao;

    @GetMapping("/providers")
    public List<Provider> get() {
        createDao();

        return providerDao.getRecords();
    }

    @PostMapping("/provider/add")
    public List<Provider> add(@RequestBody Provider provider) {
        validateRequest(provider);

        createDao();

        providerDao.add(provider);
        return providerDao.getRecords();
    }

    @GetMapping("/provider/delete/{id}")
    public List<Provider> remove(@PathVariable("id") long id) {
        createDao();

        providerDao.remove(id);
        return providerDao.getRecords();
    }

    @PostMapping("/provider/update/{id}")
    public List<Provider> update(@PathVariable("id") long id, @RequestBody Provider provider) {
        validateRequest(provider);
        
        createDao();

        providerDao.update(id, provider);
        return providerDao.getRecords();
    }

    private void createDao() {
        SimpleDriverDataSource ds = Utils.getDataSource();

        providerDao = new ProviderDao(ds);
    }
}
