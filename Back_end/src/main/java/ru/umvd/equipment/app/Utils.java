package ru.umvd.equipment.app;

import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import ru.umvd.equipment.dao.EquipmentDao;
import ru.umvd.equipment.dao.ModelDao;
import ru.umvd.equipment.dao.SupplyDao;
import ru.umvd.equipment.entities.CommEquipment;
import ru.umvd.equipment.entities.Entity;
import ru.umvd.equipment.entities.Equipment;
import ru.umvd.equipment.entities.Model;
import ru.umvd.equipment.entities.Provider;
import ru.umvd.equipment.entities.Specificity;
import ru.umvd.equipment.entities.Supply;

import static org.apache.logging.log4j.util.Strings.isBlank;

public class Utils {
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/equipment_database";
    private static final String USER = "postgres";
    private static final String PASS = "root";

    private static SimpleDriverDataSource ds;

    private Utils() {
    }

    public static SimpleDriverDataSource getDataSource() {
        if (ds == null) {
            ds = new SimpleDriverDataSource();
            ds.setDriver(new org.postgresql.Driver());
            ds.setUrl(DB_URL);
            ds.setUsername(USER);
            ds.setPassword(PASS);
        }

        return ds;
    }

    public static Supply createSupply(Supply supply) {
        if (isNullSupply(supply)) {
            return null;
        }

        return new Supply(
                supply.getInvoiceNum(),
                supply.getDate(),
                supply.getProvider()
        );
    }

    public static Equipment createEquipment(Equipment equipment) {
        if (equipment == null || (isBlank(equipment.getComposition()) && isBlank(equipment.getDescription()))) {
            return null;
        }

        return new Equipment(
                equipment.getComposition(),
                equipment.getDescription()
        );
    }

    public static Specificity createSpecificity(Specificity specificity) {
        if (specificity == null || specificity.getId() == 0) {
            return null;
        }

        return new Specificity(
                specificity.getName()
        );
    }

    public static void validateRequest(Entity entity) {
        if (entity != null && entity.getName() != null && "".equals(entity.getName().trim())) {
            throw new IllegalArgumentException("Введена пустая строка");
        }
    }

    public static Equipment createEquipmentRequest(CommEquipment commEquipmentRequest) {
        Equipment equipment = commEquipmentRequest.getEquipment();
        if (equipment.getId() == 0 && isBlank(equipment.getComposition()) && isBlank(equipment.getDescription())) {
            return null;
        }

        return commEquipmentRequest.getEquipment();
    }

    public static Supply createSupplyRequest(CommEquipment commEquipmentRequest) {
        Supply supplyRequest = commEquipmentRequest.getSupply();

        if (isNullSupply(commEquipmentRequest.getSupply())) {
            return null;
        }

        return supplyRequest;
    }

    private static boolean isNullSupply(Supply supply) {
        return supply == null || (isNullProvider(supply.getProvider())
                && isBlank(supply.getInvoiceNum()) && supply.getDate() == null);
    }

    public static CommEquipment setModelId(CommEquipment commEquipmentRequest, Model editedModel, Model modelRequest) {
        ModelDao modelDao = new ModelDao(ds);

        long modelId = editedModel.getId();
        modelDao.update(modelId, modelRequest);
        modelRequest.setId(modelId);
        commEquipmentRequest.setModel(modelRequest);

        return commEquipmentRequest;
    }

    public static CommEquipment setSupplyAndProviderId(CommEquipment commEquipmentRequest, CommEquipment editedCommEquipment) {
        Supply supplyRequest = createSupplyRequest(commEquipmentRequest);

        if (supplyRequest == null) {
            commEquipmentRequest.setSupply(null);
            return commEquipmentRequest;
        }

        SupplyDao supplyDao = new SupplyDao(ds);

        Supply editedSupply = null;
        if (editedCommEquipment.getSupply() != null && editedCommEquipment.getSupply().getId() != 0) {
            editedSupply = supplyDao.getRecord(editedCommEquipment.getSupply().getId());
        }

        if (editedSupply != null) {
            long supplyId = editedSupply.getId();
            supplyDao.update(supplyId, supplyRequest);
            supplyRequest.setId(supplyId);
        } else {
            long supplyId = supplyDao.add(supplyRequest);
            supplyRequest.setId(supplyId);
        }

        return commEquipmentRequest;
    }

    private static boolean isNullProvider(Provider provider) {
        return provider == null || isBlank(provider.getName()) || provider.getId() == 0;
    }

    public static CommEquipment setEquipmentId(CommEquipment commEquipmentRequest, CommEquipment editedCommEquipment) {
        Equipment equipmentRequest = createEquipmentRequest(commEquipmentRequest);

        if (equipmentRequest == null) {
            commEquipmentRequest.setEquipment(null);
            return commEquipmentRequest;
        }

        EquipmentDao equipmentDao = new EquipmentDao(ds);
        Equipment editedEquipment = null;

        if (equipmentRequest.getId() != 0) {
            editedEquipment = equipmentDao.getRecord(editedCommEquipment.getEquipment().getId());

            long equipmentId = editedEquipment.getId();
            equipmentDao.update(equipmentId, equipmentRequest);
            equipmentRequest.setId(equipmentId);
            commEquipmentRequest.setEquipment(equipmentRequest);
        } else {
            long equipmentId = equipmentDao.add(equipmentRequest);
            equipmentRequest.setId(equipmentId);
            commEquipmentRequest.setEquipment(equipmentRequest);
        }

        return commEquipmentRequest;
    }
}
