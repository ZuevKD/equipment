package ru.umvd.equipment.app;

import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.umvd.equipment.dao.CommEquipmentsDao;
import ru.umvd.equipment.dao.DiapasonDao;
import ru.umvd.equipment.dao.EquipmentDao;
import ru.umvd.equipment.dao.ModelDao;
import ru.umvd.equipment.dao.RadiostationDao;
import ru.umvd.equipment.dao.SpecificityDao;
import ru.umvd.equipment.dao.SupplyDao;
import ru.umvd.equipment.entities.CommEquipment;
import ru.umvd.equipment.entities.Diapason;
import ru.umvd.equipment.entities.Equipment;
import ru.umvd.equipment.entities.Model;
import ru.umvd.equipment.entities.Radiostation;
import ru.umvd.equipment.entities.Specificity;
import ru.umvd.equipment.entities.Supply;

import java.util.List;

import static org.apache.logging.log4j.util.Strings.isBlank;
import static ru.umvd.equipment.app.Utils.createEquipment;
import static ru.umvd.equipment.app.Utils.createSupply;
import static ru.umvd.equipment.app.Utils.setEquipmentId;
import static ru.umvd.equipment.app.Utils.setModelId;
import static ru.umvd.equipment.app.Utils.setSupplyAndProviderId;

@RestController
public class RadiostationController {
    private RadiostationDao radiostationDao;
    private SupplyDao supplyDao;
    private EquipmentDao equipmentDao;
    private ModelDao modelDao;
    private SpecificityDao specificityDao;
    private DiapasonDao diapasonDao;
    private CommEquipmentsDao commEquipmentsDao;

    @GetMapping("/radiostations/{id}")
    public List<CommEquipment> getRadisotationsById(@PathVariable("id") long id) {
        SimpleDriverDataSource ds = Utils.getDataSource();

        commEquipmentsDao = new CommEquipmentsDao(ds);

        return commEquipmentsDao.getOnlyRadiostationsByBranchId(id);
    }

    @PostMapping("/radiostation/add/{id}")
    public List<CommEquipment> add(@PathVariable("id") long id, @RequestBody CommEquipment commEquipmentRequest) {
        initialDao();

        Model modelRequest = commEquipmentRequest.getModel();
        Equipment equipmentRequest = commEquipmentRequest.getEquipment();
        Supply supplyRequest = commEquipmentRequest.getSupply();

        Supply supply = createSupply(supplyRequest);

        if (supply != null) {
            long idSupply = supplyDao.add(supply);
            supply.setId(idSupply);
        }

        Equipment equipment = createEquipment(equipmentRequest);

        if (equipment != null) {
            long idEquipment = equipmentDao.add(equipment);
            equipment.setId(idEquipment);
        }

        Specificity specificityRequest = modelRequest.getRadiostation().getSpecificity();
        Diapason diapasonRequest = modelRequest.getRadiostation().getDiapason();

        long idSpecificity = specificityRequest.getId();

        if (idSpecificity == 0) {
            specificityRequest = null;
        }

        Radiostation radiostation = new Radiostation(
                modelRequest.getRadiostation().getId(),
                modelRequest.getRadiostation().getName(),
                specificityRequest,
                diapasonRequest,
                modelRequest.getRadiostation().isPresenceScDevice()
        );

        long radiostationId = radiostationDao.add(radiostation);

        radiostation.setId(radiostationId);

        Model model = new Model(
                modelRequest.getName(),
                modelRequest.getLifeTime(),
                radiostation,
                null
        );

        long idModel = modelDao.add(model);
        model.setId(idModel);

        commEquipmentsDao.add(new CommEquipment(
                id,
                model,
                commEquipmentRequest.getSerialNum(),
                commEquipmentRequest.getInventoryNum(),
                commEquipmentRequest.getPrice(),
                commEquipmentRequest.getReleaseDate(),
                commEquipmentRequest.getCommissioningDate(),
                commEquipmentRequest.getDistributionPlan(),
                equipment,
                commEquipmentRequest.isModelOperability(),
                supply
        ));

        return commEquipmentsDao.getOnlyRadiostationsByBranchId(id);
    }

    @PostMapping("/radiostation/update/{id}")
    public List<CommEquipment> update(@PathVariable("id") long id, @RequestBody CommEquipment commEquipmentRequest) {
        initialDao();

        Model modelRequest = commEquipmentRequest.getModel();
        Radiostation radiostationRequest = modelRequest.getRadiostation();
        Specificity specificityRequest = radiostationRequest.getSpecificity();
        if (isBlank(specificityRequest.getName())) {
            specificityRequest = null;
        }

        CommEquipment editedCommEquipment = commEquipmentsDao.getRecord(id, commEquipmentRequest.getId());
        Model editedModel = modelDao.getRecord(editedCommEquipment.getModel().getId());
        Radiostation editedRadiostation = radiostationDao.getRecord(editedModel.getRadiostation().getId());

        if (specificityRequest != null && specificityRequest.getId() != 0) {
            radiostationRequest.setSpecificity(specificityRequest);
        } else {
            radiostationRequest.setSpecificity(null);
        }

        long radiostationId = editedRadiostation.getId();
        radiostationDao.update(radiostationId, radiostationRequest);
        radiostationRequest.setId(radiostationId);
        modelRequest.setRadiostation(radiostationRequest);

        setModelId(commEquipmentRequest, editedModel, modelRequest);
        setEquipmentId(commEquipmentRequest, editedCommEquipment);
        setSupplyAndProviderId(commEquipmentRequest, editedCommEquipment);

        commEquipmentsDao.update(editedCommEquipment.getId(), commEquipmentRequest);

        return commEquipmentsDao.getOnlyRadiostationsByBranchId(id);
    }

    @PostMapping("/radiostation/delete/{id}")
    public List<CommEquipment> delete(@PathVariable("id") long id, @RequestBody CommEquipment commEquipmentRequest) {
        initialDao();

        commEquipmentsDao.remove(commEquipmentRequest.getId());
        modelDao.remove(commEquipmentRequest.getModel().getId());
        radiostationDao.remove(commEquipmentRequest.getModel().getRadiostation().getId());
        supplyDao.remove(commEquipmentRequest.getSupply().getId());
        equipmentDao.remove(commEquipmentRequest.getEquipment().getId());

        return commEquipmentsDao.getOnlyRadiostationsByBranchId(id);
    }

    private void initialDao() {
        SimpleDriverDataSource ds = Utils.getDataSource();

        radiostationDao = new RadiostationDao(ds);
        equipmentDao = new EquipmentDao(ds);
        supplyDao = new SupplyDao(ds);
        modelDao = new ModelDao(ds);
        specificityDao = new SpecificityDao(ds);
        diapasonDao = new DiapasonDao(ds);
        commEquipmentsDao = new CommEquipmentsDao(ds);
    }

    @GetMapping("/specificities")
    public List<Specificity> getSpecificities() {
        SimpleDriverDataSource ds = Utils.getDataSource();

        specificityDao = new SpecificityDao(ds);

        return specificityDao.getRecords();
    }

    @GetMapping("/diapasons")
    public List<Diapason> getDiapasons() {
        SimpleDriverDataSource ds = Utils.getDataSource();

        diapasonDao = new DiapasonDao(ds);

        return diapasonDao.getRecords();
    }
}
