package ru.umvd.equipment.app;

import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.umvd.equipment.dao.BranchDao;
import ru.umvd.equipment.entities.Branch;

import java.util.List;

import static ru.umvd.equipment.app.Utils.validateRequest;

@RestController
public class BranchController {
    private BranchDao branchDao;

    @GetMapping("/branches/{id}")
    public List<Branch> getBranchesById(@PathVariable("id") long id) {
        SimpleDriverDataSource ds = Utils.getDataSource();

        branchDao = new BranchDao(ds);

        return branchDao.getRecordsByCodDepartment(id);
    }

    @PostMapping("/branch/add/{id}")
    public List<Branch> add(@PathVariable("id") long id, @RequestBody Branch branch) {
        validateRequest(branch);

        SimpleDriverDataSource ds = Utils.getDataSource();

        branchDao = new BranchDao(ds);

        branchDao.add(branch);
        return branchDao.getRecordsByCodDepartment(id);
    }

    @GetMapping("department/{idDepartment}/branch/{id}/delete")
    public List<Branch> remove(@PathVariable("id") long id, @PathVariable("idDepartment") long idDepartment) {
        SimpleDriverDataSource ds = Utils.getDataSource();

        branchDao = new BranchDao(ds);

        branchDao.remove(id);
        return branchDao.getRecordsByCodDepartment(idDepartment);
    }

    @PostMapping("department/{idDepartment}/branch/{id}/update")
    public List<Branch> update(@PathVariable("idDepartment") long idDepartment,
                               @PathVariable("id") long id, @RequestBody Branch branch) {
        validateRequest(branch);

        SimpleDriverDataSource ds = Utils.getDataSource();

        branchDao = new BranchDao(ds);
        branch.setIdDepartment(idDepartment);

        branchDao.update(id, branch);
        return branchDao.getRecordsByCodDepartment(idDepartment);
    }
}
