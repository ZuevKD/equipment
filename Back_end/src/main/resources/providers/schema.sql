CREATE TABLE providers
(
  cod_provider integer NOT NULL auto_increment,
  name_provider text,
  invoice_number integer,
  invoice_date date,
  CONSTRAINT providers_pkey PRIMARY KEY (cod_provider),
);
