CREATE TABLE radiostations
(
  cod_radiostation integer NOT NULL auto_increment,
  name_radiostation text,
  cod_model integer,
  cod_specificity integer,
  cod_diapason integer,
  presence_sc_device bit,
  CONSTRAINT radiostations_pkey PRIMARY KEY (cod_radiostation),
  CONSTRAINT radiostations_fkey0 FOREIGN KEY (cod_model)
      REFERENCES public.models (cod_model)
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT radiostations_fkey1 FOREIGN KEY (cod_specificity)
      REFERENCES public.specificities (cod_specificity)
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT radiostations_fkey2 FOREIGN KEY (cod_diapason)
      REFERENCES public.diapasons (cod_diapason)
      ON UPDATE NO ACTION ON DELETE NO ACTION,
);