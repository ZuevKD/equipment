CREATE TABLE specificities
(
  cod_specificity integer NOT NULL auto_increment,
  name_specificity text,
  CONSTRAINT specificities_pkey PRIMARY KEY (cod_specificity)
);