CREATE TABLE departments
(
  cod_department integer NOT NULL auto_increment,
  name_department text,
  CONSTRAINT departments_pkey PRIMARY KEY (cod_department)
);