CREATE TABLE technics (
  cod_technic integer NOT NULL auto_increment,
  name_technic text,
  cod_modal integer NOT NULL,
  CONSTRAINT technics_pkey PRIMARY KEY (cod_technic),
  CONSTRAINT technics_fkey0 FOREIGN KEY (cod_modal)
      REFERENCES models (cod_model)
      ON UPDATE NO ACTION ON DELETE NO ACTION
);