CREATE TABLE equipments
(
  cod_equipment integer NOT NULL auto_increment,
  composition text,
  commentary text,
  CONSTRAINT equipments_pkey PRIMARY KEY (cod_equipment),
);