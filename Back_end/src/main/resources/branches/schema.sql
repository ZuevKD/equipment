CREATE TABLE branches
(
  cod_branch integer NOT NULL auto_increment,
  name_branch text,
  cod_department integer,
  CONSTRAINT branches_pkey PRIMARY KEY (cod_branch)
);