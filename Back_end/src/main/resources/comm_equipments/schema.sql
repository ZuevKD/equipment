CREATE TABLE comm_equipments
(
  cod_comm_equipment integer NOT NULL auto_increment,
  cod_branch integer not null,
  cod_technic integer,
  cod_radiostation integer,
  CONSTRAINT comm_equipments_pkey PRIMARY KEY (cod_comm_equipment),
   CONSTRAINT comm_equipments_fkey0 FOREIGN KEY (cod_branch)
      REFERENCES public.branches (cod_branch)
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT comm_equipments_fkey1 FOREIGN KEY (cod_technic)
      REFERENCES public.technics (cod_technic)
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT comm_equipments_fkey2 FOREIGN KEY (cod_radiostation)
      REFERENCES public.radiostations (cod_radiostation)
      ON UPDATE NO ACTION ON DELETE NO ACTION
);