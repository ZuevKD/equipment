CREATE TABLE diapasons
(
  cod_diapason integer NOT NULL auto_increment,
  name_diapason text,
  CONSTRAINT diapasons_pkey PRIMARY KEY (cod_diapason)
);