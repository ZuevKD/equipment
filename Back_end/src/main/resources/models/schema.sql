CREATE TABLE public.models
(
  cod_model integer NOT NULL auto_increment,
  name_model text NOT NULL,
  serial_number text,
  inventory_number text,
  lifetime integer NOT NULL,
  price integer,
  release_date date,
  commissioning_date date,
  distribution_plan date,
  cod_provider integer NOT NULL,
  cod_equipment integer,
  model_operability bit NOT NULL,
  CONSTRAINT models_pkey PRIMARY KEY (cod_model),
  CONSTRAINT models_fkey0 FOREIGN KEY (cod_equipment)
      REFERENCES public.equipments (cod_equipment)
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT models_fkey1 FOREIGN KEY (cod_provider)
      REFERENCES public.providers (cod_provider)
      ON UPDATE NO ACTION ON DELETE NO ACTION
);